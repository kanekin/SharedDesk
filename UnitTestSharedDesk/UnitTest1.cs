﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SharedDesk.interfaces;
using SharedDesk;
using SharedDesk.TCP_file_transfer;
using SharedDesk.Data_storage;
using System.Collections.Generic;
using System.IO;

namespace SharedDesk
{
    [TestClass]
    public class UnitTest1
    {
        // interface for storing information
        public IStoreData data;
        private const int timeOutMillisec = 20000; // 5 sec

        private byte[] md5StringToByteArray(string md5)
        {
            String[] arr = md5.Split('-');
            byte[] array = new byte[arr.Length];

            for (int i = 0; i < arr.Length; i++)
            {
                array[i] = Convert.ToByte(arr[i], 16);
            }

            return array;
        }

        [TestMethod, Timeout(timeOutMillisec)]
        public void TestXML()
        {
            data = new XMLStorage();

            string md5 = "31-41-A8-51-FF-3A-36-E4-0E-C8-48-5A-77-18-0F-61";
            byte[] md5Byte = md5StringToByteArray(md5);

            // create a file to save
            FileInfoP2P file = new FileInfoP2P("test.avi", 500, md5Byte, FileInfoP2P.Status.newFile);

            data.addKnownFile(123, file);

            Assert.AreEqual("test.avi", data.getFileName(md5));
            Assert.AreEqual(500, data.getSize(md5));
            Assert.AreEqual("31-41-A8-51-FF-3A-36-E4-0E-C8-48-5A-77-18-0F-61", data.getMd5OfFile("test.avi"));

        }

        [TestMethod, Timeout(timeOutMillisec)]
        public void TestAddKnownFile()
        {
            data = new SqliteStorage();
            data.resetDB();

            string md5 = "69-41-A8-51-FF-3A-36-E4-0E-C8-48-5A-77-18-0F-61";
            byte[] md5Byte = md5StringToByteArray(md5);

            // create a file to save
            FileInfoP2P file = new FileInfoP2P("test_known.avi", 654, md5Byte, FileInfoP2P.Status.newFile);

            data.setGuid(6969);
            data.addKnownFile(123, file);

            Assert.AreEqual("test_known.avi", data.getFileName(md5));
            Assert.AreEqual(654, data.getSize(md5));
            Assert.AreEqual(md5, data.getMd5OfFile("test_known.avi"));
            Assert.AreEqual(true, data.fileExist(md5));

        }

        [TestMethod, Timeout(timeOutMillisec)]
        public void TestUsername()
        {
            data = new SqliteStorage();
            data.resetDB();

            // create a file to save

            data.addUser(66, "Jan");
            data.addUser(35, "Max");
            data.addUser(12, "John");
            data.addUser(92, "Tatsuya");

            Assert.AreEqual("Jan", data.getUsername(66));
            Assert.AreEqual("Max", data.getUsername(35));
            Assert.AreEqual("John", data.getUsername(12));
            Assert.AreEqual("Tatsuya", data.getUsername(92));

        }

        [TestMethod, Timeout(timeOutMillisec)]
        public void TestUsername_2()
        {
            data = new SqliteStorage();
            data.resetDB();

            // create a file to save

            data.addUser(66, "Jan");
            data.addUser(35, "Max");
 
            // should also write this error in the log file!
            data.addUser(-12, "Error");

            int test = int.MaxValue;
            test += 120;
            data.addUser(test, "Error");
            data.addUser(1326, "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");

            data.addUser(12, "John");
            data.addUser(92, "Tatsuya");

            Assert.AreEqual("Jan", data.getUsername(66));
            Assert.AreEqual("Max", data.getUsername(35));
            Assert.AreEqual("John", data.getUsername(12));
            Assert.AreEqual("Tatsuya", data.getUsername(92));

        }

        [TestMethod, Timeout(timeOutMillisec)]
        public void TestSaveGuid()
        {
            data = new SqliteStorage();
            data.resetDB();

            Assert.AreEqual(-1, data.getGuid());

            data.setGuid(-654);
            data.setGuid(1234);
            Assert.AreEqual(1234, data.getGuid());
            
            data.setGuid(64);
            Assert.AreEqual(64, data.getGuid());
            
            data.setGuid(68746);
            Assert.AreEqual(68746, data.getGuid());



        }

        [TestMethod, Timeout(timeOutMillisec)]
        public void TestSavePort()
        {
            data = new SqliteStorage();
            data.resetDB();

            Assert.AreEqual(-1, data.getPort());

            // first set guid
            data.setGuid(1234);

            data.setPort(-654);
            data.setPort(62400);
            Assert.AreEqual(62400, data.getPort());

            // allowed 
            data.setPort(65535);
            Assert.AreEqual(65535, data.getPort());

            data.setPort(100);

            // not allowed
            data.setPort(65536);
            Assert.AreEqual(100, data.getPort());

            data.setPort(999999);
            Assert.AreEqual(100, data.getPort());

        }

        [TestMethod, Timeout(timeOutMillisec)]
        public void TestSaveBootPeerPort()
        {
            data = new SqliteStorage();
            data.resetDB();

            Assert.AreEqual(-1, data.getBootPeerPort());

            // first set guid
            data.setGuid(1234);

            data.setBootPeerPort(-654);
            data.setBootPeerPort(32444);

            Assert.AreEqual(32444, data.getBootPeerPort());
            
            // allowed 
            data.setBootPeerPort(65535);
            Assert.AreEqual(65535, data.getBootPeerPort());

            data.setBootPeerPort(100);

            // not allowed
            data.setBootPeerPort(65536);
            Assert.AreEqual(100, data.getBootPeerPort());

            data.setBootPeerPort(999999);
            Assert.AreEqual(100, data.getBootPeerPort());


        }

        [TestMethod, Timeout(timeOutMillisec)]
        public void TestSaveBootPeerIP()
        {
            data = new SqliteStorage();
            data.resetDB();

            // should return "" if boot peer is not set
            Assert.AreEqual("", data.getBootPeerIP());

            // first set guid
            data.setGuid(1234);

            // error handling
            data.setBootPeerIP("127.0.1.6574");
            Assert.AreEqual("", data.getBootPeerIP());

            data.setBootPeerIP("127.565435.1.1");
            Assert.AreEqual("", data.getBootPeerIP());

            data.setBootPeerIP("19.345.145.415");
            Assert.AreEqual("", data.getBootPeerIP());

            //data.setBootPeerIP("15");
            //Assert.AreEqual("", data.getBootPeerIP());

            // correct boot peer
            data.setBootPeerIP("192.168.1.1");
            Assert.AreEqual("192.168.1.1", data.getBootPeerIP());

        }

        [TestMethod, Timeout(timeOutMillisec)]
        public void TestShareFile()
        {
            data = new SqliteStorage();
            data.resetDB();

            // create a file to save
            FileInfo file = new FileInfo("../../testFile.mp3");
            string md5 = "91-C8-93-0A-12-73-94-3E-83-B9-BE-3D-7F-1C-AA-C5";

            Assert.AreEqual(0, data.getYourSharedFiles().Count);

            // first set my guid since this is required for a connection
            // between File, Peer and State
            data.setGuid(69);
            data.authorizePublicFileDownload(file, md5);

            Assert.AreEqual(md5, data.getMd5OfFile("testFile.mp3"));
            Assert.AreEqual(1, data.getYourSharedFiles().Count);
            Assert.AreEqual("testFile.mp3", data.getFileName(md5));

        }

                /// <summary>
        /// 4 peers share the same file
        /// </summary>
        [TestMethod, Timeout(timeOutMillisec)]
        public void TestStateOfFile()
        {
            data = new SqliteStorage();
            data.resetDB();

            // create one file
            string fileOneMD5 = "45-41-A8-69-FF-3A-36-E4-0E-C8-48-5A-77-18-0F-61";
            byte[] fileOneMD5Byte = md5StringToByteArray(fileOneMD5);
            string name = "test_state_v1.avi";
            long size = 853435;

            FileInfoP2P file = new FileInfoP2P(name, size, fileOneMD5Byte, FileInfoP2P.Status.newFile);

            // create second file 
            string fileTwoMD5 = "72-12-A8-69-FF-3A-36-E4-0E-C8-48-5A-77-18-0F-61";
            byte[] fileTwoMD5Byte = md5StringToByteArray(fileTwoMD5);
            name = "test_state_v2.avi";
            size = 3547587;

            FileInfoP2P file2 = new FileInfoP2P(name, size, fileTwoMD5Byte, FileInfoP2P.Status.newFile);

            // files should not exist as we cleared the db
            Assert.IsFalse(data.fileExist(fileOneMD5));
            Assert.IsFalse(data.fileExist(fileTwoMD5));

            // add two peers that knows two different files
            data.addKnownFile(69, file);
            data.addKnownFile(564, file2);

            // file now exist
            Assert.IsTrue(data.fileExist(fileOneMD5));
            Assert.IsTrue(data.fileExist(fileTwoMD5));

            // should be 2 files avaliable for download
            Assert.AreEqual(2, data.getAvaliableFileDownloads().Count);

            // 2 files have status "new file"
            Assert.AreEqual(2, data.getAvaliableFileDownloads(FileInfoP2P.Status.newFile).Count);
            Assert.AreEqual(0, data.getAvaliableFileDownloads(FileInfoP2P.Status.downloading).Count);
            Assert.AreEqual(0, data.getAvaliableFileDownloads(FileInfoP2P.Status.complete).Count);

            // change status of the two files
            data.setStatusOfFile(fileOneMD5, FileInfoP2P.Status.downloading);
            data.setStatusOfFile(fileTwoMD5, FileInfoP2P.Status.complete);

            // should be 1 downloading and 1 complete
            Assert.AreEqual(0, data.getAvaliableFileDownloads(FileInfoP2P.Status.newFile).Count);
            Assert.AreEqual(1, data.getAvaliableFileDownloads(FileInfoP2P.Status.downloading).Count);
            Assert.AreEqual(1, data.getAvaliableFileDownloads(FileInfoP2P.Status.complete).Count);

            // change status of the two files
            data.setStatusOfFile(fileOneMD5, FileInfoP2P.Status.complete);
            data.setStatusOfFile(fileTwoMD5, FileInfoP2P.Status.complete);

            // should be two files that are complete
            Assert.AreEqual(2, data.getAvaliableFileDownloads(FileInfoP2P.Status.complete).Count);

        }


        /// <summary>
        /// 4 peers share the same file
        /// </summary>
        [TestMethod, Timeout(timeOutMillisec)]
        public void TestAvaliableFiles()
        {
            data = new SqliteStorage();
            data.resetDB();

            // create a file to save
            string md5 = "72-41-A8-51-FF-3A-36-E4-0E-C8-48-5A-77-18-0F-61";
            byte[] md5Byte = md5StringToByteArray(md5);
            string name = "test_2.avi";
            long size = 4567;

            FileInfoP2P file = new FileInfoP2P(name, size, md5Byte, FileInfoP2P.Status.newFile);

            // file should not exist as we cleared the db
            Assert.IsFalse(data.fileExist(md5));

            // this should create 4 entries in the Peer table,
            // and 1 entry in the File and the Status table
            data.addKnownFile(123, file);
            data.addKnownFile(354, file);

            // test error handling
            data.addKnownFile(-666, file);
            data.addKnownFile(-1, file);
            data.addKnownFile(69, null);

            data.addKnownFile(321, file);
            data.addKnownFile(987, file);

            // file should now exist
            Assert.IsTrue(data.fileExist(md5));
 
            // check that we get the correct file size
            Assert.AreEqual(4567, data.getSize(md5));

            // get avaliable files
            List<FileInfoP2P> files = data.getAvaliableFileDownloads();

            // should be 1 file avaliable for download 
            Assert.AreEqual(1, files.Count);

            // check that all other values are correct
            CollectionAssert.AreEqual(md5Byte, files[0].md5);
            Assert.AreEqual(name, files[0].name);
            Assert.AreEqual(size, files[0].size);

            // There should be 4 peers with this one file
            Assert.AreEqual(4, data.findPeersWithFile(md5).Count);

            // TODO: 
            // check that the list of PeerInfo object contain the right values

        }

        [TestMethod]
        public void Test_100_knownFiles()
        {
            data = new SqliteStorage();
            data.resetDB();

            for (int i = 10; i < 20; i++)
            {
                for (int y = 10; y < 20; y++)
                {
                    string filename = string.Format("test_{0}_{1}.avi", i, y);
                    string md5 = string.Format("31-41-A8-51-FF-3A-36-E4-0E-C8-48-5A-77-18-{0}-{1}", i, y);
                    byte[] md5Byte = md5StringToByteArray(md5);

                    // create a file to save
                    FileInfoP2P file = new FileInfoP2P(filename, 500, md5Byte, FileInfoP2P.Status.newFile);

                    data.addKnownFile(123, file);
                }
            }

            List<FileInfoP2P> files = data.getAvaliableFileDownloads();

            // there should be exactly 100 files avaliable for download
            Assert.AreEqual(100, files.Count);

        }

        /// <summary>
        /// 1. Removes all data in files table
        /// 2. Adds 7921 rows in the File table and 
        /// 3. Checks that 7921 files are avaliable to download
        /// 
        /// Takes from 34 sec to 2 min
        /// </summary>
        [TestMethod]
        public void Test_7921()
        {
            data = new SqliteStorage();
            data.resetDB();

            for (int i = 10; i < 99; i++)
            {
                for (int y = 10; y < 99; y++)
                {
                    string filename = string.Format("test_{0}_{1}.avi", i, y);
                    string md5 = string.Format("31-41-A8-51-FF-3A-36-E4-0E-C8-48-5A-77-18-{0}-{1}", i, y);
                    byte[] md5Byte = md5StringToByteArray(md5);

                    // create a file to save
                    FileInfoP2P file = new FileInfoP2P(filename, 500, md5Byte, FileInfoP2P.Status.newFile);

                    data.addKnownFile(123, file);
                }
            }

            List<FileInfoP2P> files = data.getAvaliableFileDownloads();

            // there should be exactly 100 files avaliable for download
            Assert.AreEqual(7921, files.Count);

        }

    }
}
