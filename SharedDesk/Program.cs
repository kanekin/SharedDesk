﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SharedDesk
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //Application.Run(new SplashScreen());
            try
            {
                Application.Run(new Sharedesk.Sharedesk_Setup());
                Application.Run(new Sharedesk.Sharedesk_Main());
                Application.Run(new SharedDeskMainApp());
            }
            catch (AccessViolationException e)
            {
                Console.WriteLine("error: " + e.ToString());
            }
        }
    }
}
