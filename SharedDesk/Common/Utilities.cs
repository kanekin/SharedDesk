﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Net.NetworkInformation;
using System.Text.RegularExpressions;
using SharedDesk.Data_storage;

namespace SharedDesk
{
    public class Utilities
    {
        private static readonly Regex validIpV4AddressRegex = new Regex(@"^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$", RegexOptions.IgnoreCase);
        private ErrorLog logFile;

        public void globalAppInitialization()
        {
            // TODO : Implement gloabl application initialization 
            // (check for file, load file, set all attributes / properties / objects, join network, and so forth...)
        }

        // Method for getting the local IP address in order to use it for the peer.
        public string LocalIPAddress()
        {
            try
            {
                IPHostEntry host;
                string localIP = "";
                host = Dns.GetHostEntry(Dns.GetHostName());
                foreach (IPAddress ip in host.AddressList)
                {
                    if (ip.AddressFamily == AddressFamily.InterNetwork)
                    {
                        localIP = ip.ToString();
                        if (localIP.Contains("192.168.1."))
                            return localIP;
                    }
                }
                return localIP;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
                return null;
            }
        }

        // Method to scan all used ports and return the next available port to be used for peer. 
        public int getNextAvailablePort()
        {
            // ini variables
            List<int> portArray = new List<int>();
            int minPortNr = 1000;

            IPGlobalProperties properties = IPGlobalProperties.GetIPGlobalProperties();
            TcpConnectionInformation[] connections = properties.GetActiveTcpConnections();
            // add all connections to array
            portArray.AddRange(from n in connections where n.LocalEndPoint.Port >= minPortNr select n.LocalEndPoint.Port);
            // temp var
            IPEndPoint[] endPoints;
            // get all tcp listeners
            endPoints = properties.GetActiveTcpListeners();
            // add tcp listeners to array
            portArray.AddRange(from n in endPoints where n.Port >= minPortNr select n.Port);
            // get all upd listeners
            endPoints = properties.GetActiveUdpListeners();
            // add upd listeners to array
            portArray.AddRange(from n in endPoints where n.Port >= minPortNr select n.Port);
            // sort array 
            portArray.Sort();
            // return next available port
            for (int i = minPortNr; i < UInt16.MaxValue; i++)
            {
                if (!portArray.Contains(i))
                    return i;
            }
            return 0;

        }

        // Method to randomly generate a guid.
        public int generateGuid()
        {
            Random rnd = new Random();
            return rnd.Next(127);
        }

        public bool IsIpV4AddressValid(string address) 
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(address))
                {
                    return validIpV4AddressRegex.IsMatch(address.Trim());
                }
                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
                return false;
            }
            
        }

        public bool IsIpV6AddressValid(string address) 
        {   
            try
            {
                if (!string.IsNullOrWhiteSpace(address))     
                {        
                    IPAddress ip;         
                    if (IPAddress.TryParse(address, out ip))         
                    {             
                        return ip.AddressFamily == AddressFamily.InterNetworkV6;        
                    }     
                }     
                return false; 
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
                return false;
            }
        }
    }
}
