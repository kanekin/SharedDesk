﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedDesk.Common
{
    class ByteSeparator
    {
        // byte array to take bytes from
        private byte[] mBuff;
        // bytes taken so far
        private int mSumOfBytes;

        public ByteSeparator(byte[] buff, bool skipFirstByte)
        {
            mBuff = buff;
            mSumOfBytes = (skipFirstByte) ? 1 : 0;
        }

        /// <summary>
        /// Gets the next bytes from the array and saves position for next get.
        /// </summary>
        /// <param name="targetSize">Bytes to take.</param>
        /// <returns>Byte array containing the (next) bytes asked for</returns>
        public byte[] getTargetByte(int targetSize)
        {
            // skips previous bytes and takes the rest
            byte[] bytes = mBuff.Skip(mSumOfBytes).Take(targetSize).ToArray();
            // saves bytes taken so far
            mSumOfBytes += targetSize;
            return bytes;
        }
    }
}
