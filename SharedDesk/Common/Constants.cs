﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedDesk.Common
{
    public static class Constants
    {

        public const bool DEBUG_UDP = false;
        public const bool DEBUG_TCP = false;

        // -----
        // Peer info constants
        // -----

        // IP SIZE
        public const int IP_SIZE = 4;
        // GUID SIZE
        public const int GUID_SIZE = 2;
        // TCP PORT SIZE
        public const int TCP_PORT_SIZE = 4;
        // UDP PORT SIZE
        public const int UDP_PORT_SIZE = 4;
        // USERNAME SIZE
        public const int UNAME_SIZE = 0;
        // PUBLIC KEY SIZE
        public const int KEY_SIZE = 0;
        // TOTAL PEER SIZE
        public const int TOTAL_PEER_SIZE = IP_SIZE + GUID_SIZE + TCP_PORT_SIZE + UDP_PORT_SIZE + UNAME_SIZE + KEY_SIZE;

        // -----
        // File constants
        // -----

        public const int FILEPART_SIZE = 1024 * 1024;
        public const int MD5_SIZE = 16;
        public const int TOTAL_FILEPART_SIZE = FILEPART_SIZE + 1 + MD5_SIZE + 4;
    }
}
