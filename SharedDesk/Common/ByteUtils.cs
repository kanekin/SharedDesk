﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedDesk.Common
{
    class ByteUtils
    {
        /// <summary>
        /// Converts byte array to PeerInfo object.
        /// </summary>
        /// <param name="arrBytes">The byte array to be converted to PeerInfo object.</param>
        /// <returns>PeerInfo object from the byte array.</returns>
        public static PeerInfo byteArrayToPeerInfo(byte[] arrBytes)
        {
            // initializing ByteSeparator without skipping command byte
            ByteSeparator bs = new ByteSeparator(arrBytes, false);
            // getting GUID
            byte[] guid = bs.getTargetByte(Constants.GUID_SIZE);
            // getting IP address
            String ipAddress = byteArrayToIPAddress(bs.getTargetByte(Constants.IP_SIZE));
            // getting udp port
            byte[] portUdpBytes = bs.getTargetByte(Constants.UDP_PORT_SIZE);
            int portUDP = BitConverter.ToInt32(portUdpBytes, 0);
            // getting tcp port
            byte[] portTcpBytes = bs.getTargetByte(Constants.TCP_PORT_SIZE);
            int portTCP = BitConverter.ToInt32(portTcpBytes, 0);
            // constructing PeerInfo object
            PeerInfo peerInfo = new PeerInfo(ipAddress, portUDP, portTCP, "RandomKey", "RandomName", guid);
            return peerInfo;
        }

        /// <summary>
        /// Converts PeerInfo object to byte array.
        /// </summary>
        /// <param name="arrBytes">The PeerInfo object to be converted to byte array.</param>
        /// <returns>Byte array from the PeerInfo object.</returns>
        public static byte[] peerInfoToByteArray(PeerInfo pi)
        {
            // get guid
            byte[] guidBytes = pi.GUID;
            // get IP
            byte[] ipAddressBytes = ipAddressToByteArray(pi.IP);
            // get udp port
            byte[] portUdpBytes = BitConverter.GetBytes(pi.UDPport);
            // get tcp port
            byte[] portTcpBytes = BitConverter.GetBytes(pi.TCPport);
            // combine byte arrays
            byte[] buffer = combineBytes(guidBytes, ipAddressBytes, portUdpBytes, portTcpBytes);
            return buffer;
        }

        /// <summary>
        /// Takes a four length byte array and converts it to string containing IP adress.
        /// </summary>
        /// <param name="arrBytes">The four length byte array to be converted to IP string.</param>
        /// <returns>
        /// The IP string from the byte array.
        /// Null if the byte array doesn't have length of four.
        /// </returns>
        public static String byteArrayToIPAddress(byte[] arrBytes)
        {
            if (arrBytes.Length == 4)
            {
                String result = "";
                for (int i = 0; i < arrBytes.Length; i++)
                {
                    int part = arrBytes[i];
                    result += part.ToString();
                    // if not last digit
                    if (i < 3)
                        result += ".";
                }
                return result;
            }
            else
                return null;
        }

        /// <summary>
        /// Converts a string containing an IP adress to byte array with length of four.
        /// Each index contains a tiny Int with each of the numbers between the '.'.
        /// </summary>
        /// <param name="address">The string containing the IP adress to be converted to byte array.</param>
        /// <returns>Return a four length byte array.</returns>
        public static byte[] ipAddressToByteArray(String address)
        {
            string[] words = address.Split('.');
            byte[][] bytes = new byte[4][];
            for (int i = 0; i < 4; i++)
            {
                int part = Convert.ToInt32(words[i]);
                byte[] row = { Convert.ToByte(part) };
                bytes[i] = row;
            }
            byte[] result = combineBytes(bytes[0], bytes[1], bytes[2], bytes[3]);
            return result;
        }

        /// <summary>
        /// Converts byte array to peer dictionary.
        /// </summary>
        /// <param name="arrBytes">The byte array to convert.</param>
        /// <returns>The peer dictionary from the byte array.</returns>
        public static Dictionary<byte[], PeerInfo> byteArrayToPeerDictionary(byte[] arrBytes)
        {
            Dictionary<byte[], PeerInfo> dictionary = new Dictionary<byte[], PeerInfo>(new ByteArrayComparer());
            int length = arrBytes.Length;
            for (int i = 0; i < arrBytes.Length; i += Constants.TOTAL_PEER_SIZE)
            {
                byte[] peerInfoByte = new byte[Constants.TOTAL_PEER_SIZE];
                Buffer.BlockCopy(arrBytes, i, peerInfoByte, 0, Constants.TOTAL_PEER_SIZE);
                PeerInfo peerInfo = byteArrayToPeerInfo(peerInfoByte);
                dictionary.Add(peerInfo.GUID, peerInfo);
            }
            return dictionary;
        }

        /// <summary>
        /// Converts routing table disctionary to byte array.
        /// </summary>
        /// <param name="peerDictionary">The dictionary to convert.</param>
        /// <returns>Byte array from the peer dictionary.</returns>
        public static byte[] dictionaryToByteArray(Dictionary<byte[], PeerInfo> peerDictionary)
        {
            if (peerDictionary == null)
            {
                // trying to convert a null object
                return null;
            }
            byte[] result = new byte[0];
            foreach (PeerInfo pi in peerDictionary.Values)
            {
                result = combineBytes(result, peerInfoToByteArray(pi));
            }
            return result;
        }

        /// <summary>
        /// Combines 2 byte arrays.
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public static byte[] combineBytes(byte[] first, byte[] second)
        {
            byte[] ret = new byte[first.Length + second.Length];
            Buffer.BlockCopy(first, 0, ret, 0, first.Length);
            Buffer.BlockCopy(second, 0, ret, first.Length, second.Length);
            return ret;
        }
        public static byte[] combineBytes(byte[] first, byte[] second, byte[] third)
        {
            return combineBytes(combineBytes(first, second), third);
        }
        public static byte[] combineBytes(byte[] first, byte[] second, byte[] third, byte[] fourth)
        {
            return combineBytes(combineBytes(first, second, third), fourth);
        }
        public static byte[] combineBytes(byte[] first, byte[] second, byte[] third, byte[] fourth, byte[] fifth)
        {
            return combineBytes(combineBytes(first, second, third, fourth), fifth);
        }
        public static byte[] combineBytes(byte[] first, byte[] second, byte[] third, byte[] fourth, byte[] fifth, byte[] sixth)
        {
            return combineBytes(combineBytes(first, second, third, fourth, fifth), sixth);
        }

        /// <summary>
        /// Converts string to byte array.
        /// </summary>
        /// <param name="str">String to convert to byte array.</param>
        /// <returns>Byte array from the string.</returns>
        public static byte[] convertStringToBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        /// <summary>
        /// Converts byte array to string.
        /// </summary>
        /// <param name="bytes">Byte array to convert to string.</param>
        /// <returns>string from the byte array.</returns>
        public static string convertByteToString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }

        internal static List<byte[]> convertStringListoToByteList(List<string> listGuid)
        {
            List<byte[]> result = new List<byte[]>();
            foreach (string s in listGuid)
            {
                result.Add(convertStringToBytes(s));
            }

            return result;
        }
    }
}
