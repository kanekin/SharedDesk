﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SharedDesk.FileWatcher
{
    class UploadFileInfoQueue
    {
        // lock object
        readonly object _locker = new object();

        // upload threads
        private Thread[] _workers;
        private Queue<string> _itemQ = new Queue<string>();

        // events for new files added, deleted, changed etc
        public delegate void PublicFolderHandler(string file);

        public event PublicFolderHandler newFileAdded;

        public UploadFileInfoQueue(string path, int workerCount)
        {
            if (path == "")
            {
                path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                path += "\\public\\";

            }

            // check if folder exist
            if (Directory.Exists(path) == false)
            {
                Directory.CreateDirectory(path);
            }

            // create a System file watcher to monitor a folder
            FileSystemWatcher watcher = new System.IO.FileSystemWatcher();
            watcher.Filter = "*.*";
            watcher.Path = path;
            watcher.IncludeSubdirectories = true;

            watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite |
                NotifyFilters.FileName | NotifyFilters.DirectoryName;

            // raise event when new file is added
            watcher.Created += watcher_Created;
            //m_Watcher.Changed += new FileSystemEventHandler(OnChanged);
            //m_Watcher.Created += new FileSystemEventHandler(OnChanged);
            //m_Watcher.Deleted += new FileSystemEventHandler(OnChanged);
            //m_Watcher.Renamed += new RenamedEventHandler(OnRenamed);
            watcher.EnableRaisingEvents = true;


            _workers = new Thread[workerCount];

            // Create and start a separate thread for each worker
            for (int i = 0; i < workerCount; i++)
            {

                _workers[i] = new Thread(UploadFileInfo);
                _workers[i].IsBackground = true;
                _workers[i].Start();
            }
        }

        /// <summary>
        /// event when a new file is added to the watched folder
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void watcher_Created(object sender, FileSystemEventArgs e)
        {
            EnqueueItem(e.FullPath);
        }

        public void Shutdown(bool waitForWorkers)
        {
            // Enqueue one null item per worker to make each exit.
            foreach (Thread worker in _workers)
            {
                EnqueueItem(null);
            }

            // Wait for workers to finish
            if (waitForWorkers)
            {
                foreach (Thread worker in _workers)
                {
                    worker.Join();
                }
            }
        }

        public void EnqueueItem(string file)
        {
            lock (_locker)
            {
                _itemQ.Enqueue(file);           // We must pulse because we're
                Monitor.Pulse(_locker);         // changing a blocking condition.
            }
        }

        void UploadFileInfo()
        {
            while (true)                        // Keep consuming until
            {                                   // told otherwise.
                string file;
                lock (_locker)
                {
                    while (_itemQ.Count == 0)
                    {
                        Monitor.Wait(_locker);
                    }
                    file = _itemQ.Dequeue();
                }

                // This signals our exit.
                if (file == null)
                {
                    return;
                }

                // set a max attempts on re-tries if you get 
                // a Exception (IOException or UnauthorizedAccessException)
                int maxAttempts = 5;
                int c = 0;

                FileInfo f = new FileInfo(file);
                while (IsFileLocked(f))
                {
                    Thread.Sleep(500);
                    c++;

                    if (c >= maxAttempts)
                    {
                        return;
                    }
                }

                Console.WriteLine("upload {0}", file);
                newFileAdded.Invoke(file);
            }
        }


        private bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            catch (UnauthorizedAccessException)
            {
                // Three possible reasons for this: 
                // 1. The caller does not have the required permission.
                // 2. path is a directory.
                // 3. path specified a read-only file.
                // 4. can be out of memory (most likely)

                // perhaps file is set to read-only while file transfer takes place
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }
    }
}
