﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using SharedDesk.UDP_protocol;
using SharedDesk.interfaces;
using SharedDesk.Common;

namespace SharedDesk
{
    class FileNotifier
    {
        int maxPeers;
        int remainingPeers;
        string fileFullPath;
        string fileName;
        FileSenderUtils fileSender;
        List<PeerInfo> onlinePeers;
        PeerInfo myInfo;

        public FileNotifier(int maxPeers, string fileFullPath, FileSenderUtils fileSender, PeerInfo myInfo)
        {
            this.maxPeers = maxPeers;
            this.remainingPeers = maxPeers;
            this.fileFullPath = fileFullPath;
            this.fileName = Path.GetFileName(fileFullPath);
            this.fileSender = fileSender;
            this.myInfo = myInfo;
            this.onlinePeers = new List<PeerInfo>();
        }

        // Adds found peers the list
        public void onFound(PeerInfo pInfo, byte[] targetGUID)
        {
            // If peer exists
            if (pInfo.GUID.SequenceEqual(targetGUID))
            {
                // Add to the list
                onlinePeers.Add(pInfo);
            }

            // Decrement counter
            remainingPeers--;

            // If all peers are found
            if (remainingPeers == 0)
            {
                //Begin download
                sendFileInfo();
                //initDownload();
            }
        }

        public void sendFileInfo()
        {
            if (onlinePeers.Count == 0)
            {
                return;
            }

            foreach (PeerInfo targetPeer in onlinePeers)
            {
                // store in xml that the peer selected is now authorized to download this file
                string md5 = "";

                // check that file exist
                if (File.Exists(fileFullPath) == false)
                {
                    Console.WriteLine("Could not find file spesified");
                    return;
                }

                FileInfo file = new FileInfo(fileFullPath);

                byte[] md5Byte = ChecksumCalc.GetMD5Checksum(fileFullPath);
                md5 = ChecksumCalc.getMD5String(md5Byte);

                Peer peer = Peer.getInstance();
                peer.fileHelper.authorizeFileDownloadWithPeer(file, md5, targetPeer);

                //updateMessages(String.Format("Sending file INFO object \"{0}\" to peer guid {1}, ip {2}:{3}", fileName, targetPeer.getGUID, targetPeer.getIP(), targetPeer.getPORT()));
                IPAddress RecevingIp = IPAddress.Parse(targetPeer.IP);

                //// Send file info 
                //// create end point
                IPEndPoint remotePoint = new IPEndPoint(RecevingIp, targetPeer.UDPport);

                fileSender.sendFileInfo(fileFullPath, peer.GUID, remotePoint, "");
            }
        }
    }
}
