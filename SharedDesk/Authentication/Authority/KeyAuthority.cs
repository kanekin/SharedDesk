using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// ShareDesk Namespace
using SharedDesk.Common;

namespace SharedDesk.Authentication.Authority
{
    [Serializable()]
    public class KeyAuthority
    {
        private Dictionary<byte[], String> mPubKeysTable;
        private String myPublicKey;
        private byte[] myGuid;

        public KeyAuthority(byte[] myGuid, String myPublicKey)
        {
            this.mPubKeysTable = new Dictionary<byte[], String>(new ByteArrayComparer());
            this.myGuid = myGuid;
            this.myPublicKey = myPublicKey;

            this.addPubKey(this.myGuid, this.myPublicKey);
        }

        public String getPeerPubKey(byte[] targetGUID)
        {
            return this.mPubKeysTable[targetGUID];
        }

        // Returns the table
        public Dictionary<byte[], String> getPeerKeys()
        {
            return this.mPubKeysTable;
        }

        public String MyPublicKey
        {
            get { return this.myPublicKey; }
            set { myPublicKey = value; }
        }

        public byte[] MyGuid
        {
            get { return this.myGuid; }
            set { myGuid = value; }
        }

        public int countKeyValues()
        {
            return this.mPubKeysTable.Values.Count;
        }

        public void replacePubKey(byte[] guid, String pk)
        {
            if (!containsPubKeyValue(pk))
            {
                String temp = this.mPubKeysTable[guid];
                this.mPubKeysTable.Remove(guid);
                this.mPubKeysTable.Add(guid, pk);
            }
        }

        public void removePubKey(byte[] guid)
        {
            foreach (KeyValuePair<byte[], String> entry in this.mPubKeysTable)
            {
                byte[] i = entry.Key;
                
                if (i == guid)
                {
                    this.mPubKeysTable.Remove(entry.Key);
                    break;
                }
            }
        }

        public void addPubKey(byte[] guid, String pk)
        {
            if (containsPubKeyValue(pk))
            {
                removePubKey(guid);
            }
            if (this.mPubKeysTable.ContainsKey(guid))
            {
                replacePubKey(guid, pk);
            }
            else
            {
                this.mPubKeysTable.Add(guid, pk);
            }
        }

        public bool containsKey(byte[] guid)
        {
            List<byte[]> list = new List<byte[]>(this.mPubKeysTable.Keys);

            // Loop through list
            foreach (byte[] i in list)
            {
                if (i == guid)
                {
                    return true;
                }
            }
            return false;
        }

        public bool containsPubKeyValue(String pk)
        {
            List<String> list = new List<String>(this.mPubKeysTable.Values);
            
            // Loop through list
            foreach (String s in list)
            {
                if (s == pk)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
