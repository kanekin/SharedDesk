﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.IO;
using System.Windows.Forms;
using RSAEncryptionLib;

namespace SharedDesk.Authentication.RSA
{
    public class RSAHelper
    {
        // Instance for RSAEncryption (see library)
        private RSAEncryption mRsa;
        private String mPrivateKey;
        private String mPublicKey;

        // Constructor
        public RSAHelper()
        {
            mRsa = new RSAEncryption();

            createKeyPairs();
        }

        //public String privateKey
        //{
        //    get { return this.mPrivateKey; }
        //    set { this.mPrivateKey = value; }
        //}

        public String publicKey
        {
            get { return this.mPublicKey; }
            set { this.mPublicKey = value; }
        }

        public void createKeyPairs()
        {
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();

            this.mPrivateKey = rsa.ToXmlString(true);
            // File.WriteAllText(pathName + "\\PrivateKey.xml", privateKey);
            mRsa.LoadPublicKey(rsa);

            this.mPublicKey = rsa.ToXmlString(false);
            // File.WriteAllText(pathName + "\\PublicKey.xml", publicKey);
            mRsa.LoadPrivateKey(rsa);

            //Console.WriteLine("Private Key: {0}", this.mPrivateKey);
            //Console.WriteLine("Public Key: {0}", this.mPublicKey);
        }

        // TODO: RSA Encryption
        public void encryptWithPublicKey() { }
        public void encryptWithPrivateKey() { }

        // TODO: RSA Decryption
        public void decryptWithPublicKey() { }
        public void decryptWithPrivateKey() { }

        // Implementation of IDisposable interface,
        // allow you to use this class as: using(RSAEncryption rsa = new RSAEncryption()) { ... }
        public void Dispose()
        {
            mRsa.Dispose();
        }
    }
}
