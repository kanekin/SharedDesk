﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Xml;
using System.IO;
using SharedDesk.Data_storage;

namespace SharedDesk.Authentication.UserMgmt
{
    public class UserHelper
    {
        private Dictionary<int, String> mContactsList;
        private Dictionary<int, String> mIncomingInvitations;
        private Dictionary<int, String> mOutgoingInvitations;
        private String mUsername;
        private int mGuid;
        private Boolean mSavedOnServer;
        private String mContactsListFilePath;
        private XmlDocument rootContactsXml;
        private ErrorLog logFile;

        public UserHelper(int myGuid, String myUsername)
        {
            this.mContactsList = new Dictionary<int, String>();
            this.mIncomingInvitations = new Dictionary<int, String>();
            this.mOutgoingInvitations = new Dictionary<int, String>();

            this.mGuid = myGuid;
            this.mUsername = myUsername;
            this.mSavedOnServer = false;

            this.mContactsListFilePath = "contacts.xml";

            this.addUsernameToContactsList(this.mGuid, this.mUsername);
            this.loadListsFromXML();
        }

        public int getNrOfIncomingInvitations()
        {
            return this.mIncomingInvitations.Count();
        }

        public int getNrOfOutgoingInvitations()
        {
            return this.mOutgoingInvitations.Count();
        }

        public void removeIncomingInvitation(int guid)
        {
            try
            {
                this.mIncomingInvitations.Remove(guid);
                saveListsToXML();
            }
            //catch (KeyNotFoundException ex)
            //{
            //    Console.WriteLine("UserHelper threw an exception in mehtod removeIncomingInvitation :" + ex.Message.ToString());
            //}
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
            }
        }

        public void removeOutgoingInvitation(int guid)
        {
            try
            {
                this.mOutgoingInvitations.Remove(guid);
                saveListsToXML();
            }
            //catch (KeyNotFoundException ex)
            //{
            //    Console.WriteLine("UserHelper threw an exception in method removeOutgoingInvitation :" + ex.Message.ToString());
            //}
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
            }
        }

        public void addIncomingInvitation(int guid, String username)
        {
            if (this.mIncomingInvitations.ContainsKey(guid))
            {
                // already in the list of outstanding invitations either with guid or username
            }
            else
            {
                this.mIncomingInvitations.Add(guid, username);
                saveListsToXML();
            }
        }

        public void addOutgoingInvitation(int guid, String username)
        {
            if (this.mOutgoingInvitations.ContainsKey(guid))
            {
                // already in the list of outstanding invitations
            }
            else
            {
                this.mOutgoingInvitations.Add(guid, username);
                saveListsToXML();
            }
        }
        
        public Boolean loadListsFromXML()
        {
            // Load:
            // contacts list
            // open invitations list
            // sent invitations list
            // TODO : UserHelper -> Boolean whether the username is already on the server list

            if (File.Exists(this.mContactsListFilePath) == false)
            {
                return false;
            }

            String xmlString = "";

            try
            {
                // read from file
                xmlString = File.ReadAllText(this.mContactsListFilePath);

                if (xmlString == "")
                {
                    File.Delete(this.mContactsListFilePath);
                    return false;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
            }

            this.rootContactsXml = new XmlDocument();
            this.rootContactsXml.LoadXml(xmlString);

            foreach (XmlElement node in rootContactsXml.DocumentElement)
            {
                if (node.Name == "Contact")
                {
                    addUsernameToContactsList(Convert.ToInt32(node.LastChild.InnerText), node.FirstChild.InnerText);
                }
                else if (node.Name == "Incoming")
                {
                    if (node.FirstChild != null)
                    {
                        addIncomingInvitation(Convert.ToInt32(node.LastChild.InnerText), node.FirstChild.InnerText);
                    }
                }
                else if (node.Name == "Outgoing")
                {
                    if (node.FirstChild != null)
                    {
                        addOutgoingInvitation(Convert.ToInt32(node.LastChild.InnerText), node.FirstChild.InnerText);
                    }
                }
            }

            return true;
        }

        public void saveListsToXML()
        {
            XmlWriter writer = null;

            try
            {
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Indent = true;

                // Save the document to a file and auto-indent the output.
                writer = XmlWriter.Create(this.mContactsListFilePath, settings);

                writer.WriteStartDocument();
                writer.WriteStartElement("Contacts-List");

                foreach (KeyValuePair<int, String> entry in this.mContactsList)
                {
                    int i = entry.Key;

                    writer.WriteStartElement("Contacts");
                    writer.WriteElementString("Username", entry.Value.ToString());
                    writer.WriteElementString("GUID", entry.Key.ToString());
                    writer.WriteEndElement();
                }

                writer.WriteStartElement("Incoming");

                foreach (KeyValuePair<int, String> entry in this.mIncomingInvitations)
                {
                    int i = entry.Key;

                    writer.WriteElementString("Username", entry.Value.ToString());
                    writer.WriteElementString("GUID", entry.Key.ToString());
                    writer.WriteEndElement();
                }

                writer.WriteEndElement();
                writer.WriteStartElement("Outgoing");

                foreach (KeyValuePair<int, String> entry in this.mOutgoingInvitations)
                {
                    int i = entry.Key;

                    writer.WriteElementString("Username", entry.Value.ToString());
                    writer.WriteElementString("GUID", entry.Key.ToString());
                    writer.WriteEndElement();
                }

                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndDocument();
                writer.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
            }
            finally
            {
                if (writer != null)
                {
                    writer.Dispose();
                    writer.Close();
                }

            }
        }

        // TODO: Implement together with list on server
        public void updateServerList(int guid, String username)
        {
            // this.mSavedOnServer = true;
        }

        public String getNameByGuidFromContactsList(int targetGUID)
        {
            try
            {
                return this.mContactsList[targetGUID];
            }
            //}
            //catch (KeyNotFoundException ex)
            //{
            //    return null;
            //}
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
                return null;
            }
        
        }

        public String getUserByGuidFromIncomingInvitations(int targetGUID)
        {
            try
            {
                return this.mIncomingInvitations[targetGUID];
            }
            //catch (KeyNotFoundException ex)
            //{
            //    return null;
            //}
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
                return null;
            }
        }

        public String getUserByGuidFromOutgoingInvitations(int targetGUID)
        {
            try
            {
                return this.mOutgoingInvitations[targetGUID];
            }
            //catch (KeyNotFoundException ex)
            //{
            //    return null;
            //}
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
                return null;
            }
        }

        // Returns the table
        public Dictionary<int, String> getContactsList()
        {
            return this.mContactsList;
        }

        public Dictionary<int, String> getListOfOutgoingInvitations()
        {
            return this.mOutgoingInvitations;
        }

        public Dictionary<int, String> getListOfIncomingInvitations()
        {
            return this.mIncomingInvitations;
        }

        public String MyUsername
        {
            get { return this.mUsername; }
            set { this.mUsername = value; }
        }

        public int MyGuid
        {
            get { return this.mGuid; }
            set { this.mGuid = value; }
        }

        public int countKeyValuesInContactsList()
        {
            return this.mContactsList.Values.Count;
        }

        public void replaceUsernameInContactsList(int guid, String username)
        {
            if (!containsUsernameValueInContactsList(username))
            {
                String temp = this.mContactsList[guid];
                this.mContactsList.Remove(guid);
                this.mContactsList.Add(guid, username);
                saveListsToXML();
            }
        }

        public void removeUsernameFromContactsList(int guid)
        {
            foreach (KeyValuePair<int, String> entry in this.mContactsList)
            {
                int i = entry.Key;

                if (i == guid)
                {
                    this.mContactsList.Remove(entry.Key);
                    saveListsToXML();
                    break;
                }
            }
        }

        public void addUsernameToContactsList(int guid, String username)
        {
            if (containsUsernameValueInContactsList(username))
            {
                removeUsernameFromContactsList(guid);
                saveListsToXML();
            }
            if (this.mContactsList.ContainsKey(guid))
            {
                replaceUsernameInContactsList(guid, username);
                saveListsToXML();
            }
            else
            {
                this.mContactsList.Add(guid, username);
                saveListsToXML();
            }
        }

        public bool containsKeyInContactsList(int guid)
        {
            List<int> list = new List<int>(this.mContactsList.Keys);

            // Loop through list
            foreach (int i in list)
            {
                if (i == guid)
                {
                    return true;
                }
            }
            return false;
        }

        public bool containsUsernameValueInContactsList(String username)
        {
            List<String> list = new List<String>(this.mContactsList.Values);

            // Loop through list
            foreach (String s in list)
            {
                if (s == username)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
