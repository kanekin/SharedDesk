﻿namespace Sharedesk
{
    partial class Sharedesk_Setup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Sharedesk_Setup));
            this.panelHeader = new System.Windows.Forms.Panel();
            this.labelLocation = new System.Windows.Forms.Label();
            this.pictureBoxLogo = new System.Windows.Forms.PictureBox();
            this.groupBoxInstallFolder = new System.Windows.Forms.GroupBox();
            this.txtBoxInstallLocation = new System.Windows.Forms.TextBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.checkBoxIcon = new System.Windows.Forms.CheckBox();
            this.checkBoxOpenAfterInstall = new System.Windows.Forms.CheckBox();
            this.checkBoxFirewall = new System.Windows.Forms.CheckBox();
            this.checkBoxStartup = new System.Windows.Forms.CheckBox();
            this.btnInstall = new System.Windows.Forms.Button();
            this.groupBoxGeneral = new System.Windows.Forms.GroupBox();
            this.groupBoxAdvanced = new System.Windows.Forms.GroupBox();
            this.checkBoxDebugLogging = new System.Windows.Forms.CheckBox();
            this.labelListenPort = new System.Windows.Forms.Label();
            this.txtBoxListenPort = new System.Windows.Forms.TextBox();
            this.checkBoxEditAdvanced = new System.Windows.Forms.CheckBox();
            this.groupBoxSharedeskFolder = new System.Windows.Forms.GroupBox();
            this.txtBoxSharedeskFolder = new System.Windows.Forms.TextBox();
            this.btnChange = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.panelHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).BeginInit();
            this.groupBoxInstallFolder.SuspendLayout();
            this.groupBoxGeneral.SuspendLayout();
            this.groupBoxAdvanced.SuspendLayout();
            this.groupBoxSharedeskFolder.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelHeader
            // 
            this.panelHeader.BackColor = System.Drawing.Color.Gray;
            this.panelHeader.Controls.Add(this.labelLocation);
            this.panelHeader.Controls.Add(this.pictureBoxLogo);
            this.panelHeader.Location = new System.Drawing.Point(1, 0);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Size = new System.Drawing.Size(382, 65);
            this.panelHeader.TabIndex = 0;
            // 
            // labelLocation
            // 
            this.labelLocation.AutoSize = true;
            this.labelLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLocation.Location = new System.Drawing.Point(11, 26);
            this.labelLocation.Name = "labelLocation";
            this.labelLocation.Size = new System.Drawing.Size(213, 17);
            this.labelLocation.TabIndex = 1;
            this.labelLocation.Text = "Choose Installation Location";
            // 
            // pictureBoxLogo
            // 
            this.pictureBoxLogo.Image = global::SharedDesk.Properties.Resources.logo;
            this.pictureBoxLogo.InitialImage = global::SharedDesk.Properties.Resources.logo;
            this.pictureBoxLogo.Location = new System.Drawing.Point(326, 12);
            this.pictureBoxLogo.Name = "pictureBoxLogo";
            this.pictureBoxLogo.Size = new System.Drawing.Size(45, 45);
            this.pictureBoxLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxLogo.TabIndex = 0;
            this.pictureBoxLogo.TabStop = false;
            // 
            // groupBoxInstallFolder
            // 
            this.groupBoxInstallFolder.Controls.Add(this.txtBoxInstallLocation);
            this.groupBoxInstallFolder.Controls.Add(this.btnBrowse);
            this.groupBoxInstallFolder.Location = new System.Drawing.Point(15, 82);
            this.groupBoxInstallFolder.Name = "groupBoxInstallFolder";
            this.groupBoxInstallFolder.Size = new System.Drawing.Size(358, 50);
            this.groupBoxInstallFolder.TabIndex = 1;
            this.groupBoxInstallFolder.TabStop = false;
            this.groupBoxInstallFolder.Text = "Install Sharedesk in:";
            // 
            // txtBoxInstallLocation
            // 
            this.txtBoxInstallLocation.Location = new System.Drawing.Point(6, 19);
            this.txtBoxInstallLocation.Name = "txtBoxInstallLocation";
            this.txtBoxInstallLocation.ReadOnly = true;
            this.txtBoxInstallLocation.Size = new System.Drawing.Size(264, 20);
            this.txtBoxInstallLocation.TabIndex = 1;
            this.txtBoxInstallLocation.Text = "C:\\Program Files\\Sharedesk\\";
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(277, 19);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(75, 20);
            this.btnBrowse.TabIndex = 0;
            this.btnBrowse.Text = "Browse...";
            this.btnBrowse.UseVisualStyleBackColor = true;
            // 
            // checkBoxIcon
            // 
            this.checkBoxIcon.AutoSize = true;
            this.checkBoxIcon.Checked = true;
            this.checkBoxIcon.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxIcon.Location = new System.Drawing.Point(6, 19);
            this.checkBoxIcon.Name = "checkBoxIcon";
            this.checkBoxIcon.Size = new System.Drawing.Size(123, 17);
            this.checkBoxIcon.TabIndex = 2;
            this.checkBoxIcon.Text = "Create Desktop icon";
            this.checkBoxIcon.UseVisualStyleBackColor = true;
            // 
            // checkBoxOpenAfterInstall
            // 
            this.checkBoxOpenAfterInstall.AutoSize = true;
            this.checkBoxOpenAfterInstall.Checked = true;
            this.checkBoxOpenAfterInstall.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxOpenAfterInstall.Location = new System.Drawing.Point(6, 42);
            this.checkBoxOpenAfterInstall.Name = "checkBoxOpenAfterInstall";
            this.checkBoxOpenAfterInstall.Size = new System.Drawing.Size(182, 17);
            this.checkBoxOpenAfterInstall.TabIndex = 3;
            this.checkBoxOpenAfterInstall.Text = "Open Sharedesk after installation";
            this.checkBoxOpenAfterInstall.UseVisualStyleBackColor = true;
            // 
            // checkBoxFirewall
            // 
            this.checkBoxFirewall.AutoSize = true;
            this.checkBoxFirewall.Checked = true;
            this.checkBoxFirewall.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxFirewall.Location = new System.Drawing.Point(6, 65);
            this.checkBoxFirewall.Name = "checkBoxFirewall";
            this.checkBoxFirewall.Size = new System.Drawing.Size(271, 17);
            this.checkBoxFirewall.TabIndex = 4;
            this.checkBoxFirewall.Text = "Add an exception for Sharedesk in Windows firewall";
            this.checkBoxFirewall.UseVisualStyleBackColor = true;
            // 
            // checkBoxStartup
            // 
            this.checkBoxStartup.AutoSize = true;
            this.checkBoxStartup.Checked = true;
            this.checkBoxStartup.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxStartup.Location = new System.Drawing.Point(6, 88);
            this.checkBoxStartup.Name = "checkBoxStartup";
            this.checkBoxStartup.Size = new System.Drawing.Size(221, 17);
            this.checkBoxStartup.TabIndex = 5;
            this.checkBoxStartup.Text = "Start Sharedesk when Windows starts up";
            this.checkBoxStartup.UseVisualStyleBackColor = true;
            // 
            // btnInstall
            // 
            this.btnInstall.BackColor = System.Drawing.Color.Gray;
            this.btnInstall.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInstall.Location = new System.Drawing.Point(233, 431);
            this.btnInstall.Name = "btnInstall";
            this.btnInstall.Size = new System.Drawing.Size(139, 30);
            this.btnInstall.TabIndex = 6;
            this.btnInstall.Text = "Install";
            this.btnInstall.UseVisualStyleBackColor = false;
            this.btnInstall.Click += new System.EventHandler(this.btnInstall_Click);
            // 
            // groupBoxGeneral
            // 
            this.groupBoxGeneral.Controls.Add(this.comboBox1);
            this.groupBoxGeneral.Controls.Add(this.label1);
            this.groupBoxGeneral.Controls.Add(this.checkBoxIcon);
            this.groupBoxGeneral.Controls.Add(this.checkBoxOpenAfterInstall);
            this.groupBoxGeneral.Controls.Add(this.checkBoxStartup);
            this.groupBoxGeneral.Controls.Add(this.checkBoxFirewall);
            this.groupBoxGeneral.Location = new System.Drawing.Point(15, 194);
            this.groupBoxGeneral.Name = "groupBoxGeneral";
            this.groupBoxGeneral.Size = new System.Drawing.Size(358, 136);
            this.groupBoxGeneral.TabIndex = 7;
            this.groupBoxGeneral.TabStop = false;
            this.groupBoxGeneral.Text = "General";
            // 
            // groupBoxAdvanced
            // 
            this.groupBoxAdvanced.Controls.Add(this.checkBoxDebugLogging);
            this.groupBoxAdvanced.Controls.Add(this.labelListenPort);
            this.groupBoxAdvanced.Controls.Add(this.txtBoxListenPort);
            this.groupBoxAdvanced.Location = new System.Drawing.Point(15, 359);
            this.groupBoxAdvanced.Name = "groupBoxAdvanced";
            this.groupBoxAdvanced.Size = new System.Drawing.Size(358, 66);
            this.groupBoxAdvanced.TabIndex = 8;
            this.groupBoxAdvanced.TabStop = false;
            this.groupBoxAdvanced.Text = "Advanced";
            // 
            // checkBoxDebugLogging
            // 
            this.checkBoxDebugLogging.AutoSize = true;
            this.checkBoxDebugLogging.Checked = true;
            this.checkBoxDebugLogging.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxDebugLogging.Enabled = false;
            this.checkBoxDebugLogging.Location = new System.Drawing.Point(6, 39);
            this.checkBoxDebugLogging.Name = "checkBoxDebugLogging";
            this.checkBoxDebugLogging.Size = new System.Drawing.Size(186, 17);
            this.checkBoxDebugLogging.TabIndex = 2;
            this.checkBoxDebugLogging.Text = "Enable anonymous debug logging";
            this.checkBoxDebugLogging.UseVisualStyleBackColor = true;
            // 
            // labelListenPort
            // 
            this.labelListenPort.AutoSize = true;
            this.labelListenPort.Enabled = false;
            this.labelListenPort.Location = new System.Drawing.Point(3, 16);
            this.labelListenPort.Name = "labelListenPort";
            this.labelListenPort.Size = new System.Drawing.Size(74, 13);
            this.labelListenPort.TabIndex = 1;
            this.labelListenPort.Text = "Listening Port:";
            // 
            // txtBoxListenPort
            // 
            this.txtBoxListenPort.Enabled = false;
            this.txtBoxListenPort.Location = new System.Drawing.Point(83, 13);
            this.txtBoxListenPort.Name = "txtBoxListenPort";
            this.txtBoxListenPort.Size = new System.Drawing.Size(69, 20);
            this.txtBoxListenPort.TabIndex = 0;
            this.txtBoxListenPort.Text = "61005";
            this.txtBoxListenPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // checkBoxEditAdvanced
            // 
            this.checkBoxEditAdvanced.AutoSize = true;
            this.checkBoxEditAdvanced.Location = new System.Drawing.Point(15, 336);
            this.checkBoxEditAdvanced.Name = "checkBoxEditAdvanced";
            this.checkBoxEditAdvanced.Size = new System.Drawing.Size(96, 17);
            this.checkBoxEditAdvanced.TabIndex = 9;
            this.checkBoxEditAdvanced.Text = "Edit Advanced";
            this.checkBoxEditAdvanced.UseVisualStyleBackColor = true;
            this.checkBoxEditAdvanced.CheckedChanged += new System.EventHandler(this.checkBoxEditAdvanced_CheckedChanged);
            // 
            // groupBoxSharedeskFolder
            // 
            this.groupBoxSharedeskFolder.Controls.Add(this.txtBoxSharedeskFolder);
            this.groupBoxSharedeskFolder.Controls.Add(this.btnChange);
            this.groupBoxSharedeskFolder.Location = new System.Drawing.Point(15, 138);
            this.groupBoxSharedeskFolder.Name = "groupBoxSharedeskFolder";
            this.groupBoxSharedeskFolder.Size = new System.Drawing.Size(358, 50);
            this.groupBoxSharedeskFolder.TabIndex = 2;
            this.groupBoxSharedeskFolder.TabStop = false;
            this.groupBoxSharedeskFolder.Text = "Create Sharedesk folder in:";
            // 
            // txtBoxSharedeskFolder
            // 
            this.txtBoxSharedeskFolder.Location = new System.Drawing.Point(6, 19);
            this.txtBoxSharedeskFolder.Name = "txtBoxSharedeskFolder";
            this.txtBoxSharedeskFolder.ReadOnly = true;
            this.txtBoxSharedeskFolder.Size = new System.Drawing.Size(264, 20);
            this.txtBoxSharedeskFolder.TabIndex = 1;
            this.txtBoxSharedeskFolder.Text = "C:\\Users\\Max\\Sharedesk";
            // 
            // btnChange
            // 
            this.btnChange.Location = new System.Drawing.Point(277, 19);
            this.btnChange.Name = "btnChange";
            this.btnChange.Size = new System.Drawing.Size(75, 20);
            this.btnChange.TabIndex = 0;
            this.btnChange.Text = "Browse...";
            this.btnChange.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 108);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Language:";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(67, 105);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(131, 21);
            this.comboBox1.TabIndex = 7;
            this.comboBox1.Text = "English (International)";
            // 
            // Sharedesk_Setup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(385, 470);
            this.Controls.Add(this.groupBoxSharedeskFolder);
            this.Controls.Add(this.checkBoxEditAdvanced);
            this.Controls.Add(this.groupBoxAdvanced);
            this.Controls.Add(this.groupBoxGeneral);
            this.Controls.Add(this.btnInstall);
            this.Controls.Add(this.groupBoxInstallFolder);
            this.Controls.Add(this.panelHeader);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Sharedesk_Setup";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sharedesk | Setup";
            this.panelHeader.ResumeLayout(false);
            this.panelHeader.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).EndInit();
            this.groupBoxInstallFolder.ResumeLayout(false);
            this.groupBoxInstallFolder.PerformLayout();
            this.groupBoxGeneral.ResumeLayout(false);
            this.groupBoxGeneral.PerformLayout();
            this.groupBoxAdvanced.ResumeLayout(false);
            this.groupBoxAdvanced.PerformLayout();
            this.groupBoxSharedeskFolder.ResumeLayout(false);
            this.groupBoxSharedeskFolder.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelHeader;
        private System.Windows.Forms.PictureBox pictureBoxLogo;
        private System.Windows.Forms.Label labelLocation;
        private System.Windows.Forms.GroupBox groupBoxInstallFolder;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.TextBox txtBoxInstallLocation;
        private System.Windows.Forms.CheckBox checkBoxIcon;
        private System.Windows.Forms.CheckBox checkBoxOpenAfterInstall;
        private System.Windows.Forms.CheckBox checkBoxFirewall;
        private System.Windows.Forms.CheckBox checkBoxStartup;
        private System.Windows.Forms.Button btnInstall;
        private System.Windows.Forms.GroupBox groupBoxGeneral;
        private System.Windows.Forms.GroupBox groupBoxAdvanced;
        private System.Windows.Forms.CheckBox checkBoxEditAdvanced;
        private System.Windows.Forms.Label labelListenPort;
        private System.Windows.Forms.TextBox txtBoxListenPort;
        private System.Windows.Forms.CheckBox checkBoxDebugLogging;
        private System.Windows.Forms.GroupBox groupBoxSharedeskFolder;
        private System.Windows.Forms.TextBox txtBoxSharedeskFolder;
        private System.Windows.Forms.Button btnChange;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox1;
    }
}