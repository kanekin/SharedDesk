﻿namespace Sharedesk
{
    partial class Sharedesk_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Sharedesk_Main));
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStripTrayIcon = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItemShow = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemPause = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItemPreferences = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItemExit = new System.Windows.Forms.ToolStripMenuItem();
            this.panelHeader = new System.Windows.Forms.Panel();
            this.labelAppVersion = new System.Windows.Forms.Label();
            this.labelHeading = new System.Windows.Forms.Label();
            this.pictureBoxLogo = new System.Windows.Forms.PictureBox();
            this.btnMessages = new System.Windows.Forms.Button();
            this.btnDevices = new System.Windows.Forms.Button();
            this.btnFolders = new System.Windows.Forms.Button();
            this.btnPublic = new System.Windows.Forms.Button();
            this.btnPreferences = new System.Windows.Forms.Button();
            this.contextMenuStripTrayIcon.SuspendLayout();
            this.panelHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // notifyIcon
            // 
            this.notifyIcon.ContextMenuStrip = this.contextMenuStripTrayIcon;
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "notifyIcon";
            this.notifyIcon.Visible = true;
            this.notifyIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon_MouseDoubleClick);
            // 
            // contextMenuStripTrayIcon
            // 
            this.contextMenuStripTrayIcon.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemShow,
            this.toolStripMenuItemPause,
            this.toolStripSeparator1,
            this.toolStripMenuItemPreferences,
            this.toolStripSeparator2,
            this.toolStripMenuItemExit});
            this.contextMenuStripTrayIcon.Name = "contextMenuStripTrayIcon";
            this.contextMenuStripTrayIcon.Size = new System.Drawing.Size(160, 104);
            // 
            // toolStripMenuItemShow
            // 
            this.toolStripMenuItemShow.Name = "toolStripMenuItemShow";
            this.toolStripMenuItemShow.Size = new System.Drawing.Size(159, 22);
            this.toolStripMenuItemShow.Text = "Show Sharedesk";
            this.toolStripMenuItemShow.Click += new System.EventHandler(this.toolStripMenuItemShow_Click);
            // 
            // toolStripMenuItemPause
            // 
            this.toolStripMenuItemPause.Name = "toolStripMenuItemPause";
            this.toolStripMenuItemPause.Size = new System.Drawing.Size(159, 22);
            this.toolStripMenuItemPause.Text = "Pause Syncing";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(156, 6);
            // 
            // toolStripMenuItemPreferences
            // 
            this.toolStripMenuItemPreferences.Name = "toolStripMenuItemPreferences";
            this.toolStripMenuItemPreferences.Size = new System.Drawing.Size(159, 22);
            this.toolStripMenuItemPreferences.Text = "Preferences";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(156, 6);
            // 
            // toolStripMenuItemExit
            // 
            this.toolStripMenuItemExit.Name = "toolStripMenuItemExit";
            this.toolStripMenuItemExit.Size = new System.Drawing.Size(159, 22);
            this.toolStripMenuItemExit.Text = "Exit Sharedesk";
            this.toolStripMenuItemExit.Click += new System.EventHandler(this.toolStripMenuItemExit_Click);
            // 
            // panelHeader
            // 
            this.panelHeader.BackColor = System.Drawing.Color.Gray;
            this.panelHeader.Controls.Add(this.labelAppVersion);
            this.panelHeader.Controls.Add(this.labelHeading);
            this.panelHeader.Controls.Add(this.pictureBoxLogo);
            this.panelHeader.Location = new System.Drawing.Point(1, 0);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Size = new System.Drawing.Size(632, 63);
            this.panelHeader.TabIndex = 1;
            // 
            // labelAppVersion
            // 
            this.labelAppVersion.AutoSize = true;
            this.labelAppVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAppVersion.Location = new System.Drawing.Point(69, 40);
            this.labelAppVersion.Name = "labelAppVersion";
            this.labelAppVersion.Size = new System.Drawing.Size(42, 15);
            this.labelAppVersion.TabIndex = 7;
            this.labelAppVersion.Text = "v 1.0.0";
            // 
            // labelHeading
            // 
            this.labelHeading.AutoSize = true;
            this.labelHeading.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHeading.Location = new System.Drawing.Point(67, 15);
            this.labelHeading.Name = "labelHeading";
            this.labelHeading.Size = new System.Drawing.Size(116, 25);
            this.labelHeading.TabIndex = 6;
            this.labelHeading.Text = "Sharedesk";
            // 
            // pictureBoxLogo
            // 
            this.pictureBoxLogo.Image = global::SharedDesk.Properties.Resources.logo;
            this.pictureBoxLogo.InitialImage = global::SharedDesk.Properties.Resources.logo;
            this.pictureBoxLogo.Location = new System.Drawing.Point(11, 7);
            this.pictureBoxLogo.Name = "pictureBoxLogo";
            this.pictureBoxLogo.Size = new System.Drawing.Size(50, 50);
            this.pictureBoxLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxLogo.TabIndex = 5;
            this.pictureBoxLogo.TabStop = false;
            // 
            // btnMessages
            // 
            this.btnMessages.BackColor = System.Drawing.Color.White;
            this.btnMessages.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(115)))), ((int)(((byte)(182)))));
            this.btnMessages.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMessages.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMessages.Image = global::SharedDesk.Properties.Resources.icon_messages;
            this.btnMessages.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnMessages.Location = new System.Drawing.Point(1, 327);
            this.btnMessages.Name = "btnMessages";
            this.btnMessages.Padding = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.btnMessages.Size = new System.Drawing.Size(90, 88);
            this.btnMessages.TabIndex = 3;
            this.btnMessages.Text = "Messages";
            this.btnMessages.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnMessages.UseVisualStyleBackColor = false;
            // 
            // btnDevices
            // 
            this.btnDevices.BackColor = System.Drawing.Color.White;
            this.btnDevices.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(115)))), ((int)(((byte)(182)))));
            this.btnDevices.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDevices.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDevices.Image = ((System.Drawing.Image)(resources.GetObject("btnDevices.Image")));
            this.btnDevices.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnDevices.Location = new System.Drawing.Point(1, 239);
            this.btnDevices.Name = "btnDevices";
            this.btnDevices.Padding = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.btnDevices.Size = new System.Drawing.Size(90, 88);
            this.btnDevices.TabIndex = 2;
            this.btnDevices.Text = "Devices";
            this.btnDevices.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnDevices.UseVisualStyleBackColor = false;
            // 
            // btnFolders
            // 
            this.btnFolders.BackColor = System.Drawing.Color.White;
            this.btnFolders.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(115)))), ((int)(((byte)(182)))));
            this.btnFolders.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFolders.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFolders.Image = ((System.Drawing.Image)(resources.GetObject("btnFolders.Image")));
            this.btnFolders.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnFolders.Location = new System.Drawing.Point(1, 151);
            this.btnFolders.Name = "btnFolders";
            this.btnFolders.Padding = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.btnFolders.Size = new System.Drawing.Size(90, 88);
            this.btnFolders.TabIndex = 1;
            this.btnFolders.Text = "Folders";
            this.btnFolders.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnFolders.UseVisualStyleBackColor = false;
            // 
            // btnPublic
            // 
            this.btnPublic.BackColor = System.Drawing.Color.White;
            this.btnPublic.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(115)))), ((int)(((byte)(182)))));
            this.btnPublic.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPublic.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPublic.Image = global::SharedDesk.Properties.Resources.icon_public;
            this.btnPublic.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnPublic.Location = new System.Drawing.Point(1, 63);
            this.btnPublic.Name = "btnPublic";
            this.btnPublic.Padding = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.btnPublic.Size = new System.Drawing.Size(90, 88);
            this.btnPublic.TabIndex = 0;
            this.btnPublic.Text = "Public";
            this.btnPublic.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnPublic.UseVisualStyleBackColor = false;
            // 
            // btnPreferences
            // 
            this.btnPreferences.BackColor = System.Drawing.Color.White;
            this.btnPreferences.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(115)))), ((int)(((byte)(182)))));
            this.btnPreferences.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPreferences.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPreferences.Image = global::SharedDesk.Properties.Resources.icon_options;
            this.btnPreferences.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnPreferences.Location = new System.Drawing.Point(1, 415);
            this.btnPreferences.Name = "btnPreferences";
            this.btnPreferences.Padding = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.btnPreferences.Size = new System.Drawing.Size(90, 88);
            this.btnPreferences.TabIndex = 4;
            this.btnPreferences.Text = "Preferences";
            this.btnPreferences.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnPreferences.UseVisualStyleBackColor = false;
            // 
            // Sharedesk_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(631, 503);
            this.Controls.Add(this.panelHeader);
            this.Controls.Add(this.btnMessages);
            this.Controls.Add(this.btnDevices);
            this.Controls.Add(this.btnFolders);
            this.Controls.Add(this.btnPublic);
            this.Controls.Add(this.btnPreferences);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Sharedesk_Main";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Sharedesk_Main_FormClosing);
            this.contextMenuStripTrayIcon.ResumeLayout(false);
            this.panelHeader.ResumeLayout(false);
            this.panelHeader.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripTrayIcon;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemShow;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemPause;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemPreferences;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemExit;
        private System.Windows.Forms.Panel panelHeader;
        private System.Windows.Forms.PictureBox pictureBoxLogo;
        private System.Windows.Forms.Label labelAppVersion;
        private System.Windows.Forms.Label labelHeading;
        private System.Windows.Forms.Button btnPreferences;
        private System.Windows.Forms.Button btnPublic;
        private System.Windows.Forms.Button btnFolders;
        private System.Windows.Forms.Button btnMessages;
        private System.Windows.Forms.Button btnDevices;
    }
}