﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sharedesk
{
    public partial class Sharedesk_Setup : Form
    {
        private Boolean checkedAdvanced;

        public Sharedesk_Setup()
        {
            InitializeComponent();

            checkedAdvanced = false;
        }

        private void checkBoxEditAdvanced_CheckedChanged(object sender, EventArgs e)
        {
            if (!checkedAdvanced)
            {
                this.checkedAdvanced = true;

                this.labelListenPort.Enabled = true;
                this.txtBoxListenPort.Enabled = true;
                this.checkBoxDebugLogging.Enabled = true;
            }
            else 
            {
                this.checkedAdvanced = false;

                this.labelListenPort.Enabled = false;
                this.txtBoxListenPort.Enabled = false;
                this.checkBoxDebugLogging.Enabled = false;
            }
        }

        private void btnInstall_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
