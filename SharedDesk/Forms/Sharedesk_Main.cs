﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sharedesk
{
    public partial class Sharedesk_Main : Form
    {
        public Sharedesk_Main()
        {
            InitializeComponent();

            // Notification / application icon in taskbar
            notifyIcon.Text = "Sharedesk";
            notifyIcon.Visible = true;

            notifyIcon.BalloonTipIcon = ToolTipIcon.Info;
            notifyIcon.BalloonTipTitle = "Sharedesk";
            notifyIcon.BalloonTipText = "The application is running in the background.";

            notifyIcon.ShowBalloonTip(3000);
        }

        private void toolStripMenuItemShow_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            this.CenterToScreen();
            this.ShowInTaskbar = true;
        }

        private void toolStripMenuItemExit_Click(object sender, EventArgs e)
        {
            this.Close();
            Application.Exit();
        }

        private void notifyIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            this.CenterToScreen();
            this.ShowInTaskbar = true;
        }

        private void Sharedesk_Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (notifyIcon != null)
            {
                notifyIcon.Visible = false;
                notifyIcon.Dispose();
                notifyIcon = null;
            }
        }
    }
}
