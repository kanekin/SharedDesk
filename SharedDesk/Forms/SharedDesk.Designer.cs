﻿namespace SharedDesk
{
    partial class SharedDeskMainApp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.TileSettings = new MetroFramework.Controls.MetroTile();
            this.TileDownloads = new MetroFramework.Controls.MetroTile();
            this.tbMessages = new MetroFramework.Controls.MetroTextBox();
            this.dlgOpenDir = new System.Windows.Forms.FolderBrowserDialog();
            this.tmrEditNotifier = new System.Windows.Forms.Timer(this.components);
            this.btnRequestFileTCPv2 = new MetroFramework.Controls.MetroButton();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.checkNewFilesOnly = new MetroFramework.Controls.MetroCheckBox();
            this.timerAutoDownload = new System.Windows.Forms.Timer(this.components);
            this.dataGridViewFiles = new MetroFramework.Controls.MetroGrid();
            this.Seeds = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.File_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Size = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DownSpeed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TimeLeft = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl1 = new MetroFramework.Controls.MetroTabControl();
            this.tabPage2 = new MetroFramework.Controls.MetroTabPage();
            this.btnSearchPublicFiles = new MetroFramework.Controls.MetroButton();
            this.btnUpdate = new MetroFramework.Controls.MetroButton();
            this.checkAutoDownload = new MetroFramework.Controls.MetroCheckBox();
            this.btnRequestFile = new MetroFramework.Controls.MetroButton();
            this.metroTabPage1 = new MetroFramework.Controls.MetroTabPage();
            this.lbName = new System.Windows.Forms.Label();
            this.tbUsername = new System.Windows.Forms.TextBox();
            this.lbInvitationsOutgoing = new System.Windows.Forms.Label();
            this.btnInvite = new System.Windows.Forms.Button();
            this.lbUsername = new System.Windows.Forms.Label();
            this.btnSTUNReq = new System.Windows.Forms.Button();
            this.labelGUID = new System.Windows.Forms.Label();
            this.listRoutingTable = new System.Windows.Forms.ListBox();
            this.labelIP = new System.Windows.Forms.Label();
            this.labelPort = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lbGUID = new System.Windows.Forms.Label();
            this.tbGUID = new System.Windows.Forms.TextBox();
            this.tbBootPort = new System.Windows.Forms.TextBox();
            this.tbBootIP = new System.Windows.Forms.TextBox();
            this.tbIP = new System.Windows.Forms.TextBox();
            this.tbPORT = new System.Windows.Forms.TextBox();
            this.btnShareFolder = new System.Windows.Forms.Button();
            this.lblIP = new System.Windows.Forms.Label();
            this.lbPort = new System.Windows.Forms.Label();
            this.btnGetRoutingTable = new System.Windows.Forms.Button();
            this.btnLeave = new System.Windows.Forms.Button();
            this.tbTarget = new System.Windows.Forms.TextBox();
            this.btnSendFile = new System.Windows.Forms.Button();
            this.btnFind = new System.Windows.Forms.Button();
            this.buttonPing = new System.Windows.Forms.Button();
            this.lbInvitationsIncoming = new System.Windows.Forms.Label();
            this.lblBootGuid = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.picBoxInvitiationsIncoming = new System.Windows.Forms.PictureBox();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFiles)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.metroTabPage1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxInvitiationsIncoming)).BeginInit();
            this.SuspendLayout();
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.TileSettings);
            this.tabPage1.Controls.Add(this.TileDownloads);
            this.tabPage1.Controls.Add(this.tbMessages);
            this.tabPage1.Location = new System.Drawing.Point(4, 38);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(517, 708);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Configuration";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // TileSettings
            // 
            this.TileSettings.ActiveControl = null;
            this.TileSettings.Location = new System.Drawing.Point(101, 15);
            this.TileSettings.Name = "TileSettings";
            this.TileSettings.Size = new System.Drawing.Size(90, 90);
            this.TileSettings.TabIndex = 53;
            this.TileSettings.Text = "Settings";
            this.TileSettings.UseSelectable = true;
            this.TileSettings.Click += new System.EventHandler(this.TileSettings_Click);
            // 
            // TileDownloads
            // 
            this.TileDownloads.ActiveControl = null;
            this.TileDownloads.Location = new System.Drawing.Point(1, 15);
            this.TileDownloads.Name = "TileDownloads";
            this.TileDownloads.Size = new System.Drawing.Size(90, 90);
            this.TileDownloads.TabIndex = 52;
            this.TileDownloads.Text = "Downloads";
            this.TileDownloads.UseSelectable = true;
            this.TileDownloads.Click += new System.EventHandler(this.TileDownloads_Click);
            // 
            // tbMessages
            // 
            this.tbMessages.Lines = new string[0];
            this.tbMessages.Location = new System.Drawing.Point(201, 13);
            this.tbMessages.MaxLength = 32767;
            this.tbMessages.Multiline = true;
            this.tbMessages.Name = "tbMessages";
            this.tbMessages.PasswordChar = '\0';
            this.tbMessages.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbMessages.SelectedText = "";
            this.tbMessages.Size = new System.Drawing.Size(311, 382);
            this.tbMessages.TabIndex = 23;
            this.tbMessages.UseSelectable = true;
            // 
            // tmrEditNotifier
            // 
            this.tmrEditNotifier.Interval = 5000;
            // 
            // btnRequestFileTCPv2
            // 
            this.btnRequestFileTCPv2.Location = new System.Drawing.Point(412, 371);
            this.btnRequestFileTCPv2.Name = "btnRequestFileTCPv2";
            this.btnRequestFileTCPv2.Size = new System.Drawing.Size(95, 23);
            this.btnRequestFileTCPv2.TabIndex = 40;
            this.btnRequestFileTCPv2.Text = "download TCP";
            this.btnRequestFileTCPv2.UseSelectable = true;
            this.btnRequestFileTCPv2.Click += new System.EventHandler(this.btnRequestFileTCPv2_Click);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseClick);
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            // 
            // checkNewFilesOnly
            // 
            this.checkNewFilesOnly.AutoSize = true;
            this.checkNewFilesOnly.Location = new System.Drawing.Point(184, 375);
            this.checkNewFilesOnly.Name = "checkNewFilesOnly";
            this.checkNewFilesOnly.Size = new System.Drawing.Size(97, 15);
            this.checkNewFilesOnly.TabIndex = 42;
            this.checkNewFilesOnly.Text = "New files only";
            this.checkNewFilesOnly.UseSelectable = true;
            // 
            // timerAutoDownload
            // 
            this.timerAutoDownload.Interval = 5000;
            this.timerAutoDownload.Tick += new System.EventHandler(this.timerAutoDownload_Tick);
            // 
            // dataGridViewFiles
            // 
            this.dataGridViewFiles.AllowUserToResizeRows = false;
            this.dataGridViewFiles.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataGridViewFiles.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewFiles.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dataGridViewFiles.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewFiles.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewFiles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewFiles.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Seeds,
            this.File_Name,
            this.Size,
            this.Status,
            this.DownSpeed,
            this.TimeLeft});
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewFiles.DefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewFiles.EnableHeadersVisualStyles = false;
            this.dataGridViewFiles.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dataGridViewFiles.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dataGridViewFiles.Location = new System.Drawing.Point(-4, 6);
            this.dataGridViewFiles.Name = "dataGridViewFiles";
            this.dataGridViewFiles.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewFiles.RowHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewFiles.RowHeadersVisible = false;
            this.dataGridViewFiles.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridViewFiles.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewFiles.Size = new System.Drawing.Size(514, 359);
            this.dataGridViewFiles.TabIndex = 44;
            this.dataGridViewFiles.SortCompare += new System.Windows.Forms.DataGridViewSortCompareEventHandler(this.dataGridViewFiles_SortCompare);
            // 
            // Seeds
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Seeds.DefaultCellStyle = dataGridViewCellStyle2;
            this.Seeds.HeaderText = "Seeds";
            this.Seeds.Name = "Seeds";
            this.Seeds.ReadOnly = true;
            this.Seeds.Width = 50;
            // 
            // File_Name
            // 
            this.File_Name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.File_Name.FillWeight = 300F;
            this.File_Name.HeaderText = "Name";
            this.File_Name.Name = "File_Name";
            this.File_Name.ReadOnly = true;
            // 
            // Size
            // 
            this.Size.HeaderText = "Size";
            this.Size.Name = "Size";
            this.Size.ReadOnly = true;
            this.Size.Width = 50;
            // 
            // Status
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Status.DefaultCellStyle = dataGridViewCellStyle3;
            this.Status.HeaderText = "Status";
            this.Status.Name = "Status";
            this.Status.ReadOnly = true;
            this.Status.Width = 40;
            // 
            // DownSpeed
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.DownSpeed.DefaultCellStyle = dataGridViewCellStyle4;
            this.DownSpeed.HeaderText = "Down Speed";
            this.DownSpeed.Name = "DownSpeed";
            this.DownSpeed.ReadOnly = true;
            this.DownSpeed.Width = 75;
            // 
            // TimeLeft
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.TimeLeft.DefaultCellStyle = dataGridViewCellStyle5;
            this.TimeLeft.HeaderText = "Time Left";
            this.TimeLeft.Name = "TimeLeft";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.metroTabPage1);
            this.tabControl1.Location = new System.Drawing.Point(12, 60);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.Padding = new System.Drawing.Point(6, 8);
            this.tabControl1.SelectedIndex = 2;
            this.tabControl1.Size = new System.Drawing.Size(525, 750);
            this.tabControl1.TabIndex = 52;
            this.tabControl1.UseSelectable = true;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnRequestFileTCPv2);
            this.tabPage2.Controls.Add(this.btnSearchPublicFiles);
            this.tabPage2.Controls.Add(this.dataGridViewFiles);
            this.tabPage2.Controls.Add(this.btnUpdate);
            this.tabPage2.Controls.Add(this.checkAutoDownload);
            this.tabPage2.Controls.Add(this.btnRequestFile);
            this.tabPage2.Controls.Add(this.checkNewFilesOnly);
            this.tabPage2.Enabled = true;
            this.tabPage2.HorizontalScrollbarBarColor = true;
            this.tabPage2.HorizontalScrollbarHighlightOnWheel = false;
            this.tabPage2.HorizontalScrollbarSize = 10;
            this.tabPage2.Location = new System.Drawing.Point(4, 38);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(517, 708);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Downloads";
            this.tabPage2.UseVisualStyleBackColor = true;
            this.tabPage2.VerticalScrollbarBarColor = true;
            this.tabPage2.VerticalScrollbarHighlightOnWheel = false;
            this.tabPage2.VerticalScrollbarSize = 10;
            this.tabPage2.Visible = false;
            // 
            // btnSearchPublicFiles
            // 
            this.btnSearchPublicFiles.Location = new System.Drawing.Point(311, 400);
            this.btnSearchPublicFiles.Name = "btnSearchPublicFiles";
            this.btnSearchPublicFiles.Size = new System.Drawing.Size(95, 23);
            this.btnSearchPublicFiles.TabIndex = 45;
            this.btnSearchPublicFiles.Text = "Search Public";
            this.btnSearchPublicFiles.UseSelectable = true;
            this.btnSearchPublicFiles.Click += new System.EventHandler(this.btnSearchPublicFiles_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(311, 371);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(95, 23);
            this.btnUpdate.TabIndex = 29;
            this.btnUpdate.Text = "update";
            this.btnUpdate.UseSelectable = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // checkAutoDownload
            // 
            this.checkAutoDownload.AutoSize = true;
            this.checkAutoDownload.Location = new System.Drawing.Point(184, 398);
            this.checkAutoDownload.Name = "checkAutoDownload";
            this.checkAutoDownload.Size = new System.Drawing.Size(105, 15);
            this.checkAutoDownload.TabIndex = 43;
            this.checkAutoDownload.Text = "Auto download";
            this.checkAutoDownload.UseSelectable = true;
            this.checkAutoDownload.CheckedChanged += new System.EventHandler(this.checkAutoDownload_CheckedChanged);
            // 
            // btnRequestFile
            // 
            this.btnRequestFile.Location = new System.Drawing.Point(412, 400);
            this.btnRequestFile.Name = "btnRequestFile";
            this.btnRequestFile.Size = new System.Drawing.Size(95, 23);
            this.btnRequestFile.TabIndex = 30;
            this.btnRequestFile.Text = "segmented";
            this.btnRequestFile.UseSelectable = true;
            this.btnRequestFile.Click += new System.EventHandler(this.btnRequestFile_Click);
            // 
            // metroTabPage1
            // 
            this.metroTabPage1.Controls.Add(this.lbName);
            this.metroTabPage1.Controls.Add(this.tbUsername);
            this.metroTabPage1.Controls.Add(this.lbInvitationsOutgoing);
            this.metroTabPage1.Controls.Add(this.btnInvite);
            this.metroTabPage1.Controls.Add(this.lbUsername);
            this.metroTabPage1.Controls.Add(this.btnSTUNReq);
            this.metroTabPage1.Controls.Add(this.labelGUID);
            this.metroTabPage1.Controls.Add(this.listRoutingTable);
            this.metroTabPage1.Controls.Add(this.labelIP);
            this.metroTabPage1.Controls.Add(this.labelPort);
            this.metroTabPage1.Controls.Add(this.label5);
            this.metroTabPage1.Controls.Add(this.lbGUID);
            this.metroTabPage1.Controls.Add(this.tbGUID);
            this.metroTabPage1.Controls.Add(this.tbBootPort);
            this.metroTabPage1.Controls.Add(this.tbBootIP);
            this.metroTabPage1.Controls.Add(this.tbIP);
            this.metroTabPage1.Controls.Add(this.tbPORT);
            this.metroTabPage1.Controls.Add(this.btnShareFolder);
            this.metroTabPage1.Controls.Add(this.lblIP);
            this.metroTabPage1.Controls.Add(this.lbPort);
            this.metroTabPage1.Controls.Add(this.btnGetRoutingTable);
            this.metroTabPage1.Controls.Add(this.btnLeave);
            this.metroTabPage1.Controls.Add(this.tbTarget);
            this.metroTabPage1.Controls.Add(this.btnSendFile);
            this.metroTabPage1.Controls.Add(this.btnFind);
            this.metroTabPage1.Controls.Add(this.buttonPing);
            this.metroTabPage1.Enabled = true;
            this.metroTabPage1.HorizontalScrollbarBarColor = true;
            this.metroTabPage1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage1.HorizontalScrollbarSize = 10;
            this.metroTabPage1.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage1.Name = "metroTabPage1";
            this.metroTabPage1.Size = new System.Drawing.Size(517, 708);
            this.metroTabPage1.TabIndex = 2;
            this.metroTabPage1.Text = "Testing";
            this.metroTabPage1.VerticalScrollbarBarColor = true;
            this.metroTabPage1.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage1.VerticalScrollbarSize = 10;
            this.metroTabPage1.Visible = true;
            // 
            // lbName
            // 
            this.lbName.AutoSize = true;
            this.lbName.Location = new System.Drawing.Point(458, 215);
            this.lbName.Name = "lbName";
            this.lbName.Size = new System.Drawing.Size(55, 13);
            this.lbName.TabIndex = 80;
            this.lbName.Text = "Username";
            // 
            // tbUsername
            // 
            this.tbUsername.Location = new System.Drawing.Point(461, 231);
            this.tbUsername.Name = "tbUsername";
            this.tbUsername.Size = new System.Drawing.Size(51, 20);
            this.tbUsername.TabIndex = 2;
            // 
            // lbInvitationsOutgoing
            // 
            this.lbInvitationsOutgoing.AutoSize = true;
            this.lbInvitationsOutgoing.Location = new System.Drawing.Point(36, 76);
            this.lbInvitationsOutgoing.Name = "lbInvitationsOutgoing";
            this.lbInvitationsOutgoing.Size = new System.Drawing.Size(0, 13);
            this.lbInvitationsOutgoing.TabIndex = 79;
            // 
            // btnInvite
            // 
            this.btnInvite.Location = new System.Drawing.Point(31, 28);
            this.btnInvite.Name = "btnInvite";
            this.btnInvite.Size = new System.Drawing.Size(121, 23);
            this.btnInvite.TabIndex = 76;
            this.btnInvite.Text = "Invite";
            this.btnInvite.UseVisualStyleBackColor = true;
            this.btnInvite.Click += new System.EventHandler(this.btnInvite_Click);
            // 
            // lbUsername
            // 
            this.lbUsername.AutoSize = true;
            this.lbUsername.Location = new System.Drawing.Point(397, 70);
            this.lbUsername.Name = "lbUsername";
            this.lbUsername.Size = new System.Drawing.Size(55, 13);
            this.lbUsername.TabIndex = 74;
            this.lbUsername.Text = "Username";
            // 
            // btnSTUNReq
            // 
            this.btnSTUNReq.Location = new System.Drawing.Point(342, 314);
            this.btnSTUNReq.Name = "btnSTUNReq";
            this.btnSTUNReq.Size = new System.Drawing.Size(113, 24);
            this.btnSTUNReq.TabIndex = 73;
            this.btnSTUNReq.Text = "getGlobalIP";
            this.btnSTUNReq.UseVisualStyleBackColor = true;
            // 
            // labelGUID
            // 
            this.labelGUID.AutoSize = true;
            this.labelGUID.Location = new System.Drawing.Point(231, 70);
            this.labelGUID.Name = "labelGUID";
            this.labelGUID.Size = new System.Drawing.Size(34, 13);
            this.labelGUID.TabIndex = 54;
            this.labelGUID.Text = "GUID";
            // 
            // listRoutingTable
            // 
            this.listRoutingTable.FormattingEnabled = true;
            this.listRoutingTable.Location = new System.Drawing.Point(180, 86);
            this.listRoutingTable.Name = "listRoutingTable";
            this.listRoutingTable.Size = new System.Drawing.Size(320, 95);
            this.listRoutingTable.TabIndex = 66;
            // 
            // labelIP
            // 
            this.labelIP.AutoSize = true;
            this.labelIP.Location = new System.Drawing.Point(282, 70);
            this.labelIP.Name = "labelIP";
            this.labelIP.Size = new System.Drawing.Size(17, 13);
            this.labelIP.TabIndex = 55;
            this.labelIP.Text = "IP";
            // 
            // labelPort
            // 
            this.labelPort.AutoSize = true;
            this.labelPort.Location = new System.Drawing.Point(358, 70);
            this.labelPort.Name = "labelPort";
            this.labelPort.Size = new System.Drawing.Size(26, 13);
            this.labelPort.TabIndex = 57;
            this.labelPort.Text = "Port";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(199, 361);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 13);
            this.label5.TabIndex = 72;
            this.label5.Text = "GUID";
            // 
            // lbGUID
            // 
            this.lbGUID.AutoSize = true;
            this.lbGUID.Location = new System.Drawing.Point(177, 215);
            this.lbGUID.Name = "lbGUID";
            this.lbGUID.Size = new System.Drawing.Size(34, 13);
            this.lbGUID.TabIndex = 59;
            this.lbGUID.Text = "GUID";
            // 
            // tbGUID
            // 
            this.tbGUID.Location = new System.Drawing.Point(180, 231);
            this.tbGUID.Name = "tbGUID";
            this.tbGUID.Size = new System.Drawing.Size(66, 20);
            this.tbGUID.TabIndex = 52;
            this.tbGUID.Text = "0";
            // 
            // tbBootPort
            // 
            this.tbBootPort.Location = new System.Drawing.Point(380, 187);
            this.tbBootPort.Name = "tbBootPort";
            this.tbBootPort.Size = new System.Drawing.Size(75, 20);
            this.tbBootPort.TabIndex = 1;
            this.tbBootPort.Text = "1200";
            // 
            // tbBootIP
            // 
            this.tbBootIP.Location = new System.Drawing.Point(261, 188);
            this.tbBootIP.Name = "tbBootIP";
            this.tbBootIP.Size = new System.Drawing.Size(103, 20);
            this.tbBootIP.TabIndex = 0;
            this.tbBootIP.Text = "192.168.1.32";
            // 
            // tbIP
            // 
            this.tbIP.Location = new System.Drawing.Point(261, 231);
            this.tbIP.Name = "tbIP";
            this.tbIP.Size = new System.Drawing.Size(104, 20);
            this.tbIP.TabIndex = 53;
            this.tbIP.Text = "127.0.0.1";
            // 
            // tbPORT
            // 
            this.tbPORT.Location = new System.Drawing.Point(380, 231);
            this.tbPORT.Name = "tbPORT";
            this.tbPORT.Size = new System.Drawing.Size(75, 20);
            this.tbPORT.TabIndex = 56;
            this.tbPORT.Text = "8080";
            // 
            // btnShareFolder
            // 
            this.btnShareFolder.Location = new System.Drawing.Point(180, 315);
            this.btnShareFolder.Name = "btnShareFolder";
            this.btnShareFolder.Size = new System.Drawing.Size(156, 23);
            this.btnShareFolder.TabIndex = 68;
            this.btnShareFolder.Text = "Share folder";
            this.btnShareFolder.UseVisualStyleBackColor = true;
            this.btnShareFolder.Click += new System.EventHandler(this.btnShareFolder_Click);
            // 
            // lblIP
            // 
            this.lblIP.AutoSize = true;
            this.lblIP.Location = new System.Drawing.Point(263, 215);
            this.lblIP.Name = "lblIP";
            this.lblIP.Size = new System.Drawing.Size(17, 13);
            this.lblIP.TabIndex = 63;
            this.lblIP.Text = "IP";
            // 
            // lbPort
            // 
            this.lbPort.AutoSize = true;
            this.lbPort.Location = new System.Drawing.Point(378, 215);
            this.lbPort.Name = "lbPort";
            this.lbPort.Size = new System.Drawing.Size(26, 13);
            this.lbPort.TabIndex = 64;
            this.lbPort.Text = "Port";
            // 
            // btnGetRoutingTable
            // 
            this.btnGetRoutingTable.Location = new System.Drawing.Point(180, 257);
            this.btnGetRoutingTable.Name = "btnGetRoutingTable";
            this.btnGetRoutingTable.Size = new System.Drawing.Size(156, 23);
            this.btnGetRoutingTable.TabIndex = 3;
            this.btnGetRoutingTable.Text = "Get routing table";
            this.btnGetRoutingTable.UseVisualStyleBackColor = true;
            this.btnGetRoutingTable.Click += new System.EventHandler(this.btnGetRoutingTable_Click);
            // 
            // btnLeave
            // 
            this.btnLeave.Location = new System.Drawing.Point(342, 257);
            this.btnLeave.Margin = new System.Windows.Forms.Padding(2);
            this.btnLeave.Name = "btnLeave";
            this.btnLeave.Size = new System.Drawing.Size(113, 23);
            this.btnLeave.TabIndex = 65;
            this.btnLeave.Text = "Leave";
            this.btnLeave.UseVisualStyleBackColor = true;
            this.btnLeave.Click += new System.EventHandler(this.btnLeave_Click);
            // 
            // tbTarget
            // 
            this.tbTarget.Location = new System.Drawing.Point(199, 377);
            this.tbTarget.Name = "tbTarget";
            this.tbTarget.Size = new System.Drawing.Size(140, 20);
            this.tbTarget.TabIndex = 4;
            this.tbTarget.Text = "B";
            // 
            // btnSendFile
            // 
            this.btnSendFile.Location = new System.Drawing.Point(180, 286);
            this.btnSendFile.Name = "btnSendFile";
            this.btnSendFile.Size = new System.Drawing.Size(156, 23);
            this.btnSendFile.TabIndex = 6;
            this.btnSendFile.Text = "Share File";
            this.btnSendFile.UseVisualStyleBackColor = true;
            this.btnSendFile.Click += new System.EventHandler(this.btnSendFile_Click);
            // 
            // btnFind
            // 
            this.btnFind.Location = new System.Drawing.Point(361, 374);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(113, 23);
            this.btnFind.TabIndex = 5;
            this.btnFind.Text = "Find";
            this.btnFind.UseVisualStyleBackColor = true;
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // buttonPing
            // 
            this.buttonPing.Location = new System.Drawing.Point(342, 285);
            this.buttonPing.Name = "buttonPing";
            this.buttonPing.Size = new System.Drawing.Size(114, 23);
            this.buttonPing.TabIndex = 67;
            this.buttonPing.Text = "Ping";
            this.buttonPing.UseVisualStyleBackColor = true;
            this.buttonPing.Click += new System.EventHandler(this.buttonPing_Click);
            // 
            // lbInvitationsIncoming
            // 
            this.lbInvitationsIncoming.AutoSize = true;
            this.lbInvitationsIncoming.BackColor = System.Drawing.Color.Red;
            this.lbInvitationsIncoming.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbInvitationsIncoming.ForeColor = System.Drawing.Color.White;
            this.lbInvitationsIncoming.Location = new System.Drawing.Point(468, 25);
            this.lbInvitationsIncoming.Name = "lbInvitationsIncoming";
            this.lbInvitationsIncoming.Size = new System.Drawing.Size(14, 13);
            this.lbInvitationsIncoming.TabIndex = 75;
            this.lbInvitationsIncoming.Text = "0";
            this.lbInvitationsIncoming.Visible = false;
            // 
            // lblBootGuid
            // 
            this.lblBootGuid.AutoSize = true;
            this.lblBootGuid.Location = new System.Drawing.Point(44, 152);
            this.lblBootGuid.Name = "lblBootGuid";
            this.lblBootGuid.Size = new System.Drawing.Size(56, 13);
            this.lblBootGuid.TabIndex = 49;
            this.lblBootGuid.Text = "BootGUID";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStatus});
            this.statusStrip1.Location = new System.Drawing.Point(20, 522);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(505, 22);
            this.statusStrip1.TabIndex = 47;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStatus
            // 
            this.toolStatus.Name = "toolStatus";
            this.toolStatus.Size = new System.Drawing.Size(45, 17);
            this.toolStatus.Text = "Status: ";
            // 
            // picBoxInvitiationsIncoming
            // 
            this.picBoxInvitiationsIncoming.ErrorImage = null;
            this.picBoxInvitiationsIncoming.InitialImage = null;
            this.picBoxInvitiationsIncoming.Location = new System.Drawing.Point(465, 21);
            this.picBoxInvitiationsIncoming.Name = "picBoxInvitiationsIncoming";
            this.picBoxInvitiationsIncoming.Size = new System.Drawing.Size(60, 60);
            this.picBoxInvitiationsIncoming.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBoxInvitiationsIncoming.TabIndex = 53;
            this.picBoxInvitiationsIncoming.TabStop = false;
            this.picBoxInvitiationsIncoming.Click += new System.EventHandler(this.picBoxInvitiationsIncoming_Click);
            // 
            // SharedDeskMainApp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(545, 564);
            this.Controls.Add(this.lbInvitationsIncoming);
            this.Controls.Add(this.picBoxInvitiationsIncoming);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.lblBootGuid);
            this.Controls.Add(this.statusStrip1);
            this.MaximumSize = new System.Drawing.Size(545, 564);
            this.MinimumSize = new System.Drawing.Size(545, 564);
            this.Name = "SharedDeskMainApp";
            this.Text = "SharedDesk";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SharedDeskMainApp_FormClosing);
            this.Load += new System.EventHandler(this.SharedDesk_Load);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFiles)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.metroTabPage1.ResumeLayout(false);
            this.metroTabPage1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxInvitiationsIncoming)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabPage tabPage1;
        //private System.Windows.Forms.TextBox tbMessages;
        private MetroFramework.Controls.MetroTextBox tbMessages;
        private System.Windows.Forms.FolderBrowserDialog dlgOpenDir;
        private System.Windows.Forms.Timer tmrEditNotifier;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.Timer timerAutoDownload;
        private System.Windows.Forms.DataGridViewTextBoxColumn Names;
        private System.Windows.Forms.Label lblBootGuid;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStatus;
        private MetroFramework.Controls.MetroButton btnRequestFileTCPv2;
        private MetroFramework.Controls.MetroCheckBox checkNewFilesOnly;
        private MetroFramework.Controls.MetroGrid dataGridViewFiles;
        private MetroFramework.Controls.MetroTabControl tabControl1;
        private MetroFramework.Controls.MetroTabPage tabPage2;
        private MetroFramework.Controls.MetroButton btnUpdate;
        private MetroFramework.Controls.MetroCheckBox checkAutoDownload;
        private MetroFramework.Controls.MetroButton btnRequestFile;
        private MetroFramework.Controls.MetroTile TileSettings;
        private MetroFramework.Controls.MetroTile TileDownloads;
        private MetroFramework.Controls.MetroTabPage metroTabPage1;
        private System.Windows.Forms.Button btnSTUNReq;
        private System.Windows.Forms.Label labelGUID;
        private System.Windows.Forms.ListBox listRoutingTable;
        private System.Windows.Forms.Label labelIP;
        private System.Windows.Forms.Label labelPort;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lbGUID;
        private System.Windows.Forms.TextBox tbGUID;
        private System.Windows.Forms.TextBox tbBootPort;
        private System.Windows.Forms.TextBox tbBootIP;
        private System.Windows.Forms.TextBox tbIP;
        private System.Windows.Forms.TextBox tbPORT;
        private System.Windows.Forms.Button btnShareFolder;
        private System.Windows.Forms.Label lblIP;
        private System.Windows.Forms.Label lbPort;
        private System.Windows.Forms.Button btnGetRoutingTable;
        private System.Windows.Forms.Button btnLeave;
        private System.Windows.Forms.TextBox tbTarget;
        private System.Windows.Forms.Button btnSendFile;
        private System.Windows.Forms.Button btnFind;
        private System.Windows.Forms.Button buttonPing;
        private System.Windows.Forms.DataGridViewTextBoxColumn Seeds;
        private System.Windows.Forms.DataGridViewTextBoxColumn File_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Size;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
        private System.Windows.Forms.DataGridViewTextBoxColumn DownSpeed;
        private System.Windows.Forms.DataGridViewTextBoxColumn TimeLeft;
        private MetroFramework.Controls.MetroButton btnSearchPublicFiles;
        private System.Windows.Forms.Label lbUsername;
        private System.Windows.Forms.Label lbInvitationsIncoming;
        private System.Windows.Forms.Button btnInvite;
        private System.Windows.Forms.Label lbInvitationsOutgoing;
        private System.Windows.Forms.TextBox tbUsername;
        private System.Windows.Forms.Label lbName;
        private System.Windows.Forms.PictureBox picBoxInvitiationsIncoming;
    }
}
