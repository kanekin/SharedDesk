﻿using MetroFramework.Forms;
using SharedDesk.Data_storage;
using SharedDesk.interfaces;
using SharedDesk.TCP_file_transfer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Security;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using SharedDesk.Common;

namespace SharedDesk
{
    public partial class SharedDeskMainApp : MetroForm
    {
        // Peer Object
        Peer peer;
        // Var for storing peer guid
        string mGuid;

        // Var for storing peer port
        int port;
        // Variable for storing local IP
        private IPAddress ip;

        // Instance for helper functions
        private Utilities helperFuncs;

        private bool isShown = false;

        // list of avaliable files
        // same list that is displayed in the data grid view  
        List<FileInfoP2P> files = null;

        private int activeDownloads = 0;

        private ErrorLog logFile;

        public SharedDeskMainApp()
        {
            InitializeComponent();
            logFile = new ErrorLog();

            Color color = System.Drawing.ColorTranslator.FromHtml("#c72f2f");
            lbInvitationsIncoming.BackColor = color;

            picBoxInvitiationsIncoming.Visible = false;

            //TODO: why do we have to pass empty List???
            // Creating Peer
            peer = Peer.getInstance();
            peer.init(new List<PeerInfo>());

            // Subscribing to events
            subscribeToListener();

            helperFuncs = new Utilities();

            // get local IP address
            ip = IPAddress.Parse(helperFuncs.LocalIPAddress());

            // set own port and boot peer ip and port
            tbBootIP.Text = peer.fileHelper.getBootPeerIP();
            tbPORT.Text = peer.fileHelper.getPort().ToString();
            tbBootPort.Text = peer.fileHelper.getBootPeerPort().ToString();

            // get guid
            mGuid = peer.fileHelper.getGuid();
            if (mGuid == "")
            {
                // TODO: 
                // create new guid 
                mGuid = "B";

                // use default
                tbPORT.Text = "1000";
                tbBootPort.Text = "1000";
                tbBootIP.Text = ip.ToString();

            }

            // show in textbox
            tbGUID.Text = mGuid.ToString();

            // show in textbox
            tbIP.Text = ip.ToString();
            FileTransferEvents.downloadComplete += FileTransferEvents_downloadComplete;
            FileTransferEvents.progress += FileTransferEvents_progress;
            FileTransferEvents.speedChange += FileTransferEvents_speedChange;
            FileTransferEvents.transferStarted += FileTransferEvents_transferStarted;
            FileTransferEvents.downloadComplete += FileTransferEvents_downloadComplete;

            // a notify icon in the corner

            Icon myIcon = Icon.ExtractAssociatedIcon(Application.ExecutablePath);
            //notifyicon = notifyIcon1;
            notifyIcon1.Text = "Shared Desk";
            notifyIcon1.Visible = true;
            //notifyicon.BalloonTipClicked += notifyicon_BalloonTipClicked;
            notifyIcon1.Icon = myIcon;

            if (isShown == true)
            {
                this.WindowState = FormWindowState.Minimized;
            }
        }

        private void MainForm_v2_Resize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            {
                isShown = true;
            }
            else if (WindowState == FormWindowState.Normal)
            {
                isShown = false;
            }
        }

        void FileTransferEvents_speedChange(ChangeInFileDownload e)
        {
            updateListBoxOfTransfers(e.filename, e.speed, e.timeLeft);
        }

        void FileTransferEvents_progress(ChangeInFileDownload e)
        {
            updateListBoxOfTransfers(e.filename, e.percent);
        }

        /// <summary>
        /// Find the index of a row in the date grid view
        /// given name of the file
        /// </summary>
        /// <returns></returns>
        private int getIndexOfFileInDataView(string name)
        {
            for (int i = 0; i < dataGridViewFiles.Rows.Count; i++)
            {

                string currentRow = dataGridViewFiles.Rows[i].Cells[1].FormattedValue.ToString();
                bool result = name.Equals(currentRow, StringComparison.Ordinal);

                if (result)
                {
                    return i;
                }
            }

            return -1;
        }

        private void updateListBoxOfTransfers(string file, string speed, string timeLeft)
        {
            if (file == "")
            {
                return;
            }

            // since different threads are dealing with file transfers this updates
            // is not executed by the thread that holds the form, so a "invoke" is requried
            // this solves it... 
            if (dataGridViewFiles.InvokeRequired)
            {
                this.Invoke(new Action<string, string, string>(updateListBoxOfTransfers), new object[] { file, speed, timeLeft });
                return;
            }
            else
            {
                if (dataGridViewFiles != null)
                {
                    // TODO: 
                    // add download progress and download speed to dataGridViewFiles

                    int index = getIndexOfFileInDataView(file);

                    if (index < 0)
                    {
                        return;
                    }

                    try
                    {
                        dataGridViewFiles[4, index].Value = speed;
                        dataGridViewFiles[5, index].Value = timeLeft;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                        logFile.WriteErrorLog(ex.ToString());
                    }

                }
            }

        }

        private void updateListBoxOfTransfers(string file, byte progress)
        {

            if (file == "")
            {
                return;
            }

            // since different threads are dealing with file transfers this updates
            // is not executed by the thread that holds the form, so a "invoke" is requried
            // this solves it... 
            if (dataGridViewFiles.InvokeRequired)
            {
                this.Invoke(new Action<string, byte>(updateListBoxOfTransfers), new object[] { file, progress });
                return;
            }
            else
            {
                if (dataGridViewFiles != null)
                {
                    // TODO: 
                    // add download progress and download speed to dataGridViewFiles

                    int index = getIndexOfFileInDataView(file);

                    if (index < 0)
                    {
                        return;
                    }

                    try
                    {
                        dataGridViewFiles[3, index].Value = progress.ToString() + " %";
                    }
                    catch (ArgumentOutOfRangeException)
                    {
                        Console.WriteLine("index of file is out of range in data view table!");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                        logFile.WriteErrorLog(ex.ToString());
                    }

                }
            }

        }

        void FileTransferEvents_downloadComplete(ChangeInFileDownload e)
        {

            // update table
            updateListBoxOfTransfers(e.filename, 100);
            updateListBoxOfTransfers(e.filename, "", "");

            // notify icon
            notifyIcon1.BalloonTipTitle = "Received File";
            notifyIcon1.BalloonTipText = e.filename;
            notifyIcon1.BalloonTipClicked += notifyIcon1_BalloonTipClicked;
            notifyIcon1.ShowBalloonTip(2000);

            if (activeDownloads > 0)
            {
                activeDownloads--;
            }

        }

        void notifyIcon1_BalloonTipClicked(object sender, EventArgs e)
        {
            string saveFolder = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            saveFolder += "\\Downloads\\";

            // opens the folder in explorer
            Process.Start(@saveFolder);

        }

        void FileTransferEvents_transferStarted(ChangeInFileDownload e)
        {
            // update table
            updateListBoxOfTransfers(e.filename, 0);

            // notify icon
            notifyIcon1.BalloonTipTitle = "Sending File";
            notifyIcon1.BalloonTipText = e.filename;
            notifyIcon1.ShowBalloonTip(2000);

        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void btnGetRoutingTable_Click(object sender, EventArgs e)
        {
            try
            {
                // get peer info from form
                string guid = tbGUID.Text;
                string ip = tbIP.Text;
                int port = Convert.ToInt32(tbPORT.Text);

                // get boor peer info from form
                string bootPeerIp = tbBootIP.Text;
                int bootPeerPort = Convert.ToInt32(tbBootPort.Text);

                // store info
                peer.fileHelper.setGuid(guid);
                peer.fileHelper.setPort(port);
                peer.fileHelper.setBootPeerIP(bootPeerIp);
                peer.fileHelper.setBootPeerPort(bootPeerPort);

                // ERROR?
                // setting GUID to 0 and "" ???
                peer.setBootPeer(0, tbBootIP.Text, Convert.ToInt32(tbBootPort.Text), "");
                peer.init(guid, ip, port);
                peer.sendRequestRoutingTable();

                if (tbUsername.Text == "")
                {
                    peer.updateUsername("UsernameNotSet");
                }
                else
                {
                    peer.updateUsername(tbUsername.Text);
                }
                toolStatus.Text = "Status: Sent routing table request";

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
            }


        }

        /// <summary>
        /// HANDLERS
        /// </summary>
        public void subscribeToListener()
        {
            peer.updateTable += new Peer.handlerUpdatedTable(handleUpdatedTable);
            peer.updateMyInfoTB += new Peer.handlerUpdatedMyInfo(handleUpdatedMyInfo);
            peer.updateMessages += new Peer.handlerUpdateMessages(handleUpdatedMessages);
            peer.updateLabelIncomingInvitations += new Peer.handlerUpdatedLabelIncomingInvitations(handleUpdatedLabelIncomingInvitations);
            peer.updateLabelOutgoingInvitations += new Peer.handlerUpdatedLabelOutgoingInvitations(handleUpdatedLabelOutgoingInvitations);
        }

        private void handleUpdatedTable()
        {
            Dictionary<byte[], string> table = new Dictionary<byte[], string>(new ByteArrayComparer());
            listRoutingTable.Invoke(new MethodInvoker(() => listRoutingTable.DataSource = null));
            foreach (KeyValuePair<byte[], PeerInfo> entry in peer.getRoutingTable.Table)
            {
                byte[] keyArr = entry.Key;
                //table.Add(entry.Key, Convert.ToString(keyArr[0], 2).PadLeft(8, '0') + Convert.ToString(keyArr[1], 2).PadLeft(8, '0') + "\t" + Convert.ToString(entry.Value.ByteGUID[0], 2).PadLeft(8, '0') + Convert.ToString(entry.Value.ByteGUID[1], 2).PadLeft(8, '0') + "\t" + entry.Value.getIP() + " : " + entry.Value.getUDPport() + "   " + entry.Value.Username);
                table.Add(entry.Key, BitConverter.ToInt16(entry.Key, 0) + "\t" + BitConverter.ToInt16(entry.Value.GUID, 0) + "\t" + entry.Value.IP + " : " + entry.Value.UDPport + "   " + entry.Value.Username);
            }
            listRoutingTable.Invoke(new MethodInvoker(() => listRoutingTable.DataSource = new BindingSource(table, null)));
            listRoutingTable.Invoke(new MethodInvoker(() => listRoutingTable.DisplayMember = "Value"));
        }

        private void handleUpdatedLabelIncomingInvitations(int number)
        {
            if (number == 0)
            {
                lbInvitationsIncoming.Invoke(new MethodInvoker(() => lbInvitationsIncoming.Visible = false));
                lbInvitationsIncoming.Invoke(new MethodInvoker(() => lbInvitationsIncoming.Text = String.Empty));

                picBoxInvitiationsIncoming.Invoke(new MethodInvoker(() => picBoxInvitiationsIncoming.Visible = false));
            }
            else
            {
                lbInvitationsIncoming.Invoke(new MethodInvoker(() => lbInvitationsIncoming.Visible = true));
                lbInvitationsIncoming.Invoke(new MethodInvoker(() => lbInvitationsIncoming.Text = number.ToString()));
                picBoxInvitiationsIncoming.Invoke(new MethodInvoker(() => picBoxInvitiationsIncoming.Visible = true));
            }
        }

        private void handleUpdatedLabelOutgoingInvitations(int number)
        {
            if (number == 0)
            {
                lbInvitationsOutgoing.Invoke(new MethodInvoker(() => lbInvitationsOutgoing.Text = String.Empty));
                lbInvitationsOutgoing.Invoke(new MethodInvoker(() => lbInvitationsOutgoing.Visible = false));
            }
            else
            {
                lbInvitationsOutgoing.Invoke(new MethodInvoker(() => lbInvitationsOutgoing.Text = "Outgoing Invitations: " + number.ToString()));
                lbInvitationsOutgoing.Invoke(new MethodInvoker(() => lbInvitationsOutgoing.Visible = true));
            }
        }

        private void handleUpdatedMyInfo()
        {
            tbIP.Invoke(new MethodInvoker(delegate { tbIP.Text = peer.IP; }));
            tbPORT.Invoke(new MethodInvoker(delegate { tbPORT.Text = peer.UDPport.ToString(); }));
        }

        private void handleUpdatedMessages(string msg)
        {
            tbMessages.Invoke(new MethodInvoker(() => tbMessages.Text += msg + Environment.NewLine));
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            try
            {
                byte[] target = ByteUtils.convertStringToBytes(tbTarget.Text);
                peer.findTarget(target);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
            }
        }

        private void btnSendFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Multiselect = true;

            DialogResult dr = ofd.ShowDialog();

            if (dr != DialogResult.OK)
            {
                // No file was selected
                return;
            }


            // Allows for selecting multiple files
            foreach (String file in ofd.FileNames)
            {
                try
                {
                    Console.WriteLine("Requesting file INFO object {0}", file);

                    // attemt to send file info object
                    peer.sendFileInfo(Path.GetFileName(file), file);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                    logFile.WriteErrorLog(ex.ToString());
                }

            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            updateDataTable();
        }

        private void updateDataTable()
        {
            if (checkNewFilesOnly.Checked)
            {
                // get only "new" files
                files = peer.fileHelper.getAvaliableFileDownloads(FileInfoP2P.Status.newFile);

            }
            else
            {
                // display all files in knownFiles.xml
                files = peer.fileHelper.getAvaliableFileDownloads();
            }

            updateFileListBox(files);
        }

        private void updateFileListBox(List<FileInfoP2P> files)
        {
            if (files == null)
            {
                toolStatus.Text = String.Format("Found 0 avaliable files for download");
                return;
            }

            dataGridViewFiles.Rows.Clear();
            for (int i = 0; i < files.Count; i++)
            {
                // set status
                string fileDownloadStatus = "";
                if (files[i].status == FileInfoP2P.Status.newFile)
                {
                    fileDownloadStatus = "0 %";
                }
                else if (files[i].status == FileInfoP2P.Status.complete)
                {
                    fileDownloadStatus = "100 %";
                }
                //else if (files[i].status == FileInfoP2P.Status.downloading)
                //{
                //    fileDownloadStatus = "starting...";
                //}

                string humanReadableFileSize = convertFileSizeToHumanReadableFormat(files[i].size);
                //List<int> peers = filehelper.findPeersWithFile(files[i].getMd5AsString());
                //dataGridViewFiles.Rows.Insert(0, peers.Count, files[i].name, humanReadableFileSize, fileDownloadStatus, "", "");
                //peers = null;

                dataGridViewFiles.Rows.Insert(0, 0, files[i].name, humanReadableFileSize, fileDownloadStatus, "", "");
            }

            toolStatus.Text = String.Format("Found {0} avaliable files for download", files.Count);
        }

        private string convertFileSizeToHumanReadableFormat(double size)
        {
            string[] sizes = { "b", "KB", "MB", "GB" };
            int order = 0;
            while (size >= 1024 && order + 1 < sizes.Length)
            {
                order++;
                size = size / 1024.0;
            }

            // if GB. Display like 1.4 GB. 
            if (order == 3)
            {

                return String.Format(CultureInfo.InvariantCulture, "{0:0.#} {1}", size, sizes[order]);
            }

            // if b, KB or MB. Display as 104 KB
            return String.Format(CultureInfo.InvariantCulture, "{0:0} {1}", size, sizes[order]);
        }

        private void btnRequestFileTCPv2_Click(object sender, EventArgs e)
        {
            //int index = listAvaliableFiles.SelectedIndex;
            int indexOfFilename = dataGridViewFiles.CurrentCell.RowIndex;
            string filename = dataGridViewFiles[1, indexOfFilename].FormattedValue.ToString();

            // request file from peers
            requestFileFromPeers(filename);
        }

        private void btnRequestFile_Click(object sender, EventArgs e)
        {
            int index = dataGridViewFiles.CurrentCell.RowIndex;
            if (index == -1)
            {
                toolStatus.Text = "Error: No file selected!";
                return;
            }

            // get selected file
            string file = dataGridViewFiles[1, index].FormattedValue.ToString();

            // get md5 of file
            string md5 = peer.fileHelper.getMd5OfFile(file);

            if (md5 == "")
            {
                toolStatus.Text = "Error: Could not find md5 of file";
                return;
            }

            // get peers with the file

            List<string> listGuid = peer.fileHelper.findPeersWithFile(md5).Distinct().ToList();

            if (listGuid == null || listGuid.Count == 0)
            {
                string error = "Error: Could not find any peers with that file.";
                toolStatus.Text = error;
                Console.WriteLine(error);
                return;
            }

            // print result 
            tbMessages.Text += String.Format("Found {0} peers with file {1}", listGuid.Count, file) + Environment.NewLine;
            foreach (string guid in listGuid)
            {
                tbMessages.Text += "guid: " + guid.ToString() + Environment.NewLine;
            }

            List<byte[]> bytelist = ByteUtils.convertStringListoToByteList(listGuid);
            peer.findFilePeers(md5, bytelist);
        }

        private void requestFileFromPeers(string fileName)
        {
            Console.WriteLine("Calculating MD5 hash of file {0}", fileName);

            // get md5 of file
            string md5 = peer.fileHelper.getMd5OfFile(fileName);

            if (md5 == "")
            {
                toolStatus.Text = "Error: Could not find md5 of file";
                return;
            }

            // get peers with the file
            List<string> listGuid = peer.fileHelper.findPeersWithFile(md5).Distinct().ToList();

            if (listGuid == null || listGuid.Count == 0)
            {
                toolStatus.Text = "Error: Could not find any peers with that file.";
                return;
            }

            // print result 
            tbMessages.Text += String.Format("Found {0} peers with file {1}", listGuid.Count, fileName) + Environment.NewLine;
            foreach (string guid in listGuid)
            {
                tbMessages.Text += "guid: " + guid + Environment.NewLine;
            }

            // send file request to peers
            bool result = false;
            int peerIndex = 0;

            // will first search for peer, if peer is not found it will return false
            // will then attempt to find and connect to the next peer
            while (result == false && listGuid.Count > peerIndex)
            {

                Stopwatch watch = new Stopwatch();

                try
                {

                    watch.Start();

                    Console.WriteLine("Requesting file {0} from peer {1}", fileName, listGuid[peerIndex]);

                    // Attempting to send file, also time this method 
                    // seems to cause the form to freeze, so parts of the file transfer can take too long

                    byte[] byteGUID = ByteUtils.convertStringToBytes(listGuid[peerIndex]);
                    result = peer.sendFileRequest(fileName, md5, byteGUID);

                    // sent one file info object
                    watch.Stop();
                    var elapsedMs = watch.ElapsedMilliseconds;

                    Console.WriteLine("Sending file request took {0} miliseconds", elapsedMs);

                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                    logFile.WriteErrorLog(ex.ToString());
                    result = true;
                }
                peerIndex++;
            }
        }

        private void notifyIcon1_MouseClick(object sender, MouseEventArgs e)
        {
            string saveFolder = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            saveFolder += "\\Downloads\\";

            // opens the folder in explorer
            Process.Start(@saveFolder);

            if (isShown == false)
            {
                this.WindowState = FormWindowState.Normal;
            }
        }

        private void TileSettings_Click(object sender, EventArgs e)
        {

        }

        private void TileDownloads_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tabPage2;
        }

        private void SharedDesk_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (notifyIcon1 != null)
            {
                notifyIcon1.Visible = false;
                notifyIcon1.Icon = null; // required to make icon disappear
                notifyIcon1.Dispose();
                notifyIcon1 = null;
            }
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateDataTable();
        }

        private void SharedDesk_Load(object sender, EventArgs e)
        {
            updateDataTable();
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            string saveFolder = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            saveFolder += "\\Downloads\\";

            // opens the folder in explorer
            Process.Start(@saveFolder);

        }

        private void checkAutoDownload_CheckedChanged(object sender, EventArgs e)
        {
            if (checkAutoDownload.Checked)
            {
                timerAutoDownload.Start();
            }
            else
            {
                timerAutoDownload.Stop();
            }

        }

        private void timerAutoDownload_Tick(object sender, EventArgs e)
        {
            // update form
            updateDataTable();

            // set a 1 download at a time
            if (activeDownloads > 0)
            {
                return;
            }

            // find new files
            List<FileInfoP2P> newFiles = peer.fileHelper.getAvaliableFileDownloads(FileInfoP2P.Status.newFile);
            if (newFiles.Count == 0)
            {
                return;
            }

            string md5 = ChecksumCalc.getMD5String(newFiles[0].md5);

            // download file
            List<string> listGuid = peer.fileHelper.findPeersWithFile(md5).Distinct().ToList();

            if (listGuid == null || listGuid.Count == 0)
            {
                toolStatus.Text = "Error: Could not find any peers with that file.";
                return;
            }

            // print result 
            tbMessages.Text += String.Format("Found {0} peers with file {1}", listGuid.Count, newFiles[0].name) + Environment.NewLine;
            foreach (string guid in listGuid)
            {
                tbMessages.Text += "guid: " + guid.ToString() + Environment.NewLine;
            }

            activeDownloads++;
            List<byte[]> bytelist = ByteUtils.convertStringListoToByteList(listGuid);
            peer.findFilePeers(md5, bytelist);

        }

        private void dataGridViewFiles_SortCompare(object sender, DataGridViewSortCompareEventArgs e)
        {

            if (e.Column.Index == 2)
            {
                e.SortResult = compareFileSize(e.CellValue1, e.CellValue2);
                e.Handled = true;
            }

        }

        public void UpdateDetails()
        {
            string customValue = string.Empty;
            string secondValue = string.Empty;
            string appData = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
            appData = Path.Combine(appData, "SharedDesk");
            string appDataFile = Path.Combine(appData, "SharedDeskAppData.xml");
            if (File.Exists(appDataFile))
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(appDataFile);

                XmlNode node = doc.DocumentElement.SelectSingleNode("/Settings/GUID/Guid");

                customValue = node.InnerText;

                node = doc.DocumentElement.SelectSingleNode("/Settings/PORT/port");

                secondValue = node.InnerText;
                //customValue = File.ReadAllText(appDataFile);
            }

            port = 1200;

            // generate guid (int)
            mGuid = "a";

            // show in textbox
            tbGUID.Text = mGuid;

            tbPORT.Text = port.ToString();

        }


        private int compareFileSize(object a, object b)
        {
            string aString = (string)a;
            string bString = (string)b;

            // Set that MB is bigger than KB and b
            if (aString.Contains("MB") && (bString.Contains("KB") || bString.Contains("b")))
            {
                return 1;
            }

            if (bString.Contains("MB") && (aString.Contains("KB") || aString.Contains("b")))
            {
                return -1;
            }

            // Set that KB is bigger than b
            if (aString.Contains("KB") && bString.Contains("b"))
            {
                return 1;
            }

            if (bString.Contains("KB") && aString.Contains("b"))
            {
                return -1;
            }

            // GB bigger than MB, KB and Byte
            if (aString.Contains("GB") && (bString.Contains("MB") || bString.Contains("KB") || bString.Contains("b")))
            {
                return 1;
            }

            if (bString.Contains("GB") && (aString.Contains("MB") || aString.Contains("KB") || aString.Contains("b")))
            {
                return -1;
            }

            // get only the number in the cell, ignore everything else
            int aValue = Convert.ToInt32(Regex.Replace(aString, "[^0-9]", ""));
            int bValue = Convert.ToInt32(Regex.Replace(bString, "[^0-9]", ""));

            return aValue.CompareTo(bValue);

        }

        private void btnSearchPublicFiles_Click(object sender, EventArgs e)
        {
            peer.sendSearchRequest();
        }

        private void buttonPing_Click(object sender, EventArgs e)
        {

        }

        private void btnShareFolder_Click(object sender, EventArgs e)
        {

        }

        private void btnInvite_Click(object sender, EventArgs e)
        {
            // collect parameters
            //int tempGuid = Convert.ToInt32(tbGUID.Text);
            //String tempUsername = tbUsername.Text;
            //String tempIP = tbIP.Text;
            //int tempPort = Convert.ToInt32(tbPORT.Text);

            //if (tempGuid != peer.GUID)
            //{
            //    peer.sendInvitationToUser(tempGuid, tempUsername, tempIP, tempPort);
            //}
        }

        private void picBoxInvitiationsIncoming_Click(object sender, EventArgs e)
        {

        }

        public void respondToInvitation(int guid, Boolean answer)
        {
            if (answer)
            {
                peer.respondToInvitationFromUser(Convert.ToInt32(tbGUID.Text), true, tbIP.Text, Convert.ToInt32(tbPORT.Text));
            }
            else
            {
                peer.respondToInvitationFromUser(Convert.ToInt32(tbGUID.Text), false, tbIP.Text, Convert.ToInt32(tbPORT.Text));
            }
        }

        private void SharedDeskMainApp_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (peer != null)
            {
                peer.sendLeaveRequests();
                peer.saveContacts();
            }
        }

        private void btnLeave_Click(object sender, EventArgs e)
        {
            peer.sendLeaveRequests();
        }
    }
}
