﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedDesk.MovingAverage
{
    interface IMovingAverage
    {
        float Average { get; }
        void AddSample(float val);
        void ClearSamples();
        void InitializeSamples(float val);
    }
}
