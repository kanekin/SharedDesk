﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedDesk
{
    public class PeerInfo
    {
        private string mIP;
        private int mUDPport;
        private int mTCPport;
        private String mPublickey;
        private String mUsername;
        private byte[] mGUID;

        public PeerInfo(string ip, int UDPport, int TCPport, String pubKey, String username, byte[] guid)
        {
            this.mIP = ip;
            this.mUDPport = UDPport;
            this.mTCPport = TCPport;
            this.mPublickey = pubKey;
            this.mUsername = username;
            this.mGUID = guid;
        }

        /// <summary>
        /// GETTERS & SETTERS
        /// </summary>

        public string IP
        {
            get { return this.mIP; }
            set { this.mIP = value; }
        }

        public int UDPport
        {
            get { return this.mUDPport; }
            set { this.mUDPport = value; }
        }

        public int TCPport
        {
            get { return mTCPport; }
            set { mTCPport = value; }
        }

        public String PublicKey
        {
            get { return this.mPublickey; }
            set { this.mPublickey = value; }
        }
        public String Username
        {
            get { return this.mUsername; }
            set { this.mUsername = value; }
        }

        public byte[] GUID
        {
            get { return this.mGUID; }
            set { this.mGUID = value; }
        }

        public string toString
        {
            get { return mUsername + " : " + mGUID + " - " + mIP + " : " + mUDPport; }
        }
    }
}
