﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using SharedDesk.Common;

namespace SharedDesk
{
    public class RoutingTable
    {
        /// <summary>
        /// Contains neighbour peers or closest found to neighbours
        /// </summary>
        private Dictionary<byte[], PeerInfo> mTable;
        
        /// <summary>
        /// This peers info.
        /// </summary>
        private PeerInfo mMyInfo;

        /// <summary>
        /// Instance of RoutingTable class.
        /// </summary>
        private static RoutingTable mInstance;

        private RoutingTable()
        {
            mTable = new Dictionary<byte[], PeerInfo>(new ByteArrayComparer());
        }

        /// <summary>
        /// Gets instance of the RoutingTable class.
        /// </summary>
        /// <returns>Instance of the RoutingTable class</returns>
        public static RoutingTable getInstance(){
            if(mInstance==null){
                mInstance = new RoutingTable();
            }
            return mInstance;
        }

        public void setMyInfo(PeerInfo myInfo){
            this.mMyInfo = myInfo;
        }

        /// <summary>
        /// Finds closest peer to target GUID in the routing table.
        /// </summary>
        /// <param name="targetGUID">GUID to find closest to.</param>
        /// <returns>PeerInfo of the closest peer found to the GUID.</returns>
        public PeerInfo findClosest(byte[] targetGUID)
        {
            PeerInfo closest = null;
            byte[] target = targetGUID;
            foreach (KeyValuePair<byte[], PeerInfo> entry in mTable)
            {
                PeerInfo p = entry.Value;
                if (closest == null)
                    closest = p;
                else if (firstGreaterThanSecond(calculateXOR(closest.GUID, target), calculateXOR(p.GUID, target)))
                   closest = p;
            }
            return closest;
        }

        /// <summary>
        /// Finds closest peer for sender GUID to target GUID.
        /// </summary>
        /// <param name="senderGUID">The GUID of the peer requesting the closest search.</param>
        /// <param name="Target">The target the sender want to find the closest to.</param>
        /// <returns>PeerInfo of the closest peer found to the GUID.</returns>
        public PeerInfo findClosestFor(byte[] senderGUID, byte[] Target)
        {
            PeerInfo closest = mMyInfo;
            byte[] target = Target;

            foreach (KeyValuePair<byte[], PeerInfo> entry in mTable)
            {
                PeerInfo p = entry.Value;
                if (firstGreaterThanSecond(calculateXOR(closest.GUID, target), calculateXOR(p.GUID, target)) && !senderGUID.SequenceEqual(p.GUID))
                    closest = p;
            }
            return closest;
        }

        /// <summary>
        /// Returns a list of the neighbours of the passed GUID.
        /// </summary>
        /// <param name="guid">GUID to find neighbours for.</param>
        /// <returns>List of neighbours</returns>
        public static List<byte[]> getTargetGUIDs(byte[] guid)
        {
            List<byte[]> targetGUIDs = new List<byte[]>();
            int exponentCapacity = (8 * Constants.GUID_SIZE) - 1;
            
            for (double i = 0; i < exponentCapacity; i++)
            {
                byte[] exponent = new byte[Constants.GUID_SIZE];
                int byteIndex = (int)i / 8;
                int bitInByteIndex = (int)i % 8;
                byte mask = (byte)(1 << bitInByteIndex);
                bool isSet = (exponent[byteIndex] & mask) != 0;
                exponent[byteIndex] |= mask;
                byte[] result = calculateXOR(guid, exponent);
                targetGUIDs.Add(result);
            }
            return targetGUIDs;
        }

        

        /// <summary>
        /// Return the xor result of two byte arrays.
        /// </summary>
        /// <param name="a">Byte array to be xored with b.</param>
        /// <param name="b">Byte array to be xored with a.</param>
        /// <returns>The xor result of the two byte arrays.</returns>
        public static byte[] calculateXOR(byte[] a, byte[] b)
        {
            // make byte[] to bit[]
            BitArray a_bits = new BitArray(a);
            BitArray b_bits = new BitArray(b);
            BitArray result = new BitArray(Constants.GUID_SIZE*8);
            int len = 0;
            // set length to be the length of the shorter byte[]
            if (a_bits.Length > b_bits.Length)
                len = b_bits.Length;
            else
                len = a_bits.Length;
            // for len
            for (int i = 0; i < len; i++)
            {
                // xor bits
                result[i] = a_bits[i] ^ b_bits[i];
            }
            // result byte[] to return
            byte[] byte_result = new byte[Constants.GUID_SIZE];
            // bit[] to byte []
            result.CopyTo(byte_result, 0);
            return byte_result;
        }

        byte[] compareGUIDs(byte[] guid1, byte[] guid2)
        {
            BitArray a_bits = new BitArray(guid1);
            BitArray b_bits = new BitArray(guid2);

            for (int i = a_bits.Length - 1; i >= 0; i--)
            {
                if (!(a_bits[i] == b_bits[i]))
                {
                    if (a_bits[i])
                    {
                        Console.WriteLine(BitConverter.ToInt16(guid1, 0) + " > " + BitConverter.ToInt16(guid2, 0));
                        return guid1;
                    }
                    Console.WriteLine(BitConverter.ToInt16(guid2, 0) + " > " + BitConverter.ToInt16(guid1, 0));
                    return guid2;
                }
            }
            Console.WriteLine("Returning null.");
            return null;
        }

        /// <summary>
        /// Pass 2 byte[] and if the first represents an int bigger than
        /// the second it returns true
        /// </summary>
        /// <param name="first">byte[] first</param>
        /// <param name="second">byte[] second</param>
        /// <returns>True if first > second</returns>
        bool firstGreaterThanSecond(byte[] first, byte[] second)
        {
            // byte arrays to bit arrays
            BitArray first_bits = new BitArray(first);
            BitArray second_bits = new BitArray(second);
            // for all the bits
            for (int i = first_bits.Length - 1; i >= 0; i--)
            {
                // if bit is not the same
                if (!(first_bits[i] == second_bits[i]))
                {
                    // if bit of a is 1 (true)
                    if (first_bits[i])
                    {
                        // a is bigger so return true
                        return true;
                    }
                    // any other case return false
                    return false;
                }
            }
            return false;
        }

        // Returns true if table contains passed PeerInfo object
        // TODO: NEEDS LOCK OBJECT
        public bool containsValue(PeerInfo peer)
        {
            List<PeerInfo> list = new List<PeerInfo>(mTable.Values);
            // Loop through list
            foreach (PeerInfo p in list)
            {
                if (p.GUID.SequenceEqual(peer.GUID))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Adds PeerInfo to the routing table.
        /// </summary>
        /// <param name="p">PeerInfo object to be added.</param>
        public void add(PeerInfo p)
        {
            // if table contains PeerInfo object
            if (containsValue(p))
            {
                // remove it
                remove(p.GUID);
            }
            // if table has other PeerInfo object at that GUID
            if (mTable.ContainsKey(p.GUID))
            {
                // replace it
                replace(p.GUID, p);
            }
            // else simply add it
            else
                mTable.Add(p.GUID, p);
        }

        // Adds peer if its the closest known to neighbour peer
        public bool addIfCloser(PeerInfo p)
        {
            List<byte[]> targets = getTargetGUIDs(mMyInfo.GUID);
            List<byte[]> closerToIndex = new List<byte[]>();
            bool added = false;
            if (containsValue(p))
                return false;
            // Finds Possible target GUIDs that p can be placed
            foreach (byte[] tar in targets)
            {
                PeerInfo closest = findClosest(tar);

                if (mTable.ContainsKey(tar))
                {
                    PeerInfo current = mTable[tar];
                    if (compareGUIDs(calculateXOR(p.GUID, tar), calculateXOR(current.GUID, tar)) != null && calculateXOR(current.GUID, tar).SequenceEqual(compareGUIDs(calculateXOR(p.GUID, tar), calculateXOR(current.GUID, tar))))
                    // calculateXOR(p.StringGUID, tar) < calculateXOR(current.StringGUID, tar)
                    {
                        closerToIndex.Add(tar);
                    }
                }
                else// if (closest == null || (compareGUIDs(calculateXOR(p.ByteGUID, tar), calculateXOR(findClosest(tar).ByteGUID, tar)) != null && calculateXOR(findClosest(tar).ByteGUID, tar).SequenceEqual(compareGUIDs(calculateXOR(p.ByteGUID, tar), calculateXOR(findClosest(tar).ByteGUID, tar)))))
                {
                //  calculateXOR(p.getGUID, tar) < calculateXOR(findClosest(tar).getGUID, tar)
                    closerToIndex.Add(tar);
                }
            }


            byte[] index = null;
            byte[] dist = null;
            foreach (byte[] closer in closerToIndex)
            {
                // calculateXOR(p.getGUID, closer) < dist
                if (dist == null || dist.SequenceEqual(compareGUIDs(calculateXOR(p.GUID, closer), dist)))
                {
                    index = closer;
                    dist = calculateXOR(p.GUID, closer);
                }
            }

            if (index != null)
            {
                added = true;
                if (mTable.ContainsKey(index))
                {
                    replace(index, p);
                }
                else
                {
                    mTable.Add(index, p);
                }
            }
            return added;
        }

        // Replaces peer at target guid with the peerinfo
        public void replace(byte[] targetGuid, PeerInfo p)
        {
            if (!containsValue(p))
            {
                remove(p.GUID);
            }

            PeerInfo temp = mTable[targetGuid];
            mTable.Remove(targetGuid);
            mTable.Add(targetGuid, p);
            addIfCloser(temp);
        }

        // Removes peer with target guid from the table
        public void remove(byte[] guid)
        {
            foreach (KeyValuePair<byte[], PeerInfo> entry in mTable)
            {
                PeerInfo p = entry.Value;
                if (p.GUID.SequenceEqual(guid))
                {
                    byte[] tempGuid = entry.Key;
                    mTable.Remove(entry.Key);
                    Peer.getInstance().refreshRemovedPeer(tempGuid);
                    break;
                }
            }
        }

     

        // Returns and sets the owner PeerInfo
        public PeerInfo MyInfo
        {
            get { return mMyInfo; }
            set { mMyInfo = value; }
        }

        // Returns table peer info in string format
        public string toString()
        {
            string result = "";
            int count = 0;
            foreach (KeyValuePair<byte[], PeerInfo> entry in mTable)
            {
                PeerInfo p = entry.Value;
                result += count + ": " + p.toString + "\n";
                count++;
            }
            return result;
        }

        public Dictionary<byte[], PeerInfo> Table
        {
            get { return this.mTable; }
            set { this.mTable = value; }
        }

        // DEPRECATED
        
        // Remove unwanted peers - Requires owner peer GUID
        public void cleanTable(byte[] guid)
        {
            List<byte[]> targets = getTargetGUIDs(guid);
            List<byte[]> list = new List<byte[]>(mTable.Keys);
            // Loop through list
            foreach (byte[] p in list)
            {
                if (!targets.Contains(p))
                {
                    mTable.Remove(p);
                }
            }
        }


        // Retruns Peerinfo at target guid
        public PeerInfo get(byte[] targetGUID)
        {
            return mTable[targetGUID];
        }
    }
}
