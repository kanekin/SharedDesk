﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedDesk.UDP_protocol;
using SharedDesk.Kadelima;
using System.Net;
using System.Timers;
using System.Threading;
using System.IO;
using System.Net.Sockets;
using SharedDesk.FileWatcher;
using SharedDesk.TCP_file_transfer;
using SharedDesk.interfaces;
using System.Collections;
using SharedDesk.Common;
using SharedDesk.Data_storage;

namespace SharedDesk
{
    public class Peer
    {
        // peers tcp port
        private int mTcpPort;
        
        // Wait time for refreshing the table in milisec
        private static int REFRESH_TIME = 30000;
        // Wait time for pinging closests peers in milisec
        private static int PING_INTERVALS = 15000;
        // Wait time for response for a ping request in milisec
        private static int WAIT_TIME_FOR_PING_RESPONSE = 7000;


        // Authroization objects
        private AuthenticationUtils mAuthSender;
        private UserUtils mUserSender;
        private Authentication.UserMgmt.UserHelper mUserHelper;
        private Authentication.Authority.KeyAuthority mKeyAuthority;
        private Authentication.RSA.RSAHelper mRSAHelperBootPeer;
        private Authentication.RSA.RSAHelper mRSAHelper;

        private static Peer mInstance;
        // My PeerInfo
        private PeerInfo myInfo;
        // Boot peer list
        private List<PeerInfo> bootList;
        // Boot PeerInfo
        private PeerInfo bootPeer;
        // My RoutingTable
        private RoutingTable routingTable;


        /// <summary>
        /// boolean for making sure that initialization of the peer happens only once
        /// initialization of the peer should occur only once.
        /// </summary>
        public static bool isInitialized = false;

        /// <summary>
        /// The socket which is used for all the udp communication
        /// </summary>
        private Socket mSocket = null;

        // The UDP Manager
        //private int mUdpPort;

        // The UDP Listener
        //private UDPListener listener;

        private KademliaSenderUtils mKademliaSender;
        private STUNSenderUtils mSTUNSender;
        private FileSenderUtils mFileSender;

        // The SearchChannel list
        private Dictionary<byte[], SearchChannel> channels;

        // Lock Object for the SearchChannel list
        private static Object channelsLock = new Object();

        // Timer that pings all the peers in the routing table
        System.Timers.Timer pingTable;

        // Timer that refreshes the table over specified interval
        System.Timers.Timer refreshTableTimer;

        // List to hold timers of pings
        Dictionary<System.Timers.Timer, byte[]> pingTimers;
        private Object timersLock = new Object();

        // Target peer - Holds found peer after search if found
        PeerInfo targetPeer;

        // interface for storing information
        public IStoreData fileHelper;

        // interface to transfer files with TCP
        TransferFile tf = null;
        FileDownloadManager fd;

        // Event to update Form
        public event handlerUpdatedTable updateTable;
        public delegate void handlerUpdatedTable();

        // Event to update label for incoming invitations
        public event handlerUpdatedLabelIncomingInvitations updateLabelIncomingInvitations;
        public delegate void handlerUpdatedLabelIncomingInvitations(int number);

        public event handlerUpdatedLabelOutgoingInvitations updateLabelOutgoingInvitations;
        public delegate void handlerUpdatedLabelOutgoingInvitations(int number);

        //event to update form myInfo
        public event handlerUpdatedMyInfo updateMyInfoTB;
        public delegate void handlerUpdatedMyInfo();

        //
        public event handlerUpdateMessages updateMessages;
        public delegate void handlerUpdateMessages(string msg);

        // lock object
        readonly object _locker = new object();

        SearchPublic searchPublic;

        private Peer() { }

        public static Peer getInstance() 
        {
            if (mInstance == null) {
                mInstance = new Peer();
            }
            return mInstance;
        }

        // TODO: pass boot peer after retrieval of peers from servers
        public void init(List<PeerInfo> list) {
            // initiating random tcp port
            Random rand = new Random();
            mTcpPort = rand.Next(20000) + 40000;
            // generate and load keys
            mRSAHelperBootPeer = new Authentication.RSA.RSAHelper();
            
            // initializing the bootList with boot peers given from the server.
            bootList = list;
            // boot peer info
            //bootPeer = new PeerInfo("127.0.0.1", 8080, mTcpPort, mRSAHelperBootPeer.publicKey, "UsernameBootPeer", ByteUtils.convertStringToBytes(""));

            // initiating the timers
            refreshTableTimer = new System.Timers.Timer();
            refreshTableTimer.Elapsed += refreshTable;
            // initializing search channel list
            channels = new Dictionary<byte[], SearchChannel>(new ByteArrayComparer());
            // List that holds the timers that waiting for ping response
            pingTimers = new Dictionary<System.Timers.Timer, byte[]>();
            
            // Set TCP file transfer protocol
            tf = new TransferFile(new TransferTCPv5());

            // file helper for storing information in xml files
            fileHelper = new SqliteStorage();

            // start threads to monitor folders in the background
            // and create Public and Downloads folders if they don't exist
            setBackgroundMonitoringOfFolders();
            // initiating ServerManager
            TCPServerManager mServerManager = new TCPServerManager();
            mServerManager.init();
            // initiating FileDownloadManager
            fd = new FileDownloadManager(myInfo, mFileSender);
            FileTransferEvents.downloadComplete += FileTransferEvents_downloadComplete;
        }

        // Used for initializing peer features requires guid, ip and port of the peer
        public void init(string guid, string ip, int port)
        {
            if (!isInitialized)
            {
                // RSAHelper - generate (public/private) and get public key to assign in PeerInfo object
                mRSAHelper = new Authentication.RSA.RSAHelper();
                String tempKey = mRSAHelper.publicKey;

                // Initializing myInfo
                string string_guid = Path.GetRandomFileName();
                string_guid = string_guid.Replace(".", "");
                myInfo = new PeerInfo(ip, port, mTcpPort, tempKey, "UsernameMyInfo", ByteUtils.convertStringToBytes(guid));

                // set PeerInfo object in KeyAuthority (add myself with public key)
                mKeyAuthority = new Authentication.Authority.KeyAuthority(myInfo.GUID, myInfo.PublicKey);

                // init socket
                mSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                mSocket.ReceiveBufferSize = 8192;
                mSocket.SendBufferSize = 8192;
                //Specify the port, so that STUN server can reply
                mSocket.Bind(new IPEndPoint(IPAddress.Parse(ip), port));

                //Initialize the kademliaSenderUtil
                //mKademliaSender = new KademliaSenderUtils(mSocket);
                mKademliaSender = KademliaSenderUtils.getInstance();
                mKademliaSender.setSocket(mSocket);

                //Initialize the STUNSenderUtils 
                mSTUNSender = new STUNSenderUtils(mSocket);
                //Initialize the FileSenderUtils
                mFileSender = FileSenderUtils.getInstance();
                mFileSender.setSocket(mSocket);

                //Initialize the authenticationUtils
                mAuthSender = new AuthenticationUtils(mSocket);
                mUserHelper = new Authentication.UserMgmt.UserHelper(404, myInfo.Username);
                mUserSender = new UserUtils(mSocket);

                // file helper for storing information in xml files
                fileHelper = new SqliteStorage();

                routingTable = RoutingTable.getInstance();
                routingTable.setMyInfo(myInfo);

                UDPListener listener = new UDPListener(mSocket, myInfo);
                listener.start();
                subscribeToListener(listener);

                // Create Routing Table and adding boot peer
                //routingTable = new RoutingTable(myInfo);
                //routingTable.add(bootPeer);

                FileDownloadManager fd = new FileDownloadManager(myInfo, mFileSender);

                byte[] guidByte = ByteUtils.convertStringToBytes(guid);
                searchPublic = new SearchPublic(guidByte, mFileSender);
                isInitialized = true;
            }
        }
        
        void FileTransferEvents_downloadComplete(ChangeInFileDownload e)
        {
            fileHelper.setStatusOfFile(e.md5, FileInfoP2P.Status.complete);
        }

        private void setBackgroundMonitoringOfFolders()
        {
            // monitor public folder
            string pathPublicFolder = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            pathPublicFolder += "\\Public\\";

            UploadFileInfoQueue uploadManagerPublicFolder = new UploadFileInfoQueue(pathPublicFolder, 1);
            uploadManagerPublicFolder.newFileAdded += uploadManager_newFileAdded;

            // monitor downloads folder
            string pathDownloadsFolder = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            pathDownloadsFolder += "\\Downloads\\";

            UploadFileInfoQueue uploadManagerDownloadsFolder = new UploadFileInfoQueue(pathDownloadsFolder, 1);
            uploadManagerDownloadsFolder.newFileAdded += uploadManager_newFileAdded;
        }
        

        // Timed event refreshinf the table
        void refreshTable(object sender, ElapsedEventArgs e)
        {
            refreshTableTimer.Stop();
            refreshTableTimer.Interval = REFRESH_TIME;
            findNeighbours();
            refreshTableTimer.Start();
        }

        public void sendRequestRoutingTable()
        {
            if (!bootPeer.GUID.SequenceEqual(GUID))
            {
                IPEndPoint remotePoint = new IPEndPoint(IPAddress.Parse(bootPeer.IP), bootPeer.UDPport);
                mKademliaSender.sendRequestRoutingTable(mTcpPort, remotePoint);
            }
        }

        public void startPingTimer()
        {
            pingTable = new System.Timers.Timer(PING_INTERVALS);
            pingTable.Elapsed += sendPingToTable;
            //TODO: make separated sensor
            //pingTable.Elapsed += sendKeepAliveToServer;
            pingTable.Start();
        }

        //send keep alive message to STUN server
        public void sendKeepAliveToServer(object sender, EventArgs e)
        {
            mSTUNSender.sendSTUN_KeepAlive(UDPConstants.STUN_SERVER_IP, UDPConstants.STUN_SERVER_PORT);
        }

        /// <summary>
        /// SEND REQUESTS
        /// </summary>

        // Send ping to all the peers in the table
        public void sendPingToTable(object sender, EventArgs e)
        {
            List<PeerInfo> list = new List<PeerInfo>(routingTable.Table.Values);
            foreach (PeerInfo p in list)
            {
                sendPing(p);
            }
        }

        // TODO : REDO PING
        // Send ping to see if alive
        private void sendPing(PeerInfo pInfo)
        {
            //if (pInfo.getGUID != myInfo.getGUID)
            //{
                //startTimeoutTimer(pInfo.ByteGUID);

                //IPEndPoint remotePoint = new IPEndPoint(IPAddress.Parse(pInfo.getIP()), pInfo.getUDPport());
                //mKademliaSender.sendPing(remotePoint);
                //UDPResponder responder = new UDPResponder(remotePoint, myInfo.getPORT());
                //responder.sendPing();
            //}
        }

        private void sendFileInfoThread(string fileName, string fileFullPath)
        {
            string md5 = "";

            // store in xml that the peer selected is now authorized to download this file
            lock (_locker)
            {

                // check that file exist
                if (File.Exists(fileFullPath) == false)
                {
                    Console.WriteLine("Could not find file spesified");
                    return;
                }

                FileInfo file = new FileInfo(fileFullPath);

                byte[] md5Byte = ChecksumCalc.GetMD5Checksum(fileFullPath);
                md5 = ChecksumCalc.getMD5String(md5Byte);

                fileHelper.authorizeFileDownloadWithPeer(file, md5, targetPeer);
            }

            updateMessages(String.Format("Sending file INFO object \"{0}\" to peer guid {1}, ip {2}:{3}", fileName, targetPeer.GUID, targetPeer.IP, targetPeer.UDPport));
            IPAddress RecevingIp = IPAddress.Parse(targetPeer.IP);


            //// Send file info 
            //// create end point
            IPEndPoint remotePoint = new IPEndPoint(RecevingIp, targetPeer.UDPport);
            mFileSender.sendFileInfo(fileFullPath, this.GUID, remotePoint, md5);
        }

        // Send ping to see if alive
        public void sendFileInfo(string fileName, string fileFullPath)
        {
            // find the guid of the peer selected
            //string selectedPeer = listRoutingTable.Items[index].ToString();

            string md5 = "";

            if (targetPeer == null)
            {
                updateMessages("No peer selected!");
                return;
            }

            lock (_locker)
            {

                // check that file exist
                if (File.Exists(fileFullPath) == false)
                {
                    Console.WriteLine("Could not find file spesified");
                    return;
                }

                FileInfo file = new FileInfo(fileFullPath);

                int maxFileSizeInBytes = 100000000;
                if (file.Length > maxFileSizeInBytes)
                {
                    Console.WriteLine("ERROR:");
                    Console.WriteLine("FILE TO BIG, not supportet yet!");
                    return;
                }

                byte[] md5Byte = ChecksumCalc.GetMD5Checksum(fileFullPath);
                md5 = ChecksumCalc.getMD5String(md5Byte);
                fileHelper.authorizeFileDownloadWithPeer(file, md5, targetPeer);

            }

            string targetGUID = ByteUtils.convertByteToString(targetPeer.GUID);

            updateMessages(String.Format("Sending file INFO object \"{0}\" to peer guid {1}, ip {2}:{3}", fileName, targetGUID, targetPeer.IP, targetPeer.UDPport));
            IPAddress RecevingIp = IPAddress.Parse(targetPeer.IP);

            //// Send file info 
            //// create end point
            IPEndPoint remotePoint = new IPEndPoint(RecevingIp, targetPeer.UDPport);
            mFileSender.sendFileInfo(fileFullPath, this.GUID, remotePoint, "");
        }

        private void startTimeoutTimer(byte[] guid)
        {
            System.Timers.Timer tempTimer = new System.Timers.Timer(WAIT_TIME_FOR_PING_RESPONSE);
            tempTimer.Elapsed += removeOffline;
            pingTimers.Add(tempTimer, guid);
            tempTimer.Start();
        }

        // Starting refresh timer with specified interval. Used for forced refresh.
        public void startRefreshTableTimer(int interval)
        {
            if (refreshTableTimer.Enabled)
            {
                refreshTableTimer.Stop();
            }
            refreshTableTimer.Interval = interval;
            refreshTableTimer.Start();
        }

        /// <summary>
        /// Finds closest to target peer for another peer
        /// </summary>
        /// <param name="senderGUID">The peer that requested the search</param>
        /// <param name="target">The peer we are serching for</param>
        /// <returns></returns>
        public PeerInfo askForClosestPeer(byte[] senderGUID, byte[] target)
        {
            return routingTable.findClosestFor(senderGUID, target);
        }
        
        //////
        // SEARCH FUNCTIONS
        //////

        /// <summary>
        /// Searches if removed peer can be replaced
        /// </summary>
        /// <param name="guid">GUID of the position of the removed peer</param>
        public void refreshRemovedPeer(byte[] guid)
        {
            searchPeer(guid, addPeerInfo);
        }

        /// <summary>
        /// Finds closest GUIDs and searches for closest peers to them in the network
        /// </summary>
        private void findNeighbours()
        {
            List<byte[]> targetGUIDs = RoutingTable.getTargetGUIDs(myInfo.GUID);
            foreach (byte[] guid in targetGUIDs)
            {
                searchPeer(guid, addPeerInfo);
            }
        }

        void uploadManager_newFileAdded(string fileFullPath)
        {
            if (routingTable == null)
            {
                // ERROR
                Console.WriteLine("ERROR: No routing table");
                return;
            }

            Dictionary<byte[], PeerInfo> peers = routingTable.Table;
            List<byte[]> guids = new List<byte[]>();
            foreach (KeyValuePair<byte[], PeerInfo> p in peers)
            {
                guids.Add(p.Value.GUID);
            }

            FileNotifier n = new FileNotifier(guids.Count, fileFullPath, mFileSender, myInfo);
            //downloads.Add(md5, fd);
            foreach (byte[] guid in guids)
            {
                searchPeer(guid, n.onFound);
            }
        }

        
        /// <summary>
        /// Finds target with GUID
        /// </summary>
        /// <param name="guid">Target GUID</param>
        public void findTarget(byte[] guid)
        {
            searchPeer(guid, handleTarget);
        }

        /// <summary>
        /// Finds the online peers that share the file with the md5.
        /// </summary>
        /// <param name="md5">Hash of the file</param>
        /// <param name="listGuid">List of known peers that have the file</param>
        public void findFilePeers(string md5, List<byte[]> listGuid)
        {
            fd.addFileForDownLoad(md5, listGuid.Count);
            foreach (byte[] guid in listGuid)
            {
                searchPeer(guid, fd.getFilehandler(md5).onFound);
            }
        }

        /// <summary>
        /// Checks if we are already searching for the target peer.
        /// </summary>
        /// <param name="guid">Target GUID</param>
        /// <returns>True if already searching.</returns>
        public bool isSearching(byte[] guid)
        {
            return channels.ContainsKey(guid);
        }

        /// <summary>
        /// Inits search if we are not searching. If we are it adds the callback function to the search.
        /// </summary>
        /// <param name="guid">Target GUID</param>
        /// <param name="f">Callback function</param>
        public void searchPeer(byte[] guid, SearchChannel.onFound f)
        {

            lock (channelsLock)
            {
                if (!isSearching(guid))
                {
                    updateMessages("Searching for target " + ByteUtils.convertByteToString(guid) + "...");
                    PeerInfo closest = routingTable.findClosest(guid);
                    if (closest != null && !(myInfo.GUID.SequenceEqual(closest.GUID)))
                    {
                        //routingTable.remove(closest.getGUID);
                        SearchChannel channel = new SearchChannel(guid, mKademliaSender);
                        channel.onPeerFound += f;
                        channel.onPeerFound += removeFromList;
                        channels.Add(guid, channel);
                        channel.onReceiveClosest(closest);
                    }
                }
                else
                {
                    channels[guid].onPeerFound += f;
                }
            }
        }

        public void sendSearchRequest()
        {
            searchPublic.searchTable(routingTable);
        }

        /// <summary>
        /// Sends a leave request to all peers in the routing table.
        /// </summary>
        public void sendLeaveRequests()
        {
            if (routingTable != null)
            {
                List<PeerInfo> list = new List<PeerInfo>(routingTable.Table.Values);
                foreach (PeerInfo p in list)
                {
                    sendLeaveRequest(p);
                }
                updateMessages("Left the network!");
            }
            else
            {
                updateMessages("You have not joined the network yet.");
            }
        }

        //TODO - to byte guid
        // Sends leave request to the pInfo peer
        private void sendLeaveRequest(PeerInfo pInfo)
        {
            if (pingTable != null)
            {
                pingTable.Stop();
            }
            refreshTableTimer.Stop();
            IPEndPoint remotePoint = new IPEndPoint(IPAddress.Parse(pInfo.IP), pInfo.UDPport);
            //UDPResponder responder = new UDPResponder(remotePoint, myInfo.getPORT());
            mKademliaSender.sendRequestLeave(myInfo.GUID, remotePoint);
        }

        //////
        // EVENT HANDLERS
        //////

        // If peer did not respond to ping this function will be called by the timer.
        void removeOffline(object sender, ElapsedEventArgs e)
        {
            lock (timersLock)
            {
                System.Timers.Timer temp = (System.Timers.Timer)sender;
                temp.Stop();
                byte[] guid = pingTimers[temp];
                pingTimers.Remove(temp);
                temp.Dispose();

                routingTable.remove(guid);
                // remove public key
                mKeyAuthority.removePubKey(guid);
                startRefreshTableTimer(5000);
                updateTable();
            }
        }

        // Subscribe to UDP listener events
        public void subscribeToListener(UDPListener l)
        {
            KademliaHandler kHandler = l.getKademliaHandler();
            
            kHandler.receiveRequestTable += new KademliaHandler.handlerRequestTable(handleRequestTable);
            kHandler.receiveClosest += new KademliaHandler.handlerFindChannel(handleReceiveClosest);
            kHandler.receiveRequestClosest += new KademliaHandler.handlerRequestClosest(handleRequestClosest);
            kHandler.receiveRequestJoin += new KademliaHandler.handlerRequestJoin(handleRequestJoin);
            kHandler.receiveRequestLeave += new KademliaHandler.handlerRequestLeave(handleRequestLeave);
            kHandler.receiveRequestPing += new KademliaHandler.handlerRequestPing(handleRequestPing);
            kHandler.receiveGUID += new KademliaHandler.handlerGUID(handleGUID);
        }


        public void handleSearchTable(RoutingTable table)
        {
            searchPublic.searchTable(table);
        }

        public void handlePublicFileSearch(EndPoint remotePoint)
        {
            List<FileInfoP2P> publicFiles = fileHelper.getYourSharedFiles();

            if (publicFiles == null)
            {
                // found no public files
                return;
            }

            if (publicFiles.Count != 0)
            {
                foreach (FileInfoP2P fileInfo in publicFiles)
                {
                    mFileSender.sendFileInfo(fileInfo.Path, this.GUID, remotePoint, fileInfo.getMd5AsString());
                }
            }
        }

        public void handleRequestFilePart(string md5, int[] parts, IPEndPoint remotePoint)
        {

            double fileSize = fileHelper.getSize(md5);
            string fullPath = fileHelper.getFullFilePathOfSharedFile(md5);

            if (fullPath == "")
            {
                // could not find the requested file
                Console.WriteLine("ERROR");
                Console.WriteLine(String.Format("Could not find md5 of file at location {0}", fullPath));
                return;
            }

            FileUpload fd = new FileUpload(md5, fullPath, fileSize, parts, remotePoint);
        }

        public void handleRequestFileAndroid(IPEndPoint remotePoint, String md5)
        {
            string filePath = fileHelper.getFullFilePathOfSharedFile(md5);
            FileUploadAndroid fd = new FileUploadAndroid(md5, filePath, remotePoint);
        }

        public void updateUsername(String name)
        {
            myInfo.Username = name;
        }

        /// <summary>
        /// received the remote ip and port of this peer. update myInfo!
        /// </summary>
        public void updateMyInfo(string ipAddress, int port)
        {
            myInfo.IP = ipAddress;
            myInfo.UDPport = port;

            if (myInfo.Username == String.Empty)
            {
                myInfo.Username = "UsernameNotSet";
            }

            if (routingTable.Table.ContainsKey(myInfo.GUID))
            {
                PeerInfo p = routingTable.get(myInfo.GUID);
                p.IP = ipAddress;
                p.UDPport = port;
            }
            else
            {
                //PeerInfo p = new PeerInfo(myInfo.getGUID, ipAddress, port, mRSAHelper.publicKey, myInfo.Username);
                //routingTable.add(p);
            }

            updateMyInfoTB();
            updateTable();
        }

        // Handling ping request
        public void handleRequestPing(IPEndPoint remotePoint)
        {
            mKademliaSender.sendGUID(404, remotePoint);
            //TODO: remove this
            //UDPResponder responder = new UDPResponder(remotePoint, remotePoint.Port);
            //responder.sendGUID(myInfo.getGUID);
        }

        // Handling GUID
        public void handleGUID(byte[] guid)
        {
            lock (timersLock)
            {
                System.Timers.Timer tempTimer = pingTimers.FirstOrDefault(x => x.Value == guid).Key;
                if (tempTimer != null)
                {
                    tempTimer.Stop();
                    pingTimers.Remove(tempTimer);
                    tempTimer.Dispose();
                }
            }
        }

        // Handling the routing table request
        public void handleRequestClosest(IPEndPoint remotePoint, byte[] sender, byte[] target)
        {
            PeerInfo targetInfo = askForClosestPeer(sender, target);
            TCPUploader.peerUpload(target, targetInfo, remotePoint);
            //mKademliaSender.sendClosest(target, targetInfo, remotePoint);

            //UDPResponder responder = new UDPResponder(remotePoint, remotePoint.Port);
            //responder.sendClosest(target, targetInfo);
        }

        // Handling the received routing table
        public void handleTable(Dictionary<byte[], PeerInfo> peers)
        {
            this.routingTable.MyInfo = myInfo;
            //this.routingTable.setPeerDictionary(peers);
            foreach (KeyValuePair<byte[], PeerInfo> entry in peers)
            {
                if (!(entry.Value.GUID.SequenceEqual(GUID)))
                {
                    if (routingTable.addIfCloser(entry.Value))
                    {
                        IPEndPoint remotePoint = new IPEndPoint(IPAddress.Parse(entry.Value.IP), entry.Value.UDPport);
                        mKademliaSender.sendRequestJoin(myInfo, remotePoint);
                    }
                }   
            }
            Console.Write(routingTable.toString());
            findNeighbours();
            startPingTimer();
            startRefreshTableTimer(30000);
            updateTable();
        }

        // Handling the routing table request
        public void handleRequestTable(IPEndPoint remotePoint)
        {
            if (routingTable.Table.Count > 0)
            {
                TCPUploader.tableUpload(routingTable.Table, remotePoint);
            }
            else
            {
                Dictionary<byte[], PeerInfo> tempTable = new Dictionary<byte[], PeerInfo>(new ByteArrayComparer());
                tempTable.Add(myInfo.GUID, myInfo);
                TCPUploader.tableUpload(tempTable, remotePoint);
            }
                //mKademliaSender.sendRoutingTable(routingTable, remotePoint);
            //TODO: remove this
            //UDPResponder responder = new UDPResponder(remotePoint, remotePoint.Port);
            //responder.sendRoutingTable(routingTable);
        }

        // Handling the routing table request
        public void handleRequestJoin(PeerInfo newPeer)
        {
            //routingTable.cleanTable(myInfo.getGUID);
            List<byte[]> targets = RoutingTable.getTargetGUIDs(myInfo.GUID);
            if (targets.Contains(newPeer.GUID))
            {
                routingTable.add(newPeer);
                mKeyAuthority.addPubKey(newPeer.GUID, newPeer.PublicKey);
            }
            else if (!routingTable.containsValue(newPeer))
            {
                routingTable.addIfCloser(newPeer);
                mKeyAuthority.addPubKey(newPeer.GUID, newPeer.PublicKey);
            }
            updateTable();
        }

        // Handling the routing table request
        public void handleRequestLeave(byte[] guid)
        {
            routingTable.remove(guid);
            // remove public key from list
            mKeyAuthority.removePubKey(guid);
            startRefreshTableTimer(1000);
            updateTable();
        }

        // Handling receive routing table request
        public void handleReceiveClosest(byte[] guid, PeerInfo currentClosest)
        {
            //routingTable.cleanTable(myInfo.getGUID);
            lock (channelsLock)
            {
                if (isSearching(guid))
                {
                    channels[guid].onReceiveClosest(currentClosest);
                }
                else 
                {
                    Console.WriteLine("Error: No search channel for: " + guid);
                }
            }
        }

        // Handles request to add peer info
        public void addPeerInfo(PeerInfo pInfo, byte[] targetGUID)
        {
            //routingTable.cleanTable(myInfo.getGUID);
            List<byte[]> targets = RoutingTable.getTargetGUIDs(myInfo.GUID);
            if (pInfo.GUID.SequenceEqual(targetGUID))
            {
                routingTable.add(pInfo);
                mKeyAuthority.addPubKey(pInfo.GUID, pInfo.PublicKey);

                IPEndPoint remotePoint = new IPEndPoint(IPAddress.Parse(pInfo.IP), pInfo.UDPport);

                mKademliaSender.sendRequestJoin(myInfo, remotePoint);

                //TODO: remove this
                //UDPResponder responder = new UDPResponder(remotePoint, myInfo.getPORT());
                //responder.sendRequestJoin(myInfo);

                updateTable();
            }
            else
            {
                bool added = routingTable.addIfCloser(pInfo);

                if (added)
                {
                    mKeyAuthority.addPubKey(pInfo.GUID, pInfo.PublicKey);

                    IPEndPoint remotePoint = new IPEndPoint(IPAddress.Parse(pInfo.IP), pInfo.UDPport);

                    mKademliaSender.sendRequestJoin(myInfo, remotePoint);
                    //UDPResponder responder = new UDPResponder(remotePoint, myInfo.getPORT());
                    //responder.sendRequestJoin(myInfo);
                    updateTable();
                }
            }
            //routingTable.cleanTable(myInfo.getGUID);
        }

        public void handleTarget(PeerInfo pInfo, byte[] targetGUID)
        {
            if (pInfo.GUID.SequenceEqual(targetGUID))
            {
                targetPeer = pInfo;
                updateMessages("Target " + ByteUtils.convertByteToString(pInfo.GUID) + " online!");
                //sendFileInfo(Path.GetFileName(file), file);
            }
            else
            {
                targetPeer = null;
                updateMessages("Target is offline!");
            }
        }

        public void removeFromList(PeerInfo pInfo, byte[] targetGUID)
        {
            if (isSearching(targetGUID))
            {
                channels.Remove(targetGUID);
            }
        }

        public void saveContacts()
        {
            if (this.mUserHelper != null)
            {
                this.mUserHelper.saveListsToXML();
            }
        }

        /// <summary>
        /// Attempt to send file request
        /// </summary>
        /// <param name="md5"></param>
        /// <param name="targetGuid"></param>
        /// <returns>return false if target guid is not found, true if request is sent</returns>
        public bool sendFileRequest(string fileName, string md5, byte[] targetGuid)
        {
            // not allowed to send request to yourself
            if (targetGuid.SequenceEqual(GUID))
            {
                return false;
            }

            findTarget(targetGuid);

            if (targetPeer == null)
            {
                return false;
            }

            if (targetPeer.GUID != targetGuid)
            {
                // could not find target and send request
                return false;
            }


            // TODO: 
            // find avaliable TCP port

            // generate random port between 40.000 - 60.000 
            Random rand = new Random();
            int tcpPort = rand.Next(20000) + 40000;

            // start listening on TCP port
            // create end point
            IPEndPoint listenFor = new IPEndPoint(IPAddress.Any, tcpPort);

            // define path where to store the file
            string saveFolder = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            saveFolder += "\\Downloads\\";

            // check if folder exist
            if (Directory.Exists(saveFolder) == false)
            {
                Directory.CreateDirectory(saveFolder);
            }

            // TODO: 
            // check if file exist

            // TODO: 
            // check that peer have the file and authorize the download
            // then open port and request transfer

            // start listening on tcp port 
            tf.listenForFile(saveFolder, listenFor);

            // send file request 
            IPEndPoint remotePoint = new IPEndPoint(IPAddress.Parse(targetPeer.IP), targetPeer.UDPport);
            mFileSender.sendFileRequest(md5, tcpPort, targetPeer.GUID, remotePoint);

            updateMessages(String.Format("Listening on port {0}, for TCP file transfer of file: {1}", tcpPort, fileName));
            updateMessages("Sending the file request to " + targetGuid.ToString());

            return true;
        }


        public void sendFile(string filePath, IPEndPoint endPoint)
        {
            tf.sendFile(filePath, endPoint);
        }

        public void sendSTUN_request()
        {
            mSTUNSender.sendSTUN_request();
        }

        //Maybe useless.
        public void sendSTUN_punch_request(string ipAddress, int port, int targetGuid)
        {
            mSTUNSender.sendSTUN_PunchRequest(ipAddress, port, targetGuid);
        }

        //Maybe useless
        public void sendSTUN_punch(string ipAddress, int port, int targetGuid)
        {
            mSTUNSender.sendSTUN_Punch(ipAddress, port, targetGuid);
        }

        //File
        public void sendFile(string filePath, EndPoint endPoint)
        {
            mFileSender.sendFileInfo(filePath, this.GUID, endPoint, "");
        }

        public void sendFileList(EndPoint remoteEnd)
        {
            List<String> filePaths = new List<String>();
            filePaths = fileHelper.getPublicFilePathsList();

            mFileSender.sendFileInfoList(filePaths, this.GUID, remoteEnd);
        }

        // TODO: Implement method for returning list of files in public folder (only file paths)
        public void getPublicFilePaths()
        {
            //return list;
        }

        public void reopenChannel(byte[] targetGuid)
        {
            SearchChannel channel = null;
            if (isSearching(targetGuid))
            {
                channel = channels[targetGuid];
            }
            channel.setHolePunched(true);
        }

        public void testReuqestPublicKey(EndPoint temp)
        {
            mAuthSender.requestPublicKey(404, myInfo.PublicKey, temp);
        }

        public void handleRequestPublicKey(byte[] senderGuid, String senderPubKey, IPEndPoint remotePoint)
        {
            // store in Key Authority
            if (mKeyAuthority.containsKey(senderGuid))
            {
                mKeyAuthority.replacePubKey(senderGuid, senderPubKey);
            }
            else
            {
                mKeyAuthority.addPubKey(senderGuid, senderPubKey);
                mAuthSender.sendPublicKey(404, myInfo.PublicKey, remotePoint);
            }
        }

        public void handleResponsePublicKey(byte[] guid, String pk, IPEndPoint remotePoint)
        {
            // store in Key Authority
            if (mKeyAuthority.containsKey(guid))
            {
                mKeyAuthority.replacePubKey(guid, pk);
            }
            else
            {
                mKeyAuthority.addPubKey(guid, pk);
                mAuthSender.requestPublicKey(404, myInfo.PublicKey, remotePoint);
            }
        }

        public void handleInvitationResponse(int senderGuid, Boolean flag, IPEndPoint remoteEndpoint)
        {
            if (flag)
            {
                if (mUserHelper.containsKeyInContactsList(senderGuid))
                {
                    String temp = mUserHelper.getUserByGuidFromOutgoingInvitations(senderGuid);
                    mUserHelper.replaceUsernameInContactsList(senderGuid, temp);
                }
                else
                {
                    String temp = mUserHelper.getUserByGuidFromOutgoingInvitations(senderGuid);
                    mUserHelper.addUsernameToContactsList(senderGuid, temp);
                }
            }
            else
            {
                // TODO: inform user somehow that invitation got rejected ? 
            }

            mUserHelper.removeOutgoingInvitation(senderGuid);

            updateLabelIncomingInvitations(this.mUserHelper.getNrOfIncomingInvitations());
            updateLabelOutgoingInvitations(this.mUserHelper.getNrOfOutgoingInvitations());
        }

        public void handleInvitationRequest(int senderGuid, String senderUsername, IPEndPoint endpoint)
        {
            mUserHelper.addIncomingInvitation(senderGuid, senderUsername);

            updateLabelIncomingInvitations(this.mUserHelper.getNrOfIncomingInvitations());
        }

        public void sendInvitationToUser(int guid, String username, string ip, int port)
        {
            // form endpoint
            IPEndPoint temp = new IPEndPoint(IPAddress.Parse(ip), port);
            // invite and send my guid, username and endpoint
            mUserSender.inviteRequest(404, myInfo.Username, temp);
            // add guid and username into list of sent out invitations
            mUserHelper.addOutgoingInvitation(guid, username);

            updateLabelIncomingInvitations(mUserHelper.getNrOfIncomingInvitations());
            updateLabelOutgoingInvitations(mUserHelper.getNrOfOutgoingInvitations());
        }

        public void respondToInvitationFromUser(int guid, Boolean answer, string ip, int port)
        {
            // create endpoint
            IPEndPoint temp = new IPEndPoint(IPAddress.Parse(ip), port);

            if (answer)
            {
                if (mUserHelper.containsKeyInContactsList(guid))
                {
                    String name = mUserHelper.getUserByGuidFromIncomingInvitations(guid);
                    mUserHelper.replaceUsernameInContactsList(guid, name);
                }
                else
                {
                    String name = mUserHelper.getUserByGuidFromIncomingInvitations(guid);
                    mUserHelper.addUsernameToContactsList(guid, name);
                }
            }

            // send my guid and answer (true = add each other to contacts list | false = denied)
            mUserSender.inviteResponse(404, answer, temp);

            mUserHelper.removeIncomingInvitation(guid);

            updateLabelIncomingInvitations(mUserHelper.getNrOfIncomingInvitations());
            updateLabelOutgoingInvitations(mUserHelper.getNrOfOutgoingInvitations());
        }

        public int getIncomingInvitationsCout()
        {
            return this.mUserHelper.getNrOfIncomingInvitations();
        }

        public int getOutgoingInvitationsCout()
        {
            return this.mUserHelper.getNrOfOutgoingInvitations();
        }

        public int getCountValues()
        {
            return mKeyAuthority.countKeyValues();
        }

        public Dictionary<byte[], String> getPublicKeys()
        {
            return mKeyAuthority.getPeerKeys();
        }

        /// <summary>
        /// GET AND SET
        /// </summary>

        // Getting routing table
        public RoutingTable getRoutingTable
        {
            get { return this.routingTable; }
        }

        public Dictionary<int, String> getUserInvitations()
        {
            return this.mUserHelper.getListOfIncomingInvitations();
        }

        public KademliaSenderUtils getKademliaSender()
        {
            return mKademliaSender;
        }

        // Getting peer GUID
        public byte[] GUID
        {
            get { return myInfo.GUID; }
        }

        public string IP
        {
            get { return myInfo.IP; }
        }

        public int UDPport
        {
            get { return myInfo.UDPport; }
        }

        public PeerInfo MyInfo
        {
            get { return myInfo; }
        }

        public void setBootPeer(int guid, String ip, int port, string stringguid)
        {
            bootPeer = new PeerInfo(ip, port, mTcpPort, mRSAHelperBootPeer.publicKey, "UsernameBootPeer", ByteUtils.convertStringToBytes(stringguid));
        }

        public FileDownloadManager FileDownloadManager
        {
            get { return this.fd; }
        }

        public int TCPPort
        {
            get { return this.mTcpPort; }
        }
    }
}
