﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedDesk.UDP_protocol;
using System.Net;
using System.Threading;

namespace SharedDesk.Kadelima
{
    public class SearchChannel
    {
        private byte[] mTargetGUID;
        private byte[] mPreviousGUID = null;

        private Timer mTimer;
        private int mPunchCount;
        private KademliaSenderUtils mKademliaSender;

        private string mIPAddress;
        private int mPort;

        bool isHolePunched = false;

        public event onFound onPeerFound;
        public delegate void onFound(PeerInfo p, byte[] targetGUID);

        // Initialized by passing the target peer (for the search) guid
        public SearchChannel(byte[] targetGUID, KademliaSenderUtils kademliaSender)
        {
            mTargetGUID = targetGUID;
            mKademliaSender = kademliaSender;
        }

        /// <summary>
        /// Initiates search for peer and then iterates until target or closest to target is found.
        /// Then it triggers onFound event.
        /// </summary>
        /// <param name="pInfo">PeerInfo object of the closest known peer to target peer.</param>
        public void onReceiveClosest(PeerInfo pInfo)
        {
            // if target not found or current closest is our previous closest
            if (mTargetGUID.SequenceEqual(pInfo.GUID) || (mPreviousGUID != null && mPreviousGUID.SequenceEqual(pInfo.GUID)))
            {
                // call onFound event
                onPeerFound(pInfo, mTargetGUID);
            }
            else
            {
                // current closest becomes previous closest
                mPreviousGUID = pInfo.GUID;
                // get peer instance
                Peer owner = Peer.getInstance();
                // ask closest peer if it knows a closer peer to the target.
                IPEndPoint remotePoint = new IPEndPoint(IPAddress.Parse(pInfo.IP), pInfo.UDPport);
                mKademliaSender.sendRequestClosest(owner.GUID, mTargetGUID, owner.TCPPort, remotePoint);
            }
        }

        public void setHolePunched(bool isPunched) 
        {
            isHolePunched = isPunched;
        }

        // Gets target peer GUID
        public byte[] TargetGUID
        {
            get { return this.mTargetGUID; }
        }

        //STUN

        public void doHolePunch(string ipAddress, int port, int targetGuid)
        {
            Peer owner = Peer.getInstance();
            owner.sendSTUN_punch_request(ipAddress, port, targetGuid);
            mIPAddress = ipAddress;
            mPort = port;
            mPunchCount = 0;

            //start timer and keep sending punch periodically until you get reply.
            RunPunchTimer();
        }

        public void RunPunchTimer()
        {
            //TimerCanceled = false;
            TimerCallback TimerDelegate = new TimerCallback(TimerTask);

            mTimer = new Timer(TimerDelegate, null, 0, 500);
        }

        private void TimerTask(object o)
        {
            if (mPunchCount < 5)
            {
                //mOwner.sendSTUN_punch(mIPAddress, mPort, mCurrentTargetGUID);
                mPunchCount++;
            }
            else {
                mTimer.Dispose();
                Console.WriteLine("Punch count reached at 5 times.");
            }
        }
    }
}
