﻿using SharedDesk.TCP_file_transfer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedDesk.interfaces
{
    public interface IStoreData
    {
        List<FileInfoP2P> getAvaliableFileDownloads();
        List<FileInfoP2P> getAvaliableFileDownloads(FileInfoP2P.Status status);
        List<FileInfoP2P> getYourSharedFiles();

        List<String> getPublicFilePathsList();

        /// <returns>guids of peers that have that file</returns>
        List<string> findPeersWithFile(string md5);

        bool fileExist(string md5);
        string getFullFilePathOfSharedFile(string md5);
        string getMd5OfFile(string name);
        string getFileName(string md5);
        long getSize(string md5);

        /// <summary>
        /// Stores a file with the calculated md5 publickly
        /// </summary>
        /// <param name="path"></param>
        void authorizePublicFileDownload(FileInfo file, string md5);

        /// <summary>
        /// Stores file and md5 etc and sets it to shared with spesific peer
        /// </summary>
        void authorizeFileDownloadWithPeer(FileInfo file, string md5, PeerInfo peer);
        void setStatusOfFile(string md5, FileInfoP2P.Status status);
        void addKnownFile(string guid, FileInfoP2P file);

        void setGuid(string guid);
        string getGuid();
        void setPort(int port);
        int getPort();
        void setBootPeerIP(string ip);
        string getBootPeerIP();
        void setBootPeerPort(int port);
        int getBootPeerPort();

        /// <summary>
        /// Stores users/peers with corresponding username
        /// </summary>
        /// <param name="guid">guid of user</param>
        void addUser(string guid, string username);

        string getUsername(string guid);

        /// <summary>
        /// Remove/reset all data. Mainly for testing
        /// </summary>
        void resetDB();
    }
}
