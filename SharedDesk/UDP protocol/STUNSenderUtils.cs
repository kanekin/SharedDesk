﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
// ShareDesk Namespace
using SharedDesk.Common;

namespace SharedDesk.UDP_protocol
{
    public class STUNSenderUtils
    {
        static IPEndPoint mStunServerEndPoint = new IPEndPoint(
           IPAddress.Parse(UDPConstants.STUN_SERVER_IP), UDPConstants.STUN_SERVER_PORT);

        private Socket mSocket = null;

        public STUNSenderUtils(Socket socket) 
        {
            mSocket = socket;
        }

        /// <summary>
        /// sending STUN reuest to STUN server
        ///
        /// </summary>
        public void sendSTUN_request()
        {
            // byte indicating what type of packet it is
            byte[] commandByte = new byte[] {(int) UDPConstants.commandByte.S_STUN_REQUEST };

            // Sending UPD packet
            mSocket.SendTo(commandByte, mStunServerEndPoint);
            Console.WriteLine("\nSTUN Client");
            Console.WriteLine("Sending STUN Request from {0} to {1}", IPAddress.Any, mStunServerEndPoint.Address);

        }

        /// <summary>
        ///sending punch request to server. Server will forward the request to the destination.
        /// [Contents]
        ///  *destinationPort: server will use this to forward
        ///  *destinationIpLength: length of IPAddress. server will use to convert ipAddress back to string
        ///  *destinationIpAddress: server will use this to forward
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="destinationIpAddress"></param>
        /// <param name="destinationPort"></param>
        public void sendSTUN_PunchRequest( string destinationIpAddress, int destinationPort, int targetGuid)
        {
            // byte indicating what type of packet it is
            byte[] commandByte = new byte[] {(int) UDPConstants.commandByte.S_STUN_PUNCH_REQUEST };

            byte[] destinationPortByteArray = BitConverter.GetBytes(destinationPort);

            byte[] destinationIpByteArray = ByteUtils.convertStringToBytes(destinationIpAddress);
            byte[] destinationIpLengthByteArray = BitConverter.GetBytes(destinationIpByteArray.Length);

            byte[] targetGuidByteArray = BitConverter.GetBytes(targetGuid);

            byte[] sendingPacket = ByteUtils.combineBytes(commandByte, targetGuidByteArray, destinationPortByteArray, destinationIpLengthByteArray, destinationIpByteArray);

            // Sending UPD packet
            mSocket.SendTo(sendingPacket, mStunServerEndPoint);
            Console.WriteLine("\nSTUN Sender");
            Console.WriteLine("Sending punch request from {0} to {1}", IPAddress.Any, mStunServerEndPoint.Address);
        }

        /// <summary>
        ///  hole pucnihg
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="ipAddress"></param>
        /// <param name="port"></param>
        /// <param name="targetGuid">use this as channel identifier</param>
        public void sendSTUN_Punch(string ipAddress, int port, int targetGuid)
        {
            // byte indicating what type of packet it is
            byte[] commandByte = new byte[] { (int) UDPConstants.commandByte.S_STUN_PUNCH };
            // byte indicating what type of packet it is
            byte[] targetGuidByte = BitConverter.GetBytes( targetGuid ) ;

            byte[] sendingPacket = ByteUtils.combineBytes(commandByte, targetGuidByte);
            IPEndPoint destinationEndPoint = new IPEndPoint(IPAddress.Parse(ipAddress), port);

            // Sending UPD packet
            mSocket.SendTo(commandByte, destinationEndPoint);
            Console.WriteLine("\nSTUN Sender");
            Console.WriteLine("Sending STUN punch from {0} to {1}:{2}", IPAddress.Any, destinationEndPoint.Address, destinationEndPoint.Port);
        }

        public void sendSTUN_KeepAlive(string ipAddress, int port)
        {

            // byte indicating what type of packet it is
            byte[] commandByte = new byte[] { (int) UDPConstants.commandByte.S_STUN_KEEP_ALIVE };

            IPEndPoint destinationEndPoint = new IPEndPoint(IPAddress.Parse(ipAddress), port);

            // Sending UPD packet
            mSocket.SendTo(commandByte, destinationEndPoint);
            // Console.WriteLine("\nUDP Responder");
            // Console.WriteLine("Sending keep alive from {0} to {1}", IPAddress.Any, destinationEndPoint.Address);
        }
    }
}
