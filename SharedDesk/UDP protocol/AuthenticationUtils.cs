using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace SharedDesk.UDP_protocol
{
    public class AuthenticationUtils
    {
        private Socket mSocket = null;

        public AuthenticationUtils(Socket socket)
        {
            mSocket = socket;
        }

        public void requestPublicKey(int myGuid, String myPublicKey, EndPoint endPoint)
        {
            // Byte indicating what type of packet it is
            byte[] commandByte = new byte[] { (int)UDPConstants.commandByte.A_PUBLICKEY_REQUEST };

            byte[] guid = new byte[] { (byte)myGuid };

            byte[] buffer = combineBytes(commandByte, guid);

            byte[] key = GetBytes(myPublicKey);

            byte[] sendBuffer = combineBytes(buffer, key);

            mSocket.SendTo(sendBuffer, endPoint);

            Console.WriteLine("\nUDP Responder");
            Console.WriteLine("Requesting public key of peer {0}", endPoint);
        }

        public void sendPublicKey(int myGuid, String myPublicKey, EndPoint endPoint)
        {
            // Byte indicating what type of packet it is
            byte[] commandByte = new byte[] { (int)UDPConstants.commandByte.A_PUBLICKEY_RESPONSE };

            byte[] guid = new byte[] { (byte)myGuid };

            byte[] buffer = combineBytes(commandByte, guid);

            byte[] key = GetBytes(myPublicKey);

            byte[] sendBuffer = combineBytes(buffer, key);

            mSocket.SendTo(sendBuffer, endPoint);

            Console.WriteLine("\nUDP Responder");

            Console.WriteLine("Sending public key {0} of peer with GUID {1} to peer {2}", myPublicKey, myGuid.ToString(), endPoint);
        }

        /// <summary>
        /// Byte[] Functionality
        /// </summary>

        // Combines two byte arrays to one
        public byte[] combineBytes(byte[] first, byte[] second)
        {
            byte[] ret = new byte[first.Length + second.Length];
            Buffer.BlockCopy(first, 0, ret, 0, first.Length);
            Buffer.BlockCopy(second, 0, ret, first.Length, second.Length);
            return ret;
        }

        static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }
    }
}
