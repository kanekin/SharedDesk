﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SharedDesk.UDP_protocol
{
    public class AndroidHandler
    {
        //public event handlerFileListRequestReceived onReceiveFileListRequest;
        //public delegate void handlerFileListRequestReceived(EndPoint remoteEnd);

        //public event handlerFileTansferRequestReceived onReceiveFileTransferRequest;
        //public delegate void handlerFileTansferRequestReceived(IPEndPoint remoteEnd, String md5);

        public void handleFileListRequest(EndPoint remoteEnd)
        {
            IPEndPoint remoteIpEndPoint = remoteEnd as IPEndPoint;
            Console.WriteLine("Received a FileListRequest from: {0}, port: {1}", remoteIpEndPoint.Address, remoteIpEndPoint.Port);

            //onReceiveFileListRequest(remoteEnd);
            Peer.getInstance().sendFileList(remoteEnd);

        }

        public void handleFileRequest(EndPoint remoteEnd, byte[] buffer)
        {
            IPEndPoint remoteIpEndPoint = remoteEnd as IPEndPoint;
            byte[] portByte =buffer.Skip(1).Take(4).ToArray();
            int port = BitConverter.ToInt32(portByte, 0);
            byte[] md5Bytes = buffer.Skip(5).Take(16).ToArray();
            string md5String = BitConverter.ToString(md5Bytes);
            Console.WriteLine(md5String);
            remoteIpEndPoint.Port = port;
            Console.WriteLine("Received a FileListRequest from: {0}, port: {1}", remoteIpEndPoint.Address, remoteIpEndPoint.Port);

            //onReceiveFileTransferRequest(remoteIpEndPoint, md5String);
            Peer.getInstance().handleRequestFileAndroid(remoteIpEndPoint, md5String);
        }
    }
}
