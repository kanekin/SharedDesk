﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using SharedDesk.TCP_file_transfer;
using SharedDesk;
using SharedDesk.interfaces;
using SharedDesk.Common;

namespace SharedDesk.UDP_protocol
{
    public class FileHandler
    {
        private Peer peer;
        private TransferFile tf = null;
        private PeerInfo myInfo;

        //public event handlerRequestFilePart receiveRequestFilePart;
        //public delegate void handlerRequestFilePart(string md5, int[] parts, IPEndPoint endpoint);

        public event handlerSearchTable receiveSearchTable;
        public delegate void handlerSearchTable(RoutingTable table);

        public event handlerPublicFileSearch receivePublicFileSearch;
        public delegate void handlerPublicFileSearch(EndPoint remotePoint);


        public FileHandler(PeerInfo myInfo)
        {

            this.myInfo = myInfo;
            this.peer =  Peer.getInstance();

            // Set TCP file transfer protocol
            tf = new TransferFile(new TransferTCPv5());
        }

        public void listenForFile(string path, IPEndPoint endPoint)
        {
            tf.listenForFile(path, endPoint);
        }

        public void handlePublicFileSearch(EndPoint remoteEnd, byte[] buff)
        {
            byte[] guidByteArray = buff.Skip(1).Take(1).ToArray();
            int remoteGuid = guidByteArray[0];
            Console.WriteLine("Received public file search from {0}", remoteGuid);
            Peer.getInstance().handlePublicFileSearch(remoteEnd);
            //receivePublicFileSearch(remoteEnd);
        }

        public void handleSearchTable(EndPoint remoteEnd, byte[] buff)
        {
            byte[] guidByteArray = buff.Skip(1).Take(1).ToArray();
            int remoteGuid = guidByteArray[0];
            byte[] data = buff.Skip(2).ToArray();
            Dictionary<byte[], PeerInfo> routingTable = ByteUtils.byteArrayToPeerDictionary(data);
            Console.WriteLine("Received search table from {0}", remoteGuid);
            //DONT REMOVE NEXT LINE
            //Peer.getInstance().handleSearchTable(routingTable);
        }

        public void handleFileInfo(EndPoint remoteEnd, byte[] buff)
        {
            IPEndPoint remoteIPEnd = remoteEnd as IPEndPoint;

            Console.WriteLine("Received a file info from: {0}, listen port: {1}", remoteEnd, remoteIPEnd.Port);
            // gets the first byte
            byte[] fileNameLengthByte = new byte[1];
            fileNameLengthByte = buff.Skip(1).Take(1).ToArray();

            // first byte has a value 0 - 255
            int fileNameLength = Convert.ToInt32(fileNameLengthByte[0]);

            // Windows 1252 encoding 
            Encoding encoding1252 = Encoding.GetEncoding(1252);
            string fileName = encoding1252.GetString(buff, 2, fileNameLength);

            byte[] fileSizeB = buff.Skip(1).Skip(1 + fileNameLength).Take(8).ToArray();
            long fileSize = BitConverter.ToInt64(fileSizeB, 0);

            // get md5 hash
            // md5 hash is 16 bytes = 128 bit
            byte[] md5 = buff.Skip(1).Skip(1 + fileNameLength + 8).Take(16).ToArray();
            //string md5String = BitConverter.ToString(md5).Replace("-", "");
            string md5String = BitConverter.ToString(md5);

            int skip = fileNameLength + 26;

            //int receivedGuid = BitConverter.ToInt32(buff, skip);
            byte[] guidByte = buff.Skip(skip).Take(Constants.GUID_SIZE).ToArray();
            string senderGUID = ByteUtils.convertByteToString(guidByte);

            //Console.WriteLine("\nFile Info:");
            Console.WriteLine(String.Format("\nFile info received from: {0}", senderGUID));
            Console.WriteLine("Name: {0}", fileName);
            Console.WriteLine("Size: {0}", fileSize.ToString());
            Console.WriteLine("md5: {0}", md5String);

            // create ip end point from udp packet ip and listen port received
            IPEndPoint remoteIpEndPoint = remoteEnd as IPEndPoint;

            // create FileInfoP2P object
            FileInfoP2P file = new FileInfoP2P(fileName, fileSize, md5, FileInfoP2P.Status.newFile);

            // store this information

            peer.fileHelper.addKnownFile(senderGUID, file);

            // respond to ping (send guid)
            //UDPResponder udpResponse = new UDPResponder(remoteIpEndPoint, port);
            //udpResponse.sendGUID(guid);
        }

        public void handleFileTransferRequest(EndPoint remoteEnd, byte[] buff)
        {
            // get UDP listening port from peer
            byte[] portByteArray = buff.Skip(1).Take(16).ToArray();
            int tcpPort = BitConverter.ToInt32(portByteArray, 0);
 
            // get guid
            byte[] receverGuid = buff.Skip(21).Take(4).ToArray();
            int receivedGuid = BitConverter.ToInt32(receverGuid, 0);

            // get md5 hash
            // md5 hash is 16 bytes = 128 bit
            byte[] md5 = buff.Skip(5).Take(16).ToArray();
            //string md5String = BitConverter.ToString(md5).Replace("-", "");
            string md5String = BitConverter.ToString(md5);

            // can optionly send a open TCP port as well 
            // then the file transfer can be start instantly 

            Console.WriteLine(String.Format("\nFile transfer request received from: {0}", remoteEnd));
            Console.WriteLine("md5: {0}", md5String);
            Console.WriteLine("tcp port: {0}", tcpPort);
            Console.WriteLine("guid: {0}", receivedGuid);

            // TODO: 
            // check if peer is authorized to download file
            // should be stores in share.xml

            //int answer;
            if (peer.fileHelper.fileExist(md5String))
            {
                // file exist in share.xml
                Console.WriteLine("Found file in share.xml, file transfer is approved!");

                // start TCP file transfer
                if (tcpPort < 0 || tcpPort > 65535)
                {
                    Console.WriteLine("Got invalid TCP port, cannot start file transfer...");
                    return;
                }

                string filePath = peer.fileHelper.getFullFilePathOfSharedFile(md5String);

                if (filePath == "")
                {
                    Console.WriteLine("Could not get the file path!");
                }

                IPEndPoint remotePeer = remoteEnd as IPEndPoint;
                remotePeer.Port = tcpPort;


                // send file!
                tf.sendFile(filePath, remotePeer);
                Console.WriteLine("Sending file {0}, to {1}", filePath, remotePeer);


            }
            else
            {
                // file does not exist in share.xml
                Console.WriteLine("File not found in share.xml, will respond with file not found");
            }



            // create ip end point from udp packet ip and listen port received


            //// request the file right away!
            //UDPResponder udpResponse = new UDPResponder(remoteIpEndPoint, port);
            //udpResponse.sendFileRequestResponse(answer, md5);
        }

        public void handleFilePartRequest(EndPoint remoteEnd, byte[] buff)
        {
            // get UDP listening port from peer
            ByteSeparator bs = new ByteSeparator(buff, true);
            byte[] portByteArray = bs.getTargetByte(Constants.TCP_PORT_SIZE);
            int tcpPort = BitConverter.ToInt32(portByteArray, 0);

            // get md5 hash
            // md5 hash is 16 bytes = 128 bit
            byte[] md5 = bs.getTargetByte(Constants.MD5_SIZE);
            //string md5String = BitConverter.ToString(md5).Replace("-", "");
            string md5String = BitConverter.ToString(md5);
            
            // get guid
            byte[] receiverGuid = bs.getTargetByte(Constants.GUID_SIZE);
            string receivedGuid = ByteUtils.convertByteToString(receiverGuid);

            byte[] nmrOfPartsByteArr = bs.getTargetByte(4);
            int nmrOfParts = BitConverter.ToInt32(nmrOfPartsByteArr, 0);

            byte[] partByteArr = bs.getTargetByte(4 * nmrOfParts);
            int[] parts = new int[partByteArr.Length / 4];

            for (int n = 0; n < partByteArr.Length; n += 4)
            {
                parts[n / 4] = BitConverter.ToInt32(partByteArr, n);
            }
            // can optionly send a open TCP port as well 
            // then the file transfer can be start instantly 

            Console.WriteLine(String.Format("\nFile transfer request received from: {0}", remoteEnd));
            Console.WriteLine("md5: {0}", md5String);
            Console.WriteLine("tcp port: {0}", tcpPort);
            Console.WriteLine("guid: {0}", receivedGuid);

            // TODO: 
            // check if peer is authorized to download file
            // should be stores in share.xml

            //int answer;
            if (peer.fileHelper.fileExist(md5String))
            {
                // file exist in share.xml
                Console.WriteLine("Found file in share.xml, file transfer is approved!");

                // start TCP file transfer
                if (tcpPort < 0 || tcpPort > 65535)
                {
                    Console.WriteLine("Got invalid TCP port, cannot start file transfer...");
                    return;
                }

                string filePath = peer.fileHelper.getFullFilePathOfSharedFile(md5String);

                if (filePath == "")
                {
                    Console.WriteLine("Could not get the file path!");
                }

                IPEndPoint remotePeer = remoteEnd as IPEndPoint;
                remotePeer.Port = tcpPort;

                // handle request
                Peer.getInstance().handleRequestFilePart(md5String, parts, remotePeer);
                Console.WriteLine("Sending file parts, to {0}", remotePeer);
            }
            else
            {
                // file does not exist in share.xml
                Console.WriteLine("File not found in share.xml, will respond with file not found");
            }
        }
 

        public void handleFileTransferResponse(EndPoint remoteEnd, byte[] buff)
        {
            // get md5 hash
            // md5 hash is 16 bytes = 128 bit
            byte[] md5 = buff.Skip(5).Take(16).ToArray();
            string md5String = BitConverter.ToString(md5);

            Console.WriteLine("Received a file transfer response from: {0},  md5: {1}", remoteEnd, md5String);

            byte[] answerByte = buff.Skip(21).ToArray();
            int answer = BitConverter.ToInt32(answerByte, 0);

            switch (answer)
            {
                case 1:
                    // approved
                    Console.WriteLine("File transfer has been approved, can open TCP port");

                    //ReceiveFileTCPv5 listen = new ReceiveFileTCPv5()

                    break;
                case 2:
                    // does not have file
                    Console.WriteLine("Could not find file requested.");
                    break;
                case 3:
                    // unathorized
                    Console.WriteLine("Peer is NOT authorized to download this file!");
                    break;
                case 4:
                    // declined
                    Console.WriteLine("File transfer request has been declined!");
                    break;
                default:
                    Console.WriteLine("ERROR: Unknown response!");
                    return;
            }


            //// TODO: 
            //// check if peer is authorized to download file
            //// should be stores in share.xml

            //int answer;
            //if (filehelper.fileExistInShareXml(md5String))
            //{
            //    // file exist in share.xml
            //    answer = 1;
            //    Console.WriteLine("Found file in share.xml, file transfer is approved!");
            //}
            //else
            //{
            //    // file does not exist in share.xml
            //    answer = 2;
            //    Console.WriteLine("File not found in share.xml, will respond with file not found");
            //}

            //// create ip end point from udp packet ip and listen port received
            //IPEndPoint remoteIpEndPoint = remoteEnd as IPEndPoint;
            //remoteIpEndPoint.Port = port;

            ////// request the file right away!
            //UDPResponder udpResponse = new UDPResponder(remoteIpEndPoint, port);
            //udpResponse.sendFileRequestResponse(answer);
        }
        /*
        private void handlePublicFileSearch(EndPoint remotePoint)
        {
            List<FileInfoP2P> publicFiles = this.filehelper.getPublicFiles();
            if (publicFiles == null)
            {
                // found no public files
                return;
            }
            if (publicFiles.Count != 0)
            {
                foreach (FileInfoP2P fileInfo in publicFiles)
                {
                    FileSenderUtils.getInstance().sendFileInfo(fileInfo.Path, myInfo.getGUID, remotePoint, fileInfo.getMd5AsString());
                }
            }
        }
         * */
    }
}
