﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace SharedDesk.UDP_protocol
{
    public class UserHandler
    {
        /// <summary>
        /// EVENTS FOR PEER
        /// </summary>
        //public event handlerInvitationRequest onInvitationRequest;
        //public delegate void handlerInvitationRequest(int guid, String username, IPEndPoint endpoint);

        //public event handlerInvitationResponse onInvitationResponse;
        //public delegate void handlerInvitationResponse(int guid, Boolean flag, IPEndPoint endpoint);

        public UserHandler() { }

        public void handleInvitationRequest(EndPoint remoteEnd, byte[] buff)
        {
            // create ip end point from udp packet ip and listen port received
            IPEndPoint remoteIpEndPoint = remoteEnd as IPEndPoint;

            byte[] guid = buff.Skip(1).Take(1).ToArray();
            int senderGuid = guid[0];

            byte[] length = buff.Skip(2).Take(1).ToArray();
            int nameLength = (int)length[0];

            byte[] username = buff.Skip(3).Take(nameLength).ToArray();
            String senderUsername = GetString(username);

            Console.WriteLine("Received an invitation from {0} with GUID {1} and username {2}", remoteEnd, senderGuid.ToString(), senderUsername);

            //onInvitationRequest(senderGuid, senderUsername, remoteIpEndPoint);
            Peer.getInstance().handleInvitationRequest(senderGuid, senderUsername, remoteIpEndPoint);
        }

        public void handleInvitationResponse(EndPoint remoteEnd, byte[] buff)
        {
            // create ip end point from udp packet ip and listen port received
            IPEndPoint remoteIpEndPoint = remoteEnd as IPEndPoint;

            byte[] guid = buff.Skip(1).Take(1).ToArray();
            int senderGuid = guid[0];

            byte[] answerFlag = buff.Skip(2).Take(1).ToArray();
            Boolean flag = BAToBool(answerFlag);

            Console.WriteLine("Received answer ( {0} ) to invitation from {1} with GUID {2}", flag.ToString(), remoteEnd, senderGuid.ToString());

            //onInvitationResponse(senderGuid, flag, remoteIpEndPoint);
            Peer.getInstance().handleInvitationResponse(senderGuid, flag, remoteIpEndPoint);
        }

        // Convert a byte array element to bool and display it. 
        public static Boolean BAToBool(byte[] bytes)
        {
            Boolean value = BitConverter.ToBoolean(bytes, 0);
            return value;
        }

        static string GetString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }
    }
}
