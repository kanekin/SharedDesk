﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
// ShareDesk Namespace
using SharedDesk.Common;

namespace SharedDesk.UDP_protocol
{
    public class STUNHandler
    {
        public Timer mTimer;
        public bool TimerCanceled;


        /// <summary>
        /// EVENTS FOR STUN
        /// </summary>
        //public event handlerPunchRequested onPunchRequested;
        //public delegate void handlerPunchRequested(string ipAddress, int port, int targetGuid);

        //public event handlerStunRespond onReceiveServerRespond;
        //public delegate void handlerStunRespond(string ipAddress, int port);

        //public event handlerStunPunchReceived onReceivePunch;
        //public delegate void handlerStunPunchReceived (int targetGuid);


        Socket mSocket;

        public STUNHandler(Socket socket)
        {
            mSocket = socket;
        }

        public void handleStunResponse(EndPoint remoteEnd, byte[] buff)
        {
            byte[] portByteArray = buff.Skip(1).Take(4).ToArray();

            byte[] ipLengthByteArray = buff.Skip(5).Take(4).ToArray();
            int ipLength = BitConverter.ToInt32(ipLengthByteArray, 0);

            byte[] ipByteArray = buff.Skip(9).Take(ipLength).ToArray();

            int port = BitConverter.ToInt32(portByteArray, 0);
            string ipAddress = ByteUtils.convertByteToString(ipByteArray);

            //STUN_Sender.sendSTUN_respond(remoteEnd);
            // Create IP endpoint from udp packet
            IPEndPoint remoteIpEndPoint = remoteEnd as IPEndPoint;
            Console.WriteLine("Received a STUN RESPOND from: {0}, port: {1}", remoteIpEndPoint.Address, remoteIpEndPoint.Port);
            Console.WriteLine("Your Global IP {0},  port: {1}", ipAddress, port);

            //onReceiveServerRespond(ipAddress, port);
            Peer peer = Peer.getInstance();
            peer.updateMyInfo(ipAddress, port);
        }

        /// <summary>
        /// handling the forwarded punch request via STUN server. send punch to the origin.
        /// </summary>
        /// <param name="remoteEnd"></param>
        /// <param name="buff"></param>
        public void handleStunForwardedPunchRequest(EndPoint remoteEnd, byte[] buff)
        {
            byte[] targetGuidArray = buff.Skip(1).Take(4).ToArray();

            byte[] portByteArray = buff.Skip(1).Skip(4).Take(4).ToArray();

            byte[] ipLengthByteArray = buff.Skip(5).Take(4).ToArray();
            int ipLength = BitConverter.ToInt32(ipLengthByteArray, 0);

            byte[] ipByteArray = buff.Skip(9).Take(ipLength).ToArray();

            int targetGuid = BitConverter.ToInt32(targetGuidArray, 0);
            int port = BitConverter.ToInt32(portByteArray, 0);
            string ipAddress = ByteUtils.convertByteToString(ipByteArray);


            //onPunchRequested(ipAddress, port, targetGuid);
            Peer.getInstance().sendSTUN_punch(ipAddress, port, targetGuid);

            // Create IP endpoint from udp packet
            IPEndPoint remoteIpEndPoint = remoteEnd as IPEndPoint;
            Console.WriteLine("Received a STUN punch request forward from: {0}, port: {1}", remoteIpEndPoint.Address, remoteIpEndPoint.Port);
            Console.WriteLine("Send punching to {0}:{1}", ipAddress, port);

        }

        /// <summary>
        /// sending STUN respond to STUN 
        ///
        /// </summary>
        public void handleStunReceivePunch(EndPoint remoteEnd, byte[] buff)
        {
            // Create IP endpoint from udp packet
            IPEndPoint remoteIpEndPoint = remoteEnd as IPEndPoint;

            byte[] targetGuidArray = buff.Skip(1).Take(Constants.GUID_SIZE).ToArray();

            //onReceivePunch(targetGuid);
            Peer.getInstance().reopenChannel(targetGuidArray);

            Console.WriteLine("\nSTUN Handler");
            Console.WriteLine("Received STUN punch from {0}:{1}", remoteIpEndPoint.Address, remoteIpEndPoint.Port);

        }


    }
}
