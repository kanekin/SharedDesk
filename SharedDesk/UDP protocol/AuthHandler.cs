﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
// ShareDesk Namespace
using SharedDesk.Common;

namespace SharedDesk.UDP_protocol
{
    public class AuthHandler
    {
        /// <summary>
        /// EVENTS FOR PEER
        /// </summary>
        //public event handlerRequestPublicKey onPublicKeyRequest;
        //public delegate void handlerRequestPublicKey(int guid, String key, IPEndPoint endpoint);

        //public event handlerResponsePublicKey onPublicKeyResponse;
        //public delegate void handlerResponsePublicKey(int guid, String key, IPEndPoint endpoint);

        public AuthHandler() { }

        public void handlePublicKeyRequest(EndPoint remoteEnd, byte[] buff)
        {
            // create ip end point from udp packet ip and listen port received
            IPEndPoint remoteIpEndPoint = remoteEnd as IPEndPoint;

            byte[] guid = buff.Skip(1).Take(Constants.GUID_SIZE).ToArray();

            byte[] key = buff.Skip(2).Take(486).ToArray();
            String senderPubKey = GetString(key);

            Console.WriteLine("Received a request for public key from {0} with GUID {1} and public key {2}", remoteEnd, guid.ToString(), senderPubKey);

            //onPublicKeyRequest(senderGuid, senderPubKey, remoteIpEndPoint);
            Peer.getInstance().handleRequestPublicKey(guid, senderPubKey, remoteIpEndPoint);
        }

        public void handlePublicKeyResponse(EndPoint remoteEnd, byte[] buff)
        {
            // create ip end point from udp packet ip and listen port received
            IPEndPoint remoteIpEndPoint = remoteEnd as IPEndPoint;

            byte[] guid = buff.Skip(1).Take(Constants.GUID_SIZE).ToArray();

            byte[] key = buff.Skip(2).Take(486).ToArray();
            String senderPubKey = GetString(key);

            Console.WriteLine("Received public key {0} from {1} with GUID {2}", senderPubKey, remoteEnd, guid.ToString());

            //onPublicKeyResponse(senderGuid, senderPubKey, remoteIpEndPoint);
            Peer.getInstance().handleResponsePublicKey(guid, senderPubKey, remoteIpEndPoint);
        }

        static string GetString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }
    }
}
