﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
// ShareDesk Namespace
using SharedDesk.Common;

namespace SharedDesk.UDP_protocol
{
    public class KademliaSenderUtils
    {
        private Socket mSocket = null;

        private static KademliaSenderUtils mInstance;

        private KademliaSenderUtils(){}

        public static KademliaSenderUtils getInstance(){
            if (mInstance == null) {
                mInstance = new KademliaSenderUtils();
            }
            return mInstance;
        }

        public void setSocket(Socket socket){
            mSocket = socket;
        }

         /// <summary>
        /// RESPONSE FUNCTIONS
        /// </summary>
        
        // Sends ping udp packet to the endPoint
        public void sendPing(EndPoint endPoint)
        {
            try
            {
                // Byte indicating what type of packet it is
                byte[] commandByte = new byte[] { (int)UDPConstants.commandByte.K_PING_REQUEST };

                // Byte array with port 
                //byte[] listenPortByteArray = BitConverter.GetBytes(listenPort);

                // Buffer to send
                //byte[] sendBuffer = combineBytes(commandByte, listenPortByteArray);

                mSocket.SendTo(commandByte, endPoint);
                Console.WriteLine("\nUDP Responder");
                Console.WriteLine("Sending ping to {0}", endPoint);
            }
            catch (Exception e) {
                Console.WriteLine(e);
            }
        }

        // Sends peer guid - sendPing response
        public void sendGUID(int GUID, EndPoint endPoint)
        {
            byte[] guid = new byte[] { (byte)GUID };
            // Byte indicating what type of packet it is
            byte[] commandByte = new byte[] { (int)UDPConstants.commandByte.K_PING_RESPONSE};

            // Buffer to send
            byte[] sendBuffer = ByteUtils.combineBytes(commandByte, guid);

            mSocket.SendTo(sendBuffer, endPoint);
            Console.WriteLine("\nUDP Responder");
            Console.WriteLine("Sending guid to {0}", endPoint);
        }

        // Sends a request to join the table of the peer at the endpoint
        public void sendRequestJoin(PeerInfo myInfo, EndPoint endPoint)
        {
            // Byte indicating what type of packet it is
            byte[] commandByte = new byte[] { (int)UDPConstants.commandByte.K_JOIN };

            // Byte array with port 
            byte[] peerInfoByteArray = ByteUtils.peerInfoToByteArray(myInfo);

            // Buffer to send
            byte[] sendBuffer = ByteUtils.combineBytes(commandByte, peerInfoByteArray);

            mSocket.SendTo(sendBuffer, endPoint);
            Console.WriteLine("\nUDP Responder");
            Console.WriteLine("Sending request to join the table of peer {0}", endPoint);
        }

        // Sends a request to join the table of the peer at the endpoint
        public void sendRequestLeave(byte[] guid, EndPoint endPoint)
        {
            // Byte indicating what type of packet it is
            byte[] commandByte = new byte[] { (int)UDPConstants.commandByte.K_LEAVE };

            // Byte array with port 
            byte[] guidByteArray = guid;

            // Buffer to send
            byte[] sendBuffer = ByteUtils.combineBytes(commandByte, guidByteArray);

            mSocket.SendTo(sendBuffer, endPoint);
            Console.WriteLine("\nUDP Responder");
            Console.WriteLine("Sending request to join the table of peer {0}", endPoint);
        }

        // Sends a routing table request to the endpoint
        public void sendRequestRoutingTable(int listenPort, EndPoint endPoint)
        {
            // Byte indicating what type of packet it is
            byte[] commandByte = new byte[] { (int) UDPConstants.commandByte.K_ROUTINGTABLE_REQUEST };

            // Byte array with port 
            byte[] listenPortByteArray = BitConverter.GetBytes(listenPort);

            // Buffer to send
            byte[] sendBuffer = ByteUtils.combineBytes(commandByte, listenPortByteArray);

            mSocket.SendTo(sendBuffer, endPoint);
            Console.WriteLine("\nUDP Responder");
            Console.WriteLine("Sending routing table request to {0}", endPoint);
        }

        // Sends the closest found PeerInfo to the endpoint
        public void sendRequestClosest(byte[] self, byte[] target, int listenPort, EndPoint endPoint)
        {
            // Command byte
            byte[] commandByte = new byte[] { (int) UDPConstants.commandByte.K_CLOSESTPEER_REQUEST };

            // Own Guid to byte[]
            byte[] own_guid = self;
            // Target Guid to byte[]
            byte[] target_guid = target;

            // Listen port to byte[] 
            byte[] listenPortByteArray = BitConverter.GetBytes(listenPort);

            // Creating buffer to send
            byte[] sendBuffer = ByteUtils.combineBytes(commandByte, own_guid, target_guid, listenPortByteArray);

            mSocket.SendTo(sendBuffer, endPoint);
            Console.WriteLine("\nUDP Responder");
            Console.WriteLine("Sending find closest to {0} request to {1}", target, endPoint);
        }

        //sends the closest found PeerInfo to the endpoint
        public void sendClosest(int targetGUID, PeerInfo closest, EndPoint endPoint)
        {
            // Convert PeerInfo to byte[]
            byte[] peerInfoInBytes = ByteUtils.peerInfoToByteArray(closest);

            // Byte indicating what type of packet it is
            byte[] commandByte = new byte[] { (int)UDPConstants.commandByte.K_CLOSESTPEER_RESPONSE };

            byte[] targetGUIDInBytes = new byte[] { (byte)targetGUID };

            // Buffer to send
            byte[] tempBuffer = ByteUtils.combineBytes(commandByte, targetGUIDInBytes);
            byte[] sendBuffer = ByteUtils.combineBytes(tempBuffer, peerInfoInBytes);

            // Sending UPD packet
            mSocket.SendTo(sendBuffer, endPoint);
        }
    }
}
