﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
// ShareDesk Namespace
using SharedDesk.Common;

namespace SharedDesk.UDP_protocol
{
    public class FileSenderUtils
    {
        private static Socket mSocket = null;

        private static FileSenderUtils mInstance = null;

        private FileSenderUtils(){ }

        public static FileSenderUtils getInstance() {
            if (mInstance == null) {
                mInstance = new FileSenderUtils();
            }
            return mInstance;
        }

        public void setSocket(Socket socket){
            mSocket = socket;
        }

        public void sendFileRequest(string md5, int tcpPort, byte[] guid, IPEndPoint endPoint)
        {
            // byte indicating what type of packet it is
            byte[] commandByte = new byte[] { (int)UDPConstants.commandByte.F_FILETRANSFER_REQUEST };

            // byte array with port 
            byte[] tcpPortBytes = BitConverter.GetBytes(tcpPort);

            // guid that should received the file
            //byte[] receiverGuidBytes = ByteUtils.convertStringToBytes(guid);

            // make string into byte[] array
            //byte[] md5ByteArray = System.Text.Encoding.ASCII.GetBytes(md5);
            byte[] md5ByteArray = ChecksumCalc.md5StringToByteArray(md5);

            // buffer to send
            byte[] sendBuffer = ByteUtils.combineBytes(commandByte, tcpPortBytes, md5ByteArray, guid);

            mSocket.SendTo(sendBuffer, endPoint);
            Console.WriteLine("\nUDP Responder");
            Console.WriteLine("Sending file request to {0}", endPoint);
        }

        public void sendPublicFileSearchRequest(byte[] guid, IPEndPoint endPoint)
        {
            // byte indicating what type of packet it is
            byte[] commandByte = new byte[] { (int)UDPConstants.commandByte.F_SEARCH_PUBLIC };

            //byte[] guidBytes = ByteUtils.convertStringToBytes(guid);
            // buffer to send
            byte[] sendBuffer = ByteUtils.combineBytes(commandByte, guid);

            mSocket.SendTo(sendBuffer, endPoint);
            Console.WriteLine("\nUDP Responder");
            Console.WriteLine("Sending public file search request to {0}", endPoint);
        }

        public void sendSearchTable(string guid, RoutingTable table, IPEndPoint endPoint)
        {
            // byte indicating what type of packet it is
            byte[] commandByte = new byte[] { (int)UDPConstants.commandByte.F_SEARCH_TABLE_RECEIVED };

            byte[] guidBytes = ByteUtils.convertStringToBytes(guid);
            // buffer to send
            byte[] sendBuffer = ByteUtils.combineBytes(commandByte, guidBytes);

            mSocket.SendTo(sendBuffer, endPoint);
            Console.WriteLine("\nUDP Responder");
            Console.WriteLine("Sending public file search request to {0}", endPoint);
        }

        public static void sendFilePartRequest(string md5, int tcpPort, byte[] guid, int[] parts, IPEndPoint endPoint)
        {

            // byte indicating what type of packet it is
            byte[] commandByte = new byte[] { (int)UDPConstants.commandByte.F_FILEPART_REQUEST };

            // byte array with port 
            byte[] tcpPortBytes = BitConverter.GetBytes(tcpPort);

            // guid that should received the file
            //byte[] receiverGuidBytes = ByteUtils.convertStringToBytes(guid);

            // make string into byte[] array
            //byte[] md5ByteArray = System.Text.Encoding.ASCII.GetBytes(md5);
            byte[] md5ByteArray = ChecksumCalc.md5StringToByteArray(md5);

            // Number of parts to byte[]
            byte[] nmrOfPartByteArr = BitConverter.GetBytes(parts.Length);

            // Make parts into byte[]
            byte[] partByteArr = new byte[parts.Length * sizeof(int)];
            Buffer.BlockCopy(parts, 0, partByteArr, 0, partByteArr.Length);

            // buffer to send
            byte[] sendBuffer = ByteUtils.combineBytes(commandByte, tcpPortBytes, md5ByteArray, guid, nmrOfPartByteArr, partByteArr);

            mSocket.SendTo(sendBuffer, endPoint);
            Console.WriteLine("\nUDP Responder");
            Console.WriteLine("Sending file parts request to {0}", endPoint);
        }

        /// <summary>
        /// Send response to file request
        /// 
        /// 1 - file transfer approved
        /// 2 - does not have file
        /// 3 - unauthorized 
        /// 4 - declined
        /// </summary>
        /// <param name="answer"></param>
        public void sendFileRequestResponse(int answer, byte[] md5, IPEndPoint endPoint)
        {

            // byte indicating what type of packet it is
            byte[] commandByte = new byte[] { (int)UDPConstants.commandByte.F_FILETRANSFER_RESPONSE };

            byte[] commandPortMd5 = ByteUtils.combineBytes(commandByte, md5);

            // make string into byte[] array
            //byte[] md5ByteArray = System.Text.Encoding.ASCII.GetBytes(md5);
            byte[] answerByte = BitConverter.GetBytes(answer);

            // buffer to send
            byte[] sendBuffer = ByteUtils.combineBytes(commandPortMd5, answerByte);

            mSocket.SendTo(sendBuffer, endPoint);
            Console.WriteLine("\nUDP Responder");
            Console.WriteLine("Sending file request response to {0}", endPoint);
        }

        public void sendFileInfo(string filePath, byte[] guid, EndPoint endPoint, string md5String)
        {

            // check if file exists
            if (File.Exists(filePath) == false)
            {
                return;
            }

            byte[] md5 = null;
            if (md5String == "")
            {
                // get md5 hash
                md5 = ChecksumCalc.GetMD5Checksum(filePath);
                if (md5 == null)
                {
                    return;
                }
            }
            else
            {
                md5 = ChecksumCalc.md5StringToByteArray(md5String);
            }

            FileInfo f = new FileInfo(filePath);

            // Using windows 1252 encoding
            Encoding encoding1252 = Encoding.GetEncoding(1252);

            //byte[] fileName = Encoding.ASCII.GetBytes(f.Name);
            byte[] fileName = encoding1252.GetBytes(f.Name);
            byte[] filenameSizePlusFilename = new byte[fileName.Length + 1];

            // copy byte representing the size of the filename
            // to the beginning of the first packet sent
            int length = fileName.Length;
            byte lengthB = (byte)length;
            new byte[] { lengthB }.CopyTo(filenameSizePlusFilename, 0);

            // copies file name in ASCII format from index 1 to length of filename
            Array.Copy(fileName, 0, filenameSizePlusFilename, 1, fileName.Length);

            // get file size and convert to 8 bytes
            long fileSize = f.Length;
            byte[] fileSizeB = BitConverter.GetBytes(fileSize);

            // preBuffer has size:
            // file name +
            // 1 byte for file name size +
            // 8 bytes is the size of the file + 
            // 16 bytes is the md5 hash of the file 
            byte[] preBuffer = new byte[fileName.Length + 1 + 8 + 16];

            // copy file name size and file name to preBuffer
            filenameSizePlusFilename.CopyTo(preBuffer, 0);

            // copy fileSizeB to end of preBuffer
            Array.Copy(fileSizeB, 0, preBuffer, fileName.Length + 1, 8);

            // copy md5 hash to preBuffer
            Array.Copy(md5, 0, preBuffer, fileName.Length + 1 + 8, 16);

            // format the transfer like this:
            // byte = filename size
            // file name
            // long = file size in bytes, ulong is 64 bit so filesize could be limitless
            // 16 bytes md5 hash of file
            // file content

            // byte indicating what type of packet it is
            byte[] commandByte = new byte[] { (int) UDPConstants.commandByte.F_FILEINFO };

            // Creating buffer to send
            byte[] fileInfoBuffer = ByteUtils.combineBytes(commandByte, preBuffer);

            // convert guid to byte array
            //byte[] guidByte = ByteUtils.convertStringToBytes(guid);

            byte[] sendBuffer = ByteUtils.combineBytes(fileInfoBuffer, guid);

            // Sending UPD packet
            mSocket.SendTo(sendBuffer, endPoint);
            Console.WriteLine("\nUDP Responder");
            Console.WriteLine(String.Format("Sending file info to {0}, file: \"{1}\", size: \"{2}\"", endPoint.ToString(), f.Name, sendBuffer.Length));
        }

        /// <summary>
        /// Send list of file infos to Android devices. One packet should containt upto 10 items. 
        /// If the size of the list is bigger call this method multiple times with separated list.
        /// </summary>
        /// <param name="filePathList"></param>
        /// <param name="guid"></param>
        /// <param name="endPoint"></param>
        public void sendFileInfoList(List<string> filePathList, byte[] guid, EndPoint endPoint)
        {
            int numOfFiles = filePathList.Count;
            //check if number of file infos, it should be >0 
            if (numOfFiles <= 0 && numOfFiles > 10)
            {
                return;
            }

            byte[] numOfFilesBytes = BitConverter.GetBytes(numOfFiles);
            byte[] listBuffer = new byte[0];

            foreach (string filePath in filePathList)
            {
                // Creating buffer to send
                byte[] fileInfoBuffer = makeFileInfoByte(filePath);

                if (fileInfoBuffer != null)
                {
                    listBuffer = ByteUtils.combineBytes(listBuffer, fileInfoBuffer);
                }
            }

            // convert guid to byte array
            //byte[] guidByte = ByteUtils.convertStringToBytes(guid);

            // byte indicating what type of packet it is
            byte[] commandByte = new byte[] { (int)UDPConstants.commandByte.F_FILEINFO };

            byte[] sendBuffer = ByteUtils.combineBytes(commandByte, numOfFilesBytes, listBuffer, guid);

            // Sending UPD packet
            mSocket.SendTo(sendBuffer, endPoint);
            Console.WriteLine("\nUDP Responder");
            Console.WriteLine(String.Format("Sending file info to {0}, num of fileinfo: {1}", endPoint.ToString(), numOfFiles));
        }

        /// <summary>
        /// make byteArray containing file info.
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        private byte[] makeFileInfoByte(string filePath)
        {
            // check if file exists
            if (File.Exists(filePath) == false)
            {
                return null;
            }

            // get md5 hash
            byte[] md5 = ChecksumCalc.GetMD5Checksum(filePath);

            Console.WriteLine(BitConverter.ToString(md5));

            FileInfo f = new FileInfo(filePath);

            // Using windows 1252 encoding
            Encoding encoding1252 = Encoding.GetEncoding(1252);

            //byte[] fileName = Encoding.ASCII.GetBytes(f.Name);
            byte[] fileName = encoding1252.GetBytes(f.Name);
            byte[] filenameSizePlusFilename = new byte[fileName.Length + 1];

            // copy byte representing the size of the filename
            // to the beginning of the first packet sent
            int length = fileName.Length;
            byte lengthB = (byte)length;
            new byte[] { lengthB }.CopyTo(filenameSizePlusFilename, 0);

            // copies file name in ASCII format from index 1 to length of filename
            Array.Copy(fileName, 0, filenameSizePlusFilename, 1, fileName.Length);

            // get file size and convert to 8 bytes
            long fileSize = f.Length;
            byte[] fileSizeB = BitConverter.GetBytes(fileSize);

            // fileInfoBuffer has size:
            // file name +
            // 1 byte for file name size +
            // 8 bytes is the size of the file + 
            // 16 bytes is the md5 hash of the file 
            byte[] fileInfoBuffer = new byte[fileName.Length + 1 + 8 + 16];

            // copy file name size and file name to preBuffer
            filenameSizePlusFilename.CopyTo(fileInfoBuffer, 0);

            // copy fileSizeB to end of preBuffer
            Array.Copy(fileSizeB, 0, fileInfoBuffer, fileName.Length + 1, 8);

            // copy md5 hash to preBuffer
            Array.Copy(md5, 0, fileInfoBuffer, fileName.Length + 1 + 8, 16);

            // format the transfer like this:
            // byte = filename size
            // file name
            // long = file size in bytes, ulong is 64 bit so filesize could be limitless
            // 16 bytes md5 hash of file
            // file content;

            return fileInfoBuffer;
        }


    }
}
