﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using SharedDesk.TCP_file_transfer;
// ShareDesk Namespace
using SharedDesk.Common;

namespace SharedDesk.UDP_protocol
{
    public class KademliaHandler
    {


        /// <summary>
        /// EVENTS FOR PEER
        /// </summary>
        public event handlerRequestTable receiveRequestTable;
        public delegate void handlerRequestTable(IPEndPoint endpoint);

        public event handlerFindChannel receiveClosest;
        public delegate void handlerFindChannel(byte[] guid, PeerInfo pInfo);

        public event handlerRequestClosest receiveRequestClosest;
        public delegate void handlerRequestClosest(IPEndPoint endpoint, byte[] sender, byte[] target);

        public event handlerRequestJoin receiveRequestJoin;
        public delegate void handlerRequestJoin(PeerInfo pInfo);

        public event handlerRequestLeave receiveRequestLeave;
        public delegate void handlerRequestLeave(byte[] guid);

        public event handlerRequestPing receiveRequestPing;
        public delegate void handlerRequestPing(IPEndPoint endpoint);

        public event handlerGUID receiveGUID;
        public delegate void handlerGUID(byte[] guid);

        public void handlePing(EndPoint remoteEnd, byte[] buff)
        {
            // byte[] portByteArray = buff.Skip(1).Take(16).ToArray();
            //int port = BitConverter.ToInt32(buff, 1);
           

            // create ip end point from udp packet ip and listen port received
            IPEndPoint remoteIpEndPoint = remoteEnd as IPEndPoint;
            //remoteIpEndPoint.Port = port;
             Console.WriteLine("Received a ping from: {0}, listen port: {1}", remoteEnd, remoteIpEndPoint.Port );

            receiveRequestPing(remoteIpEndPoint);
        }


        public void handleGuid(EndPoint remoteEnd, byte[] buff)
        {
            ByteSeparator bs = new ByteSeparator(buff, true);
            byte[] guidByteArray = bs.getTargetByte(Constants.GUID_SIZE);
            Console.WriteLine("Received a guid from: {0}, guid: {1}", remoteEnd, guidByteArray.ToString());
            receiveGUID(guidByteArray);
        }

        public void handleRequestTable(EndPoint remoteEnd, byte[] buff)
        {
            // get tcp port
            int port = BitConverter.ToInt32(buff, 1);
            // create ip endpoint from udp packet ip and tcp port received
            IPEndPoint remoteIpEndPoint = remoteEnd as IPEndPoint;
            remoteIpEndPoint.Port = port;
            // fire the event for routing table request
            receiveRequestTable(remoteIpEndPoint);
        }

        public void handleRequestClosest(EndPoint remoteEnd, byte[] buff)
        {
            ByteSeparator bs = new ByteSeparator(buff, true);
            // Take the 2 GUIDS out of the buff[]
            byte[] guidSenderByteArray = bs.getTargetByte(Constants.GUID_SIZE).ToArray();
            byte[] guidTargetByteArray = bs.getTargetByte(Constants.GUID_SIZE).ToArray();

            byte[] portByteArray = bs.getTargetByte(Constants.TCP_PORT_SIZE);
            Console.WriteLine("Received a find closest to {0} request from {1}", guidTargetByteArray, guidSenderByteArray);

            // Create IP endpoint from udp packet
            IPEndPoint remoteIpEndPoint = remoteEnd as IPEndPoint;

            int port = BitConverter.ToInt32(portByteArray, 0);
            remoteIpEndPoint.Port = port;

            // Raising the event to handle the information
            receiveRequestClosest(remoteIpEndPoint, guidSenderByteArray, guidTargetByteArray);
        }

        public void handleClosest(byte[] buff)
        {
            ByteSeparator bs = new ByteSeparator(buff, true);
            byte[] channelGUID = bs.getTargetByte(Constants.GUID_SIZE);//buff.Skip(1).Take(1).ToArray();
            byte[] data = bs.getTargetByte(Constants.TOTAL_PEER_SIZE);//buff.Skip(2).ToArray();
            PeerInfo pInfo = ByteUtils.byteArrayToPeerInfo(data);


            //get the corresponding channel
            receiveClosest(channelGUID, pInfo);
        }

        public void handleJoin(byte[] buff)
        {
            ByteSeparator bs = new ByteSeparator(buff, true);
            byte[] data = bs.getTargetByte(Constants.TOTAL_PEER_SIZE);// buff.Skip(1).ToArray();
            PeerInfo pInfo = ByteUtils.byteArrayToPeerInfo(data);
            //add the pInfo with event
            receiveRequestJoin(pInfo);
        }

        public void handleLeave(byte[] buff)
        {
            byte[] data = buff.Skip(1).Take(Constants.GUID_SIZE).ToArray();
            //remove the guid with event
            receiveRequestLeave(data);
        }
    }
}
