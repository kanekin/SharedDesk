using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace SharedDesk.UDP_protocol
{
    public class UserUtils
    {
        private Socket mSocket = null;

        public UserUtils(Socket socket)
        {
            mSocket = socket;
        }

        public void inviteRequest(int myGuid, String myUsername, EndPoint endPoint)
        {
            // Byte indicating what type of packet it is
            byte[] commandByte = new byte[] { (int)UDPConstants.commandByte.U_INVITE_REQUEST };

            byte[] guid = new byte[] { (byte)myGuid };

            byte[] buffer = combineBytes(commandByte, guid);
            
            byte[] username = GetBytes(myUsername);

            byte[] nameLength = new byte[] { (byte)username.Length };

            byte[] sendBuffer = combineBytes(buffer, nameLength);

            byte[] finalBuffer = combineBytes(sendBuffer, username);

            mSocket.SendTo(finalBuffer, endPoint);

            Console.WriteLine("\nUDP Responder");
            Console.WriteLine("Send invitation from GUID {0} with username {1} to peer {2}", myGuid.ToString(), myUsername, endPoint);
        }

        public void inviteResponse(int myGuid, Boolean flag, EndPoint endPoint)
        {
            // Byte indicating what type of packet it is
            byte[] commandByte = new byte[] { (int)UDPConstants.commandByte.U_INVITE_RESPONSE };

            byte[] guid = new byte[] { (byte)myGuid };

            byte[] buffer = combineBytes(commandByte, guid);

            byte[] answer = GetBytesBool(flag);

            byte[] sendBuffer = combineBytes(buffer, answer);

            mSocket.SendTo(sendBuffer, endPoint);

            Console.WriteLine("\nUDP Responder");

            Console.WriteLine("{0} is sending answer of invitation to peer {1}", myGuid.ToString(), endPoint);
        }

        /// <summary>
        /// Byte[] Functionality
        /// </summary>

        // Convert a bool argument to a byte array and display it. 
        public static byte[] GetBytesBool(bool argument)
        {
            byte[] byteArray = BitConverter.GetBytes(argument);
            return byteArray;
        }

        // Combines two byte arrays to one
        public byte[] combineBytes(byte[] first, byte[] second)
        {
            byte[] ret = new byte[first.Length + second.Length];
            Buffer.BlockCopy(first, 0, ret, 0, first.Length);
            Buffer.BlockCopy(second, 0, ret, first.Length, second.Length);
            return ret;
        }

        static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }
    }
}
