﻿using SharedDesk.UDP_protocol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
// ShareDesk Namespace
using SharedDesk.TCP_file_transfer;
using SharedDesk.Common;
using SharedDesk.Data_storage;

namespace SharedDesk.UDP_protocol
{
    /// <summary>
    /// Class that will handle the UDP socket
    /// </summary>
    public class UDPListener
    {
        private Socket mSocket = null;
        private byte[] buff = new byte[8192];

        private List<IPEndPoint> mEndPointList;

        // ref remoteEndPoint
        private EndPoint remoteEndPoint = new IPEndPoint(IPAddress.Any, 0);
        //private int listenPort;

        private KademliaHandler mKademliaHandler;
        private STUNHandler mSTUNHandler;
        private FileHandler mFileHandler;
        private AuthHandler mAuthHandler;
        private AndroidHandler mAndroidHandler;
        private UserHandler mUserHandler;
        private ErrorLog logFile;

        public UDPListener(Socket socket, PeerInfo myInfo)
        {
            //this.listenPort = UDPManager.myPort;
            mSocket = socket;
            ///Console.Write("Listening on port {0} ", UDPManager.myPort);
            mEndPointList = new List<IPEndPoint>();
            
            mSTUNHandler = new STUNHandler(mSocket);
            mKademliaHandler = new KademliaHandler();
            mFileHandler = new FileHandler(myInfo);
            mAuthHandler = new AuthHandler();
            mAndroidHandler = new AndroidHandler();
            mUserHandler = new UserHandler();
            logFile = new ErrorLog();
        }

        public void start()
        {
            // Start listening
            try
            {
                mSocket.BeginReceiveFrom(buff, 0, buff.Length, SocketFlags.None, ref remoteEndPoint, new AsyncCallback(Listen), mSocket);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
            }
        }

        public KademliaHandler getKademliaHandler() 
        {
            return mKademliaHandler;
        }

        public STUNHandler getSTUNHandler()
        {
            return mSTUNHandler;
        }

        public AndroidHandler getAndroidHandler()
        {
            return mAndroidHandler;
        }

        public FileHandler getFileHandler()
        {
            return mFileHandler;
        }
        public AuthHandler getAuthHandler()
        {
            return mAuthHandler;
        }

        public UserHandler getUserHandler()
        {
            return mUserHandler;
        }

        public void setKademliaHandler(KademliaHandler kHandler)
        {
            mKademliaHandler = kHandler;
        }

        public void closeSocket()
        {
            mSocket.Close();
        }

        public void Listen(IAsyncResult ar)
        {
            int received = 0;
            Socket s = null;
            EndPoint remoteEnd = new IPEndPoint(IPAddress.Any, 0);

            try
            {
                s = (Socket)ar.AsyncState;
                received = s.EndReceiveFrom(ar, ref remoteEnd);

                //Console.WriteLine("\nUDP Listner port: {0}", listenPort);
                Console.WriteLine(
                    "{0} bytes received from {1}",
                    received,
                    remoteEnd
                );
            }
            //catch (SocketException ex)
            //{
            //    Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
            //    logFile.WriteErrorLog(ex.ToString()); 
            //    return;
            //}
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
            }

            if (received <= 0)
            {
                return;
            }

            byte firstByte = buff[0];
            UDPConstants.commandByte command = (UDPConstants.commandByte)firstByte;

            switch (command)
            {
                case UDPConstants.commandByte.ERROR:
                    break;
                case UDPConstants.commandByte.K_PING_REQUEST:
                    // Packet should be exactly 5 bytes
                    if (received != 1)
                    {
                        Console.WriteLine("Ping packet received wrong size...");
                        break;
                    }
                    mKademliaHandler.handlePing(remoteEnd, buff);
                    break;
                case UDPConstants.commandByte.K_PING_RESPONSE:
                    // Packet should be exactly 17 bytes
                    if (received != 2)
                    {
                        Console.WriteLine("GUID packet recevied wrong size...");
                        break;
                    }
                    mKademliaHandler.handleGuid(remoteEnd, buff);
                    break;
                case UDPConstants.commandByte.K_ROUTINGTABLE_REQUEST:
                    mKademliaHandler.handleRequestTable(remoteEnd, buff);
                    break;
                case UDPConstants.commandByte.K_CLOSESTPEER_REQUEST:
                    mKademliaHandler.handleRequestClosest(remoteEnd, buff);
                    break;
                case UDPConstants.commandByte.K_CLOSESTPEER_RESPONSE:
                    mKademliaHandler.handleClosest(buff);
                    break;
                case UDPConstants.commandByte.K_JOIN:
                    mKademliaHandler.handleJoin(buff);
                    break;
                case UDPConstants.commandByte.K_LEAVE:
                    mKademliaHandler.handleLeave(buff);
                    break;
                case UDPConstants.commandByte.F_FILEINFO:
                    mFileHandler.handleFileInfo(remoteEnd, buff);
                    break;
                case UDPConstants.commandByte.F_FILETRANSFER_REQUEST:
                    mFileHandler.handleFileTransferRequest(remoteEnd, buff);
                    break;
                case UDPConstants.commandByte.F_FILETRANSFER_RESPONSE:
                    mFileHandler.handleFileTransferResponse(remoteEnd, buff);
                    break;
                case UDPConstants.commandByte.F_FILEPART_REQUEST:
                    mFileHandler.handleFilePartRequest(remoteEnd, buff);
                    break;
                case UDPConstants.commandByte.F_FILEPART_RESPONSE:
                    //mFileHandler.handleFilePartResponse(remoteEnd, buff);
                    break;
                case UDPConstants.commandByte.F_SENDFILE_REQUEST:
                    break;
                case UDPConstants.commandByte.F_SEARCH_PUBLIC:
                    mFileHandler.handlePublicFileSearch(remoteEnd, buff);
                    break;
                case UDPConstants.commandByte.F_SEARCH_TABLE_RECEIVED:
                    mFileHandler.handleSearchTable(remoteEnd, buff);
                    break;
                case UDPConstants.commandByte.S_STUN_REQUEST:
                    //Clients should not receive this message
                    break;
                case UDPConstants.commandByte.S_STUN_RESPONSE:
                    mSTUNHandler.handleStunResponse(remoteEnd, buff);
                    break;
                case UDPConstants.commandByte.S_STUN_PUNCH_REQUEST:
                    //Clients should not receive this message
                    break;
                case UDPConstants.commandByte.S_STUN_PUNCH_REQUEST_FORWARD:
                    mSTUNHandler.handleStunForwardedPunchRequest(remoteEnd, buff);
                    break;
                case UDPConstants.commandByte.S_STUN_PUNCH:
                    mSTUNHandler.handleStunReceivePunch(remoteEnd, buff);
                    //Do we need to reply? telling that punch is received? Tunneling complete?
                    break;
                case UDPConstants.commandByte.S_STUN_KEEP_ALIVE:
                    //DO nothing?
                    break;
                case UDPConstants.commandByte.C_HANDLE_MESSAGE:
                    // TODO: 
                    break;
                        // jan sucks
                case UDPConstants.commandByte.A_PUBLICKEY_REQUEST:
                    mAuthHandler.handlePublicKeyRequest(remoteEnd, buff);
                    break;
                case UDPConstants.commandByte.A_PUBLICKEY_RESPONSE:
                    mAuthHandler.handlePublicKeyResponse(remoteEnd, buff);
                    break;
                case UDPConstants.commandByte.ANDROID_REQUEST_FILE_INFO:
                    mAndroidHandler.handleFileListRequest(remoteEnd);
                    break;
                case UDPConstants.commandByte.ANDROID_REQUEST_FILE:
                    mAndroidHandler.handleFileRequest(remoteEnd, buff);
					break;
                case UDPConstants.commandByte.U_INVITE_REQUEST:
                    mUserHandler.handleInvitationRequest(remoteEnd, buff);
                    break;
                case UDPConstants.commandByte.U_INVITE_RESPONSE:
                    mUserHandler.handleInvitationResponse(remoteEnd, buff);
                    break;
                default:
                    
                    Console.WriteLine("Not a valid command..."+buff);
                    for (int i = 0; i < 5; i++)
                    {
                        Console.WriteLine(buff[i]);
                    }
                    break;
            }
			
            // Starting the socket again
            try
            {
                mSocket.BeginReceiveFrom(buff, 0, buff.Length, SocketFlags.None, ref remoteEndPoint, new AsyncCallback(Listen), mSocket);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());

                // restart socket? 
                mSocket.Close();
                this.start();
            }

        }
    }
}
