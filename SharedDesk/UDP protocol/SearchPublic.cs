﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace SharedDesk.UDP_protocol
{
    class SearchPublic
    {
        private byte[] mGuid;
        private List<int> mSearchedPeers;
        private FileSenderUtils mFileSender;
        private Object mLockLists;
        private bool isSearching;

        public SearchPublic(byte[] myGuid, FileSenderUtils sender)
        {
            mLockLists = new Object();
            
            mFileSender = sender;

            // TODO: 
            // change from int to byte[] or string

            mSearchedPeers = new List<int>();
            mGuid = myGuid;
        }

        public void searchTable(RoutingTable table)
        {
            lock (mLockLists)
            {
                List<PeerInfo> peers = new List<PeerInfo>(table.Table.Values);
                foreach (PeerInfo p in peers)
                {
                    if (!mSearchedPeers.Contains(404))
                    {
                        IPEndPoint remotePoint = new IPEndPoint(IPAddress.Parse(p.IP), p.UDPport);
                        mSearchedPeers.Add(404);
                        mFileSender.sendPublicFileSearchRequest(mGuid, remotePoint);
                    }
                }
            }
        }
    }
}