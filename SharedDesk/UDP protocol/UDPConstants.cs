﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedDesk.UDP_protocol
{
    public static class UDPConstants
    {

        public const string STUN_SERVER_IP = "83.86.76.83";// John "92.109.173.161";//"192.168.0.107"; Rijswijk "83.86.76.83"
        public const int STUN_SERVER_PORT = 5000;
        // define all the possible commands
        public enum commandByte : byte
        {
            ERROR = 0,
            K_PING_REQUEST,//1
            K_PING_RESPONSE,//2
            K_ROUTINGTABLE_REQUEST,//3
            K_ROUTINGTABLE_RECEIVED,//4
            K_CLOSESTPEER_REQUEST,//5
            K_CLOSESTPEER_RESPONSE, //6
            K_JOIN, //7
            K_LEAVE, //8
            F_FILEINFO, // for app 9
            F_FILETRANSFER_REQUEST, // for app 10
            F_FILETRANSFER_RESPONSE,// 11
            F_FILEPART_REQUEST,// 12
            F_FILEPART_RESPONSE,// 13
            F_SENDFILE_REQUEST,// 14
            F_SEARCH_PUBLIC, // 15
            F_SEARCH_TABLE_RECEIVED,//16
            S_STUN_REQUEST,// 16
            S_STUN_RESPONSE,// 17
            S_STUN_PUNCH_REQUEST,// 18
            S_STUN_PUNCH_REQUEST_FORWARD,//19
            S_STUN_PUNCH,//20
            S_STUN_KEEP_ALIVE,//21
            A_PUBLICKEY_REQUEST,    // AUTH: RSA public key request 22
            C_HANDLE_MESSAGE, //23
            A_PUBLICKEY_RESPONSE,   // AUTH: RSA send public key 24
            A_PUBLICKEY_SEND,        // AUTH: RSA send public key,25
            ANDROID_REQUEST_FILE_INFO, //26
            ANDROID_REQUEST_FILE, //27
            U_INVITE_REQUEST,
            U_INVITE_RESPONSE
        };
    }
}
