﻿using SharedDesk.MovingAverage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedDesk.TCP_file_transfer
{
    class BandwidthCounterSegmented
    {
        // for time left, display the "moving average" of 
        // the last 10 values. This display a "smooth" value change
        IMovingAverage avg = new SimpleMovingAverage(10);

        int receivedMBs = 0;
        string currentSpeed = "0 MB/s";
        string timeLeft = "NaN";

        public BandwidthCounterSegmented()
        {

        }

        public void addPart()
        {
            receivedMBs++;
        }
        public void calc(int remainingParts)
        {

            if (receivedMBs == 0 || remainingParts == 0)
            {
                currentSpeed = "0 MB/s";
                //timeLeft = "NaN";
                avg.AddSample(0);
            }
            else
            {
                currentSpeed = String.Format("{0} MB/s", receivedMBs);

                // get seconds left
                int secLeft = remainingParts / receivedMBs;

                // add it to the moving average object
                avg.AddSample(secLeft);

                // display the moving average value of the time left
                timeLeft = String.Format("{0} sec", Math.Ceiling(avg.Average));
            }

            receivedMBs = 0;
        }

        public string getPerSecond()
        {
            return currentSpeed;
        }
        public string getTimeLeft()
        {
            return timeLeft;
        }

    }
}
