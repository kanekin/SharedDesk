﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net.Sockets;
using System.Net;
using SharedDesk.UDP_protocol;
using SharedDesk.Common;

namespace SharedDesk
{
    public class TCPUploader
    {

        public static void peerUpload(byte[] targetGUID, PeerInfo closest, EndPoint endPoint)
        {
            // create socket for connection
            Socket sendingSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            // set buffer size
            sendingSocket.SendBufferSize = TCPStateObject.BufferSize;
            // connect the socket to the remote endpoint.
            sendingSocket.Connect(endPoint);

            // convert PeerInfo of closest to byte[]
            byte[] peerInfoInBytes = ByteUtils.peerInfoToByteArray(closest);
            // the peer GUID asked for
            byte[] targetGUIDInBytes = targetGUID;
            // buffer to send
            byte[] sendBuffer = ByteUtils.combineBytes(targetGUIDInBytes, peerInfoInBytes);

            Thread sendThread = new Thread(() => send("PEERINFO", sendBuffer, sendingSocket));
            sendThread.IsBackground = true;

            sendThread.Start();
        }

        public static void tableUpload(Dictionary<byte[], PeerInfo> table, EndPoint endPoint)
        {
            // create socket for connection
            Socket sendingSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            // set buffer size
            sendingSocket.SendBufferSize = TCPStateObject.BufferSize;
            // connect the socket to the remote endpoint.
            sendingSocket.Connect(endPoint);

            // the number of peers in the table
            byte[] sizeOfTable = BitConverter.GetBytes(table.Count());
            // convert RoutingTable to byte[]
            byte[] tableByte = ByteUtils.dictionaryToByteArray(table);
            // buffer to send
            byte[] sendBuffer = ByteUtils.combineBytes(sizeOfTable, tableByte);

            Thread sendThread = new Thread(() => send("TABLE", sendBuffer, sendingSocket));
            sendThread.IsBackground = true;

            sendThread.Start();
        }
        

        public static void send(string contactType, byte[] content, Socket sendingSocket)
        {
            try
            {
                // encdoder for text to byte and byte to text
                ASCIIEncoding encoder = new ASCIIEncoding();
                // for making contact
                byte[] contactBuffer = encoder.GetBytes(contactType);

                // telling the "server" we want to send a filepart
                int bytesRec = sendingSocket.Send(contactBuffer);
                // waiting for response
                byte[] bufferRec = new byte[1024];
                sendingSocket.Receive(bufferRec);

                // response to string
                string response = encoder.GetString(bufferRec, 0, bytesRec);

                // if ok
                if (response.Trim().CompareTo("OK") == 0)
                {
                    Console.WriteLine("Server responded with " + response);
                    // send content
                    int temp = sendingSocket.Send(content);
                    // wait response
                    bufferRec = new byte[1024];
                    sendingSocket.Receive(bufferRec);
                    // response to string
                    response = encoder.GetString(bufferRec, 0, bytesRec);
                    // if ok
                    if (response.Trim().CompareTo("OK") == 0)
                    {
                        Console.WriteLine("Server received the object.");
                        //say you are done
                        temp = sendingSocket.Send(encoder.GetBytes("DONE"));
                    }
                    else
                    Console.WriteLine("Server responded with error: "+ response);
                }
                // close connection
                sendingSocket.Close();
            }
            catch (Exception e) { Console.Out.WriteLine(e.StackTrace); }
        }
    }
}
