﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Threading;
// Sharedesk Namespace
using SharedDesk.Common;

namespace SharedDesk
{
    class FileUpload
    {
        // MD5 of the file we want to download
        string md5;
        // The path of the file.
        string filePath;
        // Number of parts
        int numberOfParts;
        // Size of each part (1MB)
        int partSize = 1024 * 1024;
        Thread sendThread;
        private IPEndPoint remotePoint;
        private int[] parts;

        public FileUpload(string md5, string fullPath, double fileSize, int[] parts, IPEndPoint remotePoint)
        {
            this.md5 = md5;
            this.parts = parts;
            this.remotePoint = remotePoint;
            this.filePath = fullPath;
            this.numberOfParts = (int)Math.Ceiling((double)(fileSize / partSize));
            this.sendThread = new Thread(new ThreadStart(initUpload));
            this.sendThread.IsBackground = true;

            sendThread.Start();
        }

        private void initUpload()
        {
            // create socket for connection
            Socket sendingSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            sendingSocket.SendBufferSize = TCPStateObject.BufferSize;

            // Connect the socket to the remote endpoint.
            sendingSocket.Connect(remotePoint);

            // prepare array with the parts requested
            byte[][] partArray = SplitFileArray(filePath, parts);

            // encdoder for text to byte and byte to text
            ASCIIEncoding encoder = new ASCIIEncoding();

            try
            {
                for (int i = 0; i < partArray.Length; i++)
                {
                    // telling the "server" we want to send a filepart
                    byte[] bufferSent = encoder.GetBytes("FILEPART");
                    int bytesRec = sendingSocket.Send(bufferSent);

                    // waiting for response
                    byte[] bufferRec = new byte[1024];
                    sendingSocket.Receive(bufferRec);

                    string response = encoder.GetString(bufferRec, 0, bytesRec);
                    if (response.Trim().CompareTo("OK") == 0)
                    {
                        Console.WriteLine("Server responded with " + response);
                        byte[] sendBuffer = getBuffer(md5, parts[i], partArray[i]);
                        int temp = sendingSocket.Send(sendBuffer);

                        bufferRec = new byte[1024];
                        sendingSocket.Receive(bufferRec);

                        response = encoder.GetString(bufferRec, 0, bytesRec);
                        if (response.Trim().CompareTo("OK") == 0)
                        {
                            Console.WriteLine("Server received the part");
                        }
                    }
                    else
                        Console.WriteLine("Server responded with error: "+ response);
                }
                sendingSocket.Send(encoder.GetBytes("DONE"));
                sendingSocket.Close();
            }
            catch (Exception e) { Console.Out.WriteLine(e.StackTrace); }
        }


        private static byte[][] SplitFileArray(string fileInputPath, int[] parts)
        {
            //File to byte arrays were we copy the parts from
            Byte[] byteSource = System.IO.File.ReadAllBytes(fileInputPath);
            //The length of the file in bytes.
            int fileLength = byteSource.Length;
            //The size of the parts
            int partSize = 1024 * 1024;//1MB
            //NOT USED BUT NEEDED//Calculates total parts of file
            int numberOfParts = (int)Math.Ceiling(((double)fileLength / partSize));
            //The return array that contains the parts
            Byte[][] partArray = new Byte[parts.Length][];


            //Looping through the array of the requested parts to find them
            for (int i = 0; i < parts.Length; i++)
            {
                //MAGIC
                if ((parts[i] + 1) * partSize > fileLength)
                {
                    int lastPartSize = fileLength - (parts[i] * partSize);
                    partArray[i] = new byte[partSize];
                    Array.Copy(byteSource, parts[i] * partSize, partArray[i], 0, lastPartSize);
                }
                else
                {
                    partArray[i] = new byte[partSize];
                    Array.Copy(byteSource, parts[i] * partSize, partArray[i], 0, partSize);
                }
            }
            return partArray;
        }

        public byte[] getBuffer(string md5, int part, byte[] content)
        {
            // byte indicating what type of packet it is
            byte[] commandByte = new byte[] { 11 };

            // make string into byte[] array
            byte[] md5ByteArray = ChecksumCalc.md5StringToByteArray(md5);

            byte[] partByteArray = BitConverter.GetBytes(part);

            // buffer to send
            byte[] sendBuffer = ByteUtils.combineBytes(commandByte, md5ByteArray, partByteArray, content);
            
            return sendBuffer;
        }
    }
}
