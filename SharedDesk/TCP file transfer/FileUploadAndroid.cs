﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using SharedDesk.UDP_protocol;
using SharedDesk.Data_storage;

namespace SharedDesk
{
    class FileUploadAndroid
    {
        // MD5 of the file we want to download
        string md5;
        // The path of the file.
        string filePath;
        // Filehelper to read file data
        //FileHelper filehelper;

        Thread sendThread;
        private IPEndPoint remotePoint;
        private ErrorLog logFile;

        public FileUploadAndroid(string md5, string filePath, IPEndPoint remotePoint)
        {
            this.md5 = md5;
            //this.parts = parts;
            this.remotePoint = remotePoint;
            //filehelper = new FileHelper();
            //this.filePath = filehelper.getFullFilePathOfSharedFile(this.md5);
            this.filePath = filePath;
            this.sendThread = new Thread(new ThreadStart(initUpload));
            this.sendThread.IsBackground = true;
            sendThread.Start();
        }

        private void initUpload()
        {

            Socket sendingSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            Byte[] byteSource = System.IO.File.ReadAllBytes(filePath);
            sendingSocket.SendBufferSize = byteSource.Length;
            sendingSocket.ReceiveBufferSize = byteSource.Length;
            //sendingSocket.SendBufferSize = (int)1.5 * 1024 * 1024;
            //sendingSocket.SendBufferSize = 65536;

            // Don't allow another socket to bind to this port.
            // Should perhaps consider this one
            //listener.ExclusiveAddressUse = true;

            // Connect the socket to the remote endpoint.
            Console.WriteLine("Connecting to: " + remotePoint.Address + ":" + remotePoint.Port);
            sendingSocket.Connect(remotePoint);

            //byte[][] partArray = SplitFileArray(filePath, parts);
            //UDPResponder responder = new UDPResponder(remotePoint, myInfo.getPORT());
            try
            {
                int temp = sendingSocket.Send(byteSource);
            }
            //catch (Exception e) { Console.Out.WriteLine(e.StackTrace); }
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
            }
        }
    }
}
