﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using SharedDesk.UDP_protocol;
using SharedDesk.Data_storage;

namespace SharedDesk.TCP_file_transfer
{
    public class FileDownloadHandler
    {
        // The md5 of the file
        private string md5;
        // The name of the file
        private string name;
        // The size of the file in bytes
        private double size;
        // The size of each part (1MB)
        private int partSize = 1024 * 1024;
        // The size of the last part
        private double lastPartSize;
        // Total parts of the file
        private int totalParts;
        // Object for locking the fileParts byte[][]
        private Object lockFileObj = new Object();
        // Remaining parts parts of the file
        private int remainingParts;
        // byte[][] with the parts of the file
        private byte[][] fileParts;
        // Object for locking the peer list, and counter;
        private Object lockPeersObj = new Object();
        // Online peers that own the file
        private List<PeerInfo> peers;
        // Peers counter
        private int totalPeers;
        // parts received counter
        private int nrOfPartsReceived = 0;
        private int port;

        // set if file is complete
        private bool complete = false;
        // in order to update the download speed
        BandwidthCounterSegmented bandwidth;
        // timer to check mb/s
        System.Timers.Timer timer = null;
        // ErrorHandler
        private ErrorLog logFile;

        public event handlerFileComplete fileCompleted;
        public delegate void handlerFileComplete(string md5);

        public FileDownloadHandler(string md5, double fileSize, string fileName, int totalPeers, int port)
        {
            this.md5 = md5;
            this.peers = new List<PeerInfo>();
            this.size = fileSize;
            this.name = fileName;
            this.totalParts = (int)Math.Ceiling(((double)size / partSize));
            this.remainingParts = totalParts;
            this.fileParts = new byte[totalParts][];
            this.lastPartSize = size - (partSize * (totalParts - 1));
            this.totalPeers = totalPeers;
            this.port = port;
            this.logFile = new ErrorLog();
        }

        // Send file download requests
        public void sendDownloadRequests()
        {
            if (peers.Count == 0)
            {
                Console.WriteLine("No peers found with this file");
                return;
            }

            bandwidth = new BandwidthCounterSegmented();

            timer = new System.Timers.Timer() { Interval = 1000, Enabled = true };
            timer.Elapsed += timer_Elapsed;
            timer.Start();

            // Averege parts to be asked from each peer
            int avgPartReq = totalParts / peers.Count;
            // For every online peer found that has the file
            for (int i = 0; i < peers.Count; i++)
            {
                // The array that will contain the requested parts
                int[] parts;
                // If its not the final peer
                if (i != peers.Count - 1)
                    // Request avg nmbr of parts
                    parts = new int[avgPartReq];
                // If it is the final peer
                else
                    // Request the remaining parts
                    parts = new int[totalParts - (avgPartReq * i)];
                // Adding the parts to the parts array
                for (int j = i * avgPartReq; j < avgPartReq * i + parts.Length; j++)
                {
                    parts[j - (i * avgPartReq)] = j;
                }

                // Request the parts
                IPEndPoint remotePoint = new IPEndPoint(IPAddress.Parse(peers[i].IP), peers[i].UDPport);
                FileSenderUtils.sendFilePartRequest(md5, port, Peer.getInstance().GUID, parts, remotePoint);
            }
        }

        void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            // don't trigger event if download is complete
            if (complete)
            {
                return;
            }

            // calculate the MB/s and the time left
            bandwidth.calc(remainingParts);

            // trigger event with information (every sec)
            FileTransferEvents.transferSpeed(name, md5, bandwidth.getPerSecond(), bandwidth.getTimeLeft());
        }

        // Creates the file upon receiving all parts
        private void completeFile()
        {
            // set file download complete
            complete = true;

            // stop and remove the timer that says mb/s and time left
            if (timer != null)
            {
                timer.Stop();
                timer.Dispose();
                timer = null;
            }

            // File byte array
            byte[] completeFile = new byte[(int)size];
            // For all the parts in the filePart[]
            for (int i = 0; i < fileParts.Length; i++)
            {
                // Until the end of the part
                for (int j = 0; j < fileParts[i].Length; j++)
                {
                    // Add to complete file byte array
                    completeFile[i * partSize + j] = fileParts[i][j];
                }
            }

            // define path where to store the file
            string saveFolder = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            saveFolder += "\\Downloads\\";

            // check if folder exist
            if (Directory.Exists(saveFolder) == false)
            {
                Directory.CreateDirectory(saveFolder);
            }

            try
            {
                // Crete stream, write and close when done
                FileStream fs = new FileStream("Downloads/" + name, FileMode.CreateNew);
                fs.Write(completeFile, 0, (int)size);
                fs.Close();
                fs.Dispose();
                fs = null;

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
            }
            finally
            {

                FileTransferEvents.transferSpeed(name, md5, "", "");
                FileTransferEvents.Percentage(name, md5, 100);
                FileTransferEvents.FileReceived(name, md5);
            }

            // Firing the event
            fileCompleted(md5);

            // set file to status "complete" in knownFiles.xml 
            Peer peer = Peer.getInstance();
            peer.init(new List<PeerInfo>());
            peer.fileHelper.setStatusOfFile(md5, FileInfoP2P.Status.complete);

        }

        // Adds part to the fileParts
        public void receivePart(int index, byte[] content)
        {
            // Locking fileParts and remainingParts
            lock (lockFileObj)
            {
                if (index == totalParts - 1)
                {
                    // make new array with last part size
                    byte[] temp = new byte[(int)lastPartSize];
                    // copy last part length from the content
                    Array.Copy(content, 0, temp, 0, temp.Length);
                    // set conent to your new array
                    content = temp;
                }
                // Adding part
                fileParts[index] = content;

                Console.Out.WriteLine("Name: " + name + " Part: " + index);
                // Decreamenting the counter
                remainingParts--;

                // calc percentage
                // trigger event with percentage increase 
                if (nrOfPartsReceived != 0)
                {
                    float af = (float)nrOfPartsReceived / (float)totalParts;
                    //progressbar equals rounded.
                    byte tot = (byte)Math.Ceiling(af * 100);
                    if (tot < 100)
                    {
                        FileTransferEvents.Percentage(name, md5, tot);
                    }
                    else
                    {
                        FileTransferEvents.Percentage(name, md5, 100);
                    }
                }

                nrOfPartsReceived++;
                bandwidth.addPart();

                // If no more parts are needed
                if (remainingParts == 0)
                {
                    // Complete the file
                    completeFile();
                }


            }
        }

        // Adds found peers the list
        public void onFound(PeerInfo pInfo, byte[] targetGUID)
        {
            // Locks the peer list and counter
            lock (lockPeersObj)
            {
                // If peer exists
                if (pInfo.GUID.SequenceEqual(targetGUID))
                    // Add to the list
                    peers.Add(pInfo);
                // Decrement counter
                totalPeers--;
                // If all peers are found
                if (totalPeers == 0)
                {
                    //Begin download
                    sendDownloadRequests();
                    //initDownload();
                }
            }
        }
    }
}
