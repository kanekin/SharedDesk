﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedDesk
{

    public delegate void MessageHandler(ChangeInFileDownload e);

    public class ChangeInFileDownload : EventArgs
    {
        public string filename { get; set; }
        public string speed { get; set; }
        public string timeLeft { get; set; }
        public byte percent { get; set; }
        public string md5 { get; set; }
        public byte[] md5ByteArray { get; set; }
    }

    static class FileTransferEvents
    {
        // event handler
        public delegate void MessageHandler(ChangeInFileDownload e);

        // events
        public static event MessageHandler transferStarted;
        public static event MessageHandler downloadComplete;
        public static event MessageHandler progress;
        public static event MessageHandler speedChange;

        public static void FileReceived(string filename, string md5)
        {
            ChangeInFileDownload args = new ChangeInFileDownload();
            args.filename = filename;
            args.md5 = md5;

            downloadComplete.Invoke(args);
        }

        public static void TransferStarted(string fileName, string md5)
        {

            ChangeInFileDownload args = new ChangeInFileDownload();
            args.filename = fileName;
            args.md5 = md5;

            transferStarted.Invoke(args);
        }

        public static void Percentage(string fileName, string md5, byte percentComplete)
        {
            ChangeInFileDownload args = new ChangeInFileDownload();
            args.filename = fileName;
            args.md5 = md5;
            args.percent = percentComplete;

            progress.Invoke(args);

        }

        public static void Percentage(string fileName, byte[] md5, byte percentComplete)
        {
            ChangeInFileDownload args = new ChangeInFileDownload();
            args.filename = fileName;
            args.md5ByteArray = md5;
            args.percent = percentComplete;

            progress.Invoke(args);

        }

        public static void transferSpeed(string fileName, string md5, string speed, string timeLeft)
        {

            ChangeInFileDownload args = new ChangeInFileDownload();
            args.filename = fileName;
            args.md5 = md5;
            args.speed = speed;
            args.timeLeft = timeLeft;

            speedChange.Invoke(args);

        }

        public static void transferSpeed(string fileName, byte[] md5, string speed, string timeLeft)
        {

            // BUG:
            // for slow transfers it tend to show 0 kb/s 
            // then suddenly 10 kb - 20 kb and 0 kb/s again

            // Quick fix
            if (speed != "0 b/s")
            {

                ChangeInFileDownload args = new ChangeInFileDownload();
                args.filename = fileName;
                args.md5ByteArray = md5;
                args.speed = speed;
                args.timeLeft = timeLeft;

                speedChange.Invoke(args);

            }
        }


    }
}
