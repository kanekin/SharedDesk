﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using SharedDesk.Kadelima;
using SharedDesk.UDP_protocol;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using SharedDesk.TCP_file_transfer;
using SharedDesk.interfaces;
using SharedDesk.Data_storage;

namespace SharedDesk
{
    public class FileDownloadManager
    {
        Dictionary<string, FileDownloadHandler> fileDownloads;
        // Lock object
        Object lockPeersObj = new Object();
        Object lockFileObj = new Object();
        private PeerInfo myInfo;
        int port;

        // ErrorHandler
        private ErrorLog logFile;

        public FileDownloadManager(PeerInfo myInfo, FileSenderUtils filesender)
        {
            this.logFile = new ErrorLog();
            this.myInfo = myInfo;
            this.fileDownloads = new Dictionary<string, FileDownloadHandler>();
            this.port = Peer.getInstance().TCPPort;
        }

        public void addFileForDownLoad(string md5, int totalPeers)
        {
            // if file is already downloading
            if (fileDownloads.ContainsKey(md5))
            {
                Console.WriteLine("Download already started!");
                return;
            }

            Peer peer = Peer.getInstance();

            // get file size
            double fileSize = peer.fileHelper.getSize(md5);
            // get file name
            string fileName = peer.fileHelper.getFileName(md5);
            // create file handler
            FileDownloadHandler fHandler = new FileDownloadHandler(md5, fileSize, fileName, totalPeers, port);
            // subscribing to event
            fHandler.fileCompleted += new FileDownloadHandler.handlerFileComplete(completeFileDownload);
            // add to the fileDownloads
            fileDownloads.Add(md5, fHandler);
        }

        public void completeFileDownload(string md5)
        {
            // Removing file from downloads list
            fileDownloads.Remove(md5);
        }

        public void handleFilePart(byte[] buff)
        {
            // get md5 hash - 16 byte
            byte[] md5 = buff.Skip(1).Take(16).ToArray();
            // get part number - 4 byte
            byte[] partByteArray = buff.Skip(17).Take(4).ToArray();
            // get the content (1MB)
            byte[] content = buff.Skip(21).Take(1024 * 1024).ToArray();
            // byte md5 to string md5
            string md5String = BitConverter.ToString(md5);
            // byte part number to int part number
            int part = BitConverter.ToInt32(partByteArray, 0);
            // pass content to the appropriate file handler

            try
            {
                // receivePart(part, content);
                fileDownloads[md5String].receivePart(part, content);
            }
            catch (Exception ex)
            {
                // ERROR: 
                // An unhandled exception of type 'System.Collections.Generic.KeyNotFoundException' occurred in mscorlib.dll

                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
            }
        }

        // get appropriate file handler
        public FileDownloadHandler getFilehandler(string md5)
        {
            return fileDownloads[md5];
        }
    }
}
