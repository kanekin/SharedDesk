﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using SharedDesk.Common;
using SharedDesk.UDP_protocol;

namespace SharedDesk
{
    class TCPServerManager
    {
        Thread listenThread;
        private Peer mPeer;
        private int mPort;
        // lock object
        Object lockPeersObj = new Object();
        // bool for listening or not
        bool listening = false;

        public TCPServerManager()
        {
            this.listenThread = new Thread(new ThreadStart(startListening));
            this.listenThread.IsBackground = true;

            this.mPeer = Peer.getInstance();
            this.mPort = mPeer.TCPPort;
        }

        public void init()
        {
            // If we are not listening already
            if (!listening)
            {
                // Start listening
                listenThread.Start();
            }
        }

        public static ManualResetEvent allDone = new ManualResetEvent(false);
        private void startListening()
        {
            // start listening on TCP port
            // create end point
            EndPoint listenPoint = new IPEndPoint(IPAddress.Any, mPort);
            Socket listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                // Set listening to true
                listening = true;
                // Bind to port
                listener.Bind(listenPoint);
                // Set backlog
                listener.Listen(100);
                bool listen = true;
                // While listen is true
                while (listen)
                {
                    allDone.Reset();
                    // Accept peers
                    listener.BeginAccept(new AsyncCallback(AcceptCallback), listener);
                    allDone.WaitOne();
                }
            }
            catch (SocketException socketError)
            {
                Console.WriteLine("Socket error while transfering: " + socketError.Message);
                Console.WriteLine("Reseting connection...");
                //resetConnection();
            }
            catch (Exception error)
            {
                Console.WriteLine("Error while transfering: " + error.Message);
                return;
            }
        }


        // Accepting peer
        public void AcceptCallback(IAsyncResult ar)
        {
            Console.WriteLine("New Connection.");
            Socket listener = (Socket)ar.AsyncState;
            Socket handler = listener.EndAccept(ar);

            TCPStateObject state = new TCPStateObject();
            state.workSocket = handler;
            try
            {
                // Begins to asynchronously receive data 
                handler.BeginReceive(
                    state.buffer,        // An array of type Byt for received data 
                    0,             // The zero-based position in the buffer  
                    TCPStateObject.BufferSize, // The number of bytes to receive 
                    SocketFlags.None,// Specifies send and receive behaviors 
                    new AsyncCallback(makeContanct),//An AsyncCallback delegate 
                    state            // Specifies infomation for receive operation 
                    );

                // Begins an asynchronous operation to accept an attempt 
                AsyncCallback aCallback = new AsyncCallback(AcceptCallback);
                listener.BeginAccept(aCallback, listener);
            }
            catch (Exception exc) { Console.Out.WriteLine(exc.StackTrace); }
        }

        // Receiving data
        public void makeContanct(IAsyncResult ar)
        {
            try
            {
                TCPStateObject tempState = (TCPStateObject)ar.AsyncState;
                Socket handler = tempState.workSocket;
                // The number of bytes received. 
                byte[] buffer = tempState.buffer;
                int bytesRead = handler.EndReceive(ar);
                if (bytesRead > 0)
                {
                    ASCIIEncoding encoder = new ASCIIEncoding();
                    string action = encoder.GetString(buffer, 0, bytesRead);
                    switch (action)
                    {
                        case "TABLE":
                            Console.WriteLine("Peer wants to sent TABLE");
                            tempState.workSocket.BeginReceive(tempState.buffer, 0, TCPStateObject.BufferSize, SocketFlags.None, new AsyncCallback(receiveRoutingTable), tempState);
                            handler.Send(encoder.GetBytes("OK"));
                            break;
                        case "PEERINFO":
                            Console.WriteLine("Peer wants to sent PEERINFO");
                            tempState.workSocket.BeginReceive(tempState.buffer, 0, TCPStateObject.BufferSize, SocketFlags.None, new AsyncCallback(receivePeerInfo), tempState);
                            handler.Send(encoder.GetBytes("OK"));
                            break;
                        case "FILEPART":
                            Console.WriteLine("Peer wants to sent FILEPART");
                            tempState.workSocket.BeginReceive(tempState.buffer, 0, TCPStateObject.BufferSize, SocketFlags.None, new AsyncCallback(receivePart), tempState);
                            handler.Send(encoder.GetBytes("OK"));
                            break;
                        case "DONE":
                            Console.WriteLine("Peer is finished");
                            handler.Close();
                            break;
                        default:
                            Console.WriteLine("Unknown request");
                            tempState.workSocket.BeginReceive(tempState.buffer, 0, TCPStateObject.BufferSize, SocketFlags.None, new AsyncCallback(makeContanct), tempState);
                            handler.Send(encoder.GetBytes("ERROR"));
                            break;
                    }
                }
                else
                    tempState.workSocket.BeginReceive(tempState.buffer, 0, TCPStateObject.BufferSize, SocketFlags.None, new AsyncCallback(makeContanct), tempState);
            }
            catch (SocketException exc) { Console.Out.WriteLine(exc.StackTrace + "\n" + exc.ErrorCode); }

        }


        // receiving part
        private void receivePart(IAsyncResult ar)
        {
            try
            {
                // taking the state object
                TCPStateObject tempState = (TCPStateObject)ar.AsyncState;
                // taking the socket
                Socket handler = tempState.workSocket;
                // taking the buffer
                byte[] buffer = tempState.buffer;
                // the number of bytes received. 
                int bytesRead = handler.EndReceive(ar);

                // making temp array with pre existing buffer + the bytes we just read
                byte[] tempArr = new byte[tempState.bufferToAdd.Length + bytesRead];
                // populating the temp array with the pre existing buffer and the new one
                Array.Copy(tempState.bufferToAdd, 0, tempArr, 0, tempState.bufferToAdd.Length);
                Array.Copy(tempState.buffer, 0, tempArr, tempState.bufferToAdd.Length, bytesRead);
                // replacing the old buffer
                tempState.bufferToAdd = tempArr;

                // if the buffer is equal or exceeds the size we expect
                if (tempState.bufferToAdd.Length >= Constants.TOTAL_FILEPART_SIZE)
                {
                    // we take as much as we expected and put it in the download manager
                    mPeer.FileDownloadManager.handleFilePart(tempState.bufferToAdd.Take(Constants.TOTAL_FILEPART_SIZE).ToArray());
                    // and we empty the buffer
                    tempState.bufferToAdd = new byte[0];
                    // we are done with our with the part and we return to the makeContact state
                    tempState.workSocket.BeginReceive(tempState.buffer, 0, TCPStateObject.BufferSize, SocketFlags.None, new AsyncCallback(makeContanct), tempState);
                    ASCIIEncoding encoder = new ASCIIEncoding();
                    handler.Send(encoder.GetBytes("OK"));
                }
                else
                {
                    // we have not received what we expect so we try again
                    tempState.workSocket.BeginReceive(tempState.buffer, 0, TCPStateObject.BufferSize, SocketFlags.None, new AsyncCallback(receivePart), tempState);
                }
            }
            catch (SocketException exc) { Console.Out.WriteLine(exc.StackTrace + "\n" + exc.ErrorCode); }

        }

        // receiving PeerInfo object
        private void receivePeerInfo(IAsyncResult ar)
        {
            try
            {
                // taking the state object
                TCPStateObject tempState = (TCPStateObject)ar.AsyncState;
                // taking the socket
                Socket handler = tempState.workSocket;
                // taking the buffer
                byte[] buffer = tempState.buffer;
                // the number of bytes received. 
                int bytesRead = handler.EndReceive(ar);

                // making temp array with pre existing buffer + the bytes we just read
                byte[] tempArr = new byte[tempState.bufferToAdd.Length + bytesRead];
                // populating the temp array with the pre existing buffer and the new one
                Array.Copy(tempState.bufferToAdd, 0, tempArr, 0, tempState.bufferToAdd.Length);
                Array.Copy(tempState.buffer, 0, tempArr, tempState.bufferToAdd.Length, bytesRead);
                // replacing the old buffer
                tempState.bufferToAdd = tempArr;

                // if the buffer is equal or exceeds the size we expect
                if (tempState.bufferToAdd.Length >= Constants.TOTAL_PEER_SIZE + Constants.GUID_SIZE)
                {
                    byte[] buff = tempState.bufferToAdd.Take(Constants.TOTAL_PEER_SIZE + Constants.GUID_SIZE).ToArray();

                    ByteSeparator bs = new ByteSeparator(buff, false);
                    byte[] channelGUID = bs.getTargetByte(Constants.GUID_SIZE);
                    byte[] data = bs.getTargetByte(Constants.TOTAL_PEER_SIZE);
                    PeerInfo pInfo = ByteUtils.byteArrayToPeerInfo(data);
                    byte[] targetGUID = channelGUID;


                    //get the corresponding channel
                    mPeer.handleReceiveClosest(targetGUID, pInfo);
                    
                    //tempState.workSocket.BeginReceive(tempState.buffer, 0, TCPStateObject.BufferSize, SocketFlags.None, new AsyncCallback(makeContanct), tempState);
                    ASCIIEncoding encoder = new ASCIIEncoding();
                    handler.Send(encoder.GetBytes("OK"));
                    handler.Close();
                }
                else
                {
                    // we have not received what we expect so we try again
                    tempState.workSocket.BeginReceive(tempState.buffer, 0, TCPStateObject.BufferSize, SocketFlags.None, new AsyncCallback(receivePeerInfo), tempState);
                }
            }
            catch (SocketException exc) { Console.Out.WriteLine(exc.StackTrace + "\n" + exc.ErrorCode); }
        }

        // receiving RoutingTable
        private void receiveRoutingTable(IAsyncResult ar)
        {
            try
            {
                // taking the state object
                TCPStateObject tempState = (TCPStateObject)ar.AsyncState;
                // taking the socket
                Socket handler = tempState.workSocket;
                // taking the buffer
                byte[] buffer = tempState.buffer;
                // the number of bytes received. 
                int bytesRead = handler.EndReceive(ar);

                // making temp array with pre existing buffer + the bytes we just read
                byte[] tempArr = new byte[tempState.bufferToAdd.Length + bytesRead];
                // populating the temp array with the pre existing buffer and the new one
                Array.Copy(tempState.bufferToAdd, 0, tempArr, 0, tempState.bufferToAdd.Length);
                Array.Copy(tempState.buffer, 0, tempArr, tempState.bufferToAdd.Length, bytesRead);
                // replacing the old buffer
                tempState.bufferToAdd = tempArr;

                if (tempState.fistContact == false)
                {
                    if (tempState.bufferToAdd.Length >= 4)
                    {
                        tempState.fistContact = true;
                        int numberOfPeers = BitConverter.ToInt32(tempState.bufferToAdd.Take(4).ToArray(), 0);
                        tempState.numberOfObjects = numberOfPeers;
                        tempState.bufferToAdd = tempState.bufferToAdd.Skip(4).ToArray();
                    }
                }
                // if the buffer is equal or exceeds the size we expect
                if (tempState.bufferToAdd.Length >= Constants.TOTAL_PEER_SIZE*tempState.numberOfObjects)
                {

                    byte[] buff = tempState.bufferToAdd.Take(Constants.TOTAL_PEER_SIZE*tempState.numberOfObjects).ToArray();

                    ByteSeparator bs = new ByteSeparator(buff, false);
                    byte[] data = bs.getTargetByte(tempState.numberOfObjects*Constants.TOTAL_PEER_SIZE);
                    Dictionary<byte[], PeerInfo> peerDictionary = ByteUtils.byteArrayToPeerDictionary(data);

                    this.mPeer.handleTable(peerDictionary);

                    //tempState.workSocket.BeginReceive(tempState.buffer, 0, TCPStateObject.BufferSize, SocketFlags.None, new AsyncCallback(makeContanct), tempState);
                    ASCIIEncoding encoder = new ASCIIEncoding();
                    handler.Send(encoder.GetBytes("OK"));
                    handler.Close();
                    Console.WriteLine("Table received.");
                }
                else
                {
                    // we have not received what we expect so we try again
                    tempState.workSocket.BeginReceive(tempState.buffer, 0, TCPStateObject.BufferSize, SocketFlags.None, new AsyncCallback(receiveRoutingTable), tempState);
                }
            }
            catch (SocketException exc) { Console.Out.WriteLine(exc.StackTrace + "\n" + exc.ErrorCode); }
        }
    }
}
