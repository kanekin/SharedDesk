﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedDesk.TCP_file_transfer
{
    public class FileInfoP2P
    {
        public enum Status { newFile, downloading, complete, uploading, unknown };

        public string name;
        public long size;
        public byte[] md5;
        public Status status;
        private string path;

        public FileInfoP2P(string name, long size, byte[] md5, Status status )
        {
            this.name = name;
            this.size = size;
            this.md5 = md5;
            this.status = status;
        }

        public string Path
        {
            get { return this.path; }
            set { this.path = value; }
        }

        public string getMd5AsString()
        {
            return BitConverter.ToString(md5);
        }



    }
}
