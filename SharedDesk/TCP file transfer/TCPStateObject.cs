﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
namespace SharedDesk
{
    class TCPStateObject
    {
        // client socket.
        public Socket workSocket = null;
        // the size of the buffer
        public const int BufferSize = 1024 * 4;
        // receive buffer
        public byte[] buffer = new byte[BufferSize];
        // temp buffer
        public byte[] bufferToAdd = new byte[0];
        // setted to true if we need to specify if first contact
        public bool fistContact = false;
        // number of objects that you are going to be received
        public int numberOfObjects = 1;
        public Object lockBuffObj = new Object();
    }
}
