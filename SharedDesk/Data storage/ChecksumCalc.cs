﻿using SharedDesk.Data_storage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SharedDesk
{
    /// <summary>
    /// Class for calculating checksum of file
    /// 
    /// In the pre-buffer of the file transfer, send a MD5 hash
    /// When the receiving client has received the whole file, check the hash again
    /// We can then comfirm that the file received is 100% identical to the one sent
    /// </summary>
    class ChecksumCalc
    {

        // lock object
        static readonly object _checksumLocker = new object();
        private ErrorLog logFile;

        static public string getMD5String(byte[] md5Bytes)
        {
            if (md5Bytes == null)
            {
                return null;
            }

            return BitConverter.ToString(md5Bytes);
        }

        /// <summary>
        /// get md5 hash of file
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        static public byte[] GetMD5Checksum(string filePath)
        {

            HashAlgorithm MD5 = new MD5CryptoServiceProvider();

            lock (_checksumLocker)
            {

                try
                {

                    int maxAttempts = 5;
                    int c = 0;

                    // first check if the file is "locked"
                    // used by another process or some other IOExcpetion
                    // try to access the file every 500 milli sec 5 times before
                    // giving up
                    FileInfo f = new FileInfo(filePath);
                    while (IsFileLocked(f))
                    {
                        Thread.Sleep(500);
                        c++;

                        if (c >= maxAttempts)
                        {
                            return null;
                        }
                    }

                    // when file is avaliable attempt to calc hash
                    using (var stream = new BufferedStream(File.OpenRead(filePath), 100000))
                    {
                        return MD5.ComputeHash(stream);
                    }
                }
                catch (IOException ex)
                {
                    // TODO: 
                    // if this method return null, handle it it in the 
                    // methods that call this method!

                    Console.WriteLine("Used by another process: {0}", ex.Message);
                    Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                    //logFile.WriteErrorLog(ex.ToString());
                    return null;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                    //logFile.WriteErrorLog(ex.ToString());
                    return null;
                }


            }

        }


        static public byte[] md5StringToByteArray(string md5)
        {
            // ERROR: 
            // experienced a null referrence here!
            if (md5 == null)
            {
                return null;
            }

            String[] arr = md5.Split('-');
            byte[] array = new byte[arr.Length];

            for (int i = 0; i < arr.Length; i++)
            {
                array[i] = Convert.ToByte(arr[i], 16);
            }

            return array;
        }

        /// <summary>
        /// Check if arrays is identical to each other
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns>true if they are identical, false other vice</returns>
        static public bool checkIfHashisIdentical(byte[] a, byte[] b)
        {
            return Enumerable.SequenceEqual(a, b);
        }

        static public bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            catch (UnauthorizedAccessException)
            {
                // Three possible reasons for this: 
                // 1. The caller does not have the required permission.
                // 2. path is a directory.
                // 3. path specified a read-only file.
                // 4. can be out of memory (most likely)

                // perhaps file is set to read-only while file transfer takes place
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;

        }
    }

}

