﻿using SharedDesk.interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using SharedDesk.TCP_file_transfer;
using System.IO;
using System.Net;
using SharedDesk.Common;
using SharedDesk.Data_storage;

namespace SharedDesk.Data_storage
{
    public class SqliteStorage : IStoreData
    {

        private string ConnectionString = "Data Source=DB.db;Version=3;";
        private ErrorLog logFile;

        // constructor
        public SqliteStorage()
        {
            logFile = new ErrorLog();

            // check if db exist
            string file = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            file += "\\DB.db";

            if (File.Exists(file) == false)
            {
                // create db  
                createDB();
            }

        }

        private void createDB()
        {

            string sql = File.ReadAllText("..\\..\\..\\createDB.sql"); // .sql file path

            try
            {
                using (SQLiteConnection c = new SQLiteConnection(ConnectionString))
                {
                    c.Open();
                    using (SQLiteCommand cmd = new SQLiteCommand(sql, c))
                    {
                        cmd.ExecuteNonQuery();
                    }
                    c.Close();

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
            }
        }

        /// <summary>
        /// Remove all entries tables: File, Peer and Status
        /// </summary>
        public void resetDB()
        {

            string sqlDeleteStatus = "delete from state";
            string sqlDeletePeer = "delete from peer";
            string sqlDeleteFile = "delete from file";
            string sqlDeleteMySettings = "delete from MySettings";

            try
            {
                using (SQLiteConnection c = new SQLiteConnection(ConnectionString))
                {
                    c.Open();
                    using (SQLiteCommand cmd = new SQLiteCommand(sqlDeleteStatus, c))
                    {
                        cmd.ExecuteNonQuery();
                    }

                    using (SQLiteCommand cmd = new SQLiteCommand(sqlDeletePeer, c))
                    {
                        cmd.ExecuteNonQuery();
                    }

                    using (SQLiteCommand cmd = new SQLiteCommand(sqlDeleteFile, c))
                    {
                        cmd.ExecuteNonQuery();
                    }

                    using (SQLiteCommand cmd = new SQLiteCommand(sqlDeleteMySettings, c))
                    {
                        cmd.ExecuteNonQuery();
                    }
                    c.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
            }
        }

        public List<FileInfoP2P> getAvaliableFileDownloads()
        {
            return getAllAvaliableFiles();
        }

        private List<FileInfoP2P> getAllAvaliableFiles()
        {
            string sql = "select md5, name, size, complete from File where allowDownload == 0";
            List<FileInfoP2P> files = new List<FileInfoP2P>();

            try
            {
                using (SQLiteConnection c = new SQLiteConnection(ConnectionString))
                {
                    c.Open();
                    using (SQLiteCommand cmd = new SQLiteCommand(sql, c))
                    {
                        SQLiteDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            string name = (string)reader["name"];
                            long size = (long)reader["size"];
                            string md5 = (string)reader["md5"];
                            byte[] md5Byte = ChecksumCalc.md5StringToByteArray(md5);
                            if (md5Byte == null)
                            {
                                throw new Exception("error converting to md5!");
                            }

                            int complete = Convert.ToInt32(reader["complete"]);

                            FileInfoP2P f;
                            if (complete == 1)
                            {
                                f = new FileInfoP2P(name, size, md5Byte, FileInfoP2P.Status.complete);
                            }
                            else
                            {
                                f = new FileInfoP2P(name, size, md5Byte, FileInfoP2P.Status.newFile);
                            }
                            files.Add(f);
                        }
                        reader.Close();
                        reader.Dispose();
                    }
                    c.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
                return null;
            }

            return files;
        }

        public List<FileInfoP2P> getAvaliableFileDownloads(FileInfoP2P.Status status)
        {
            string sql;
            List<FileInfoP2P> files = new List<FileInfoP2P>();

            switch (status)
            {
                case FileInfoP2P.Status.newFile:
                    sql = "select md5, name, size from File where complete == 0 and allowDownload == 0 and downloading == 0";
                    break;
                case FileInfoP2P.Status.downloading:
                    sql = "select md5, name, size from File where complete == 0 and allowDownload == 0 and downloading == 1";
                    break;
                case FileInfoP2P.Status.complete:
                    sql = "select md5, name, size from File where complete == 1 and allowDownload == 0 and downloading == 0";
                    break;
                case FileInfoP2P.Status.uploading:
                    sql = "select md5, name, size from File where complete == 1 and allowDownload == 1 and uploading == 1";
                    break;
                default:
                    return null;
            }

            try
            {
                using (SQLiteConnection c = new SQLiteConnection(ConnectionString))
                {
                    c.Open();
                    using (SQLiteCommand cmd = new SQLiteCommand(sql, c))
                    {
                        SQLiteDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            string name = (string)reader["name"];
                            long size = (long)reader["size"];
                            string md5 = (string)reader["md5"];
                            byte[] md5Byte = ChecksumCalc.md5StringToByteArray(md5);
                            if (md5Byte == null)
                            {
                                throw new Exception("error converting to md5!");
                            }

                            FileInfoP2P f = new FileInfoP2P(name, size, md5Byte, status);
                            files.Add(f);
                        }
                        reader.Close();
                        reader.Dispose();
                    }
                    c.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
                return null;
            }

            return files;

        }

        public List<FileInfoP2P> getYourSharedFiles()
        {
            string sql = "select md5, name, size from File where complete == 1 and allowDownload == 1";
            List<FileInfoP2P> files = new List<FileInfoP2P>();

            try
            {
                using (SQLiteConnection c = new SQLiteConnection(ConnectionString))
                {
                    c.Open();
                    using (SQLiteCommand cmd = new SQLiteCommand(sql, c))
                    {
                        SQLiteDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            string name = (string)reader["name"];
                            long size = (long)reader["size"];
                            string md5 = (string)reader["md5"];
                            byte[] md5Byte = ChecksumCalc.md5StringToByteArray(md5);
                            if (md5Byte == null)
                            {
                                throw new Exception("error converting to md5!");
                            }

                            FileInfoP2P f = new FileInfoP2P(name, size, md5Byte, FileInfoP2P.Status.newFile);
                            files.Add(f);
                        }
                        reader.Close();
                        reader.Dispose();
                    }
                    c.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
                return null;
            }

            return files;
        }

        public List<string> getPublicFilePathsList()
        {
            throw new NotImplementedException();
        }

        public List<string> findPeersWithFile(string md5)
        {
            string sql = String.Format("select Peer_guid from State where file_md5 = '{0}'", md5);
            List<string> guids = new List<string>();

            try
            {
                using (SQLiteConnection c = new SQLiteConnection(ConnectionString))
                {
                    c.Open();
                    using (SQLiteCommand cmd = new SQLiteCommand(sql, c))
                    {
                        SQLiteDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            string guid = (string)reader["Peer_guid"];
                            guids.Add(guid);
                        }
                        reader.Close();
                        reader.Dispose();
                    }
                    c.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
                return null;
            }

            return guids;
        }

        public bool fileExist(string md5)
        {
            object o = null;
            try
            {
                string sql = String.Format("select * from file where md5 = '{0}'", md5);
                using (SQLiteConnection c = new SQLiteConnection(ConnectionString))
                {
                    c.Open();
                    using (SQLiteCommand cmd = new SQLiteCommand(sql, c))
                    {
                        o = cmd.ExecuteScalar();
                    }
                    c.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
            }

            if (o == null)
            {
                return false;
            }
            else
            {
                return true;
            }

        }

        public string getFullFilePathOfSharedFile(string md5)
        {
            string sql = String.Format("select path from file where md5 = '{0}'", md5);
            object result = null;

            try
            {
                using (SQLiteConnection c = new SQLiteConnection(ConnectionString))
                {
                    c.Open();
                    using (SQLiteCommand cmd = new SQLiteCommand(sql, c))
                    {
                        SQLiteDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            result = reader["path"];
                        }
                        reader.Close();
                        reader.Dispose();
                    }
                    c.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
            }

            if (result == null)
            {
                return "";
            }

            return (string)result;
        }

        public string getMd5OfFile(string name)
        {
            string sql = String.Format("select md5 from file where name = '{0}'", name); ;
            object result = null;

            try
            {
                using (SQLiteConnection c = new SQLiteConnection(ConnectionString))
                {
                    c.Open();
                    using (SQLiteCommand cmd = new SQLiteCommand(sql, c))
                    {
                        SQLiteDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            result = reader["md5"];
                        }
                        reader.Close();
                        reader.Dispose();
                    }
                    c.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
                return "";
            }

            return result.ToString();

        }

        public string getFileName(string md5)
        {
            string sql = String.Format("select name from file where md5 = '{0}'", md5);
            object result = null;

            try
            {
                using (SQLiteConnection c = new SQLiteConnection(ConnectionString))
                {
                    c.Open();
                    using (SQLiteCommand cmd = new SQLiteCommand(sql, c))
                    {
                        SQLiteDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            result = reader["name"];
                        }
                        reader.Close();
                        reader.Dispose();
                    }
                    c.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
                return "";
            }

            if (result == null)
            {
                return "";
            }

            return result.ToString();

        }

        public long getSize(string md5)
        {
            string sql = String.Format("select size from file where md5 = '{0}'", md5);
            object result = null;

            try
            {
                using (SQLiteConnection c = new SQLiteConnection(ConnectionString))
                {
                    c.Open();
                    using (SQLiteCommand cmd = new SQLiteCommand(sql, c))
                    {
                        SQLiteDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            result = reader["size"];
                        }
                        reader.Close();
                        reader.Dispose();
                    }
                    c.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
            }

            if (result == null)
            {
                return 0;
            }

            return (long)result;
        }

        public void authorizePublicFileDownload(System.IO.FileInfo file, string md5)
        {
            byte[] md5Byte = ChecksumCalc.md5StringToByteArray(md5);
            FileInfoP2P f = new FileInfoP2P(file.Name, file.Length, md5Byte, FileInfoP2P.Status.complete);

            addFileP2P(f);
            updateFilePath(md5, file.FullName);

            // add to state
            string myGuid = getGuid();

            if (myGuid == "")
            {
                // my guid has not been set!
                // add my guid
                return;
            }

            connectPeerAndFile(myGuid, md5);

            // set download complete and allowDownload 
            setStatusOfFile(md5, 0, 0, 1, 1, 1);

        }

        private bool guidExistInPeerTable(string guid)
        {
            string sql_select = String.Format("select * from peer where guid = '{0}'", guid);

            bool exist = false;
            try
            {
                using (SQLiteConnection c = new SQLiteConnection(ConnectionString))
                {
                    c.Open();

                    // check if guid already exist
                    using (SQLiteCommand cmd = new SQLiteCommand(sql_select, c))
                    {
                        SQLiteDataReader reader = cmd.ExecuteReader();
                        exist = reader.HasRows;
                        reader.Close();
                        reader.Dispose();
                    }
                    c.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
            }

            return exist;
        }

        private void addPeer(string guid, string username)
        {
            // TODO:
            // check if username already exist
            // if so, return false

            if (guidExistInPeerTable(guid))
            {
                return;
            }

            string sql = "";
            if (username == "")
            {
                sql = String.Format("insert into peer (guid, username) values ('{0}', NULL)", guid);
            }
            else
            {
                sql = String.Format("insert into peer (guid, username) values ('{0}', '{1}')", guid, username);
            }

            try
            {
                using (SQLiteConnection c = new SQLiteConnection(ConnectionString))
                {
                    c.Open();

                    // check if guid already exist
                    using (SQLiteCommand cmd = new SQLiteCommand(sql, c))
                    {
                        cmd.ExecuteNonQuery();
                    }
                    c.Close();
                }
            }
            catch (SQLiteException)
            {
                // I expect to get an error here. We attempt to insert a row with an GUID
                // that might already exist, then sqlite trigger the constraint and cause a exception.  
                // But the error can be safely ignored. 
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
            }
        }

        private void addFileP2P(TCP_file_transfer.FileInfoP2P file)
        {
            // TODO:
            // attempt to insert only the md5 (in case it does not already exist)
            // then update the table 

            string md5 = ChecksumCalc.getMD5String(file.md5);
            string sql;

            sql = String.Format("insert into file (md5, name, path, size) values ('{0}', '{1}', '{2}', {3})",
                                md5, file.name, file.Path, file.size);

            try
            {
                using (SQLiteConnection c = new SQLiteConnection(ConnectionString))
                {
                    c.Open();
                    using (SQLiteCommand cmd = new SQLiteCommand(sql, c))
                    {
                        cmd.ExecuteNonQuery();
                    }
                    c.Close();
                }
            }
            catch (SQLiteException)
            {
                // I expect to get an error here. We attempt to insert a row with an md5
                // that might already exist, then sqlite trigger the constraint and cause a exception.  
                // But the error can be safely ignored. 
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
            }
        }

        private void updateFilePublic(string md5, bool isPublic)
        {
            string sql;
            if (isPublic)
            {
                sql = String.Format("update File set public = 1 where md5 == '{0}'", md5);
            }
            else
            {
                sql = String.Format("update File set public = 0 where md5 == '{0}'", md5);
            }


            try
            {
                using (SQLiteConnection c = new SQLiteConnection(ConnectionString))
                {
                    c.Open();
                    using (SQLiteCommand cmd = new SQLiteCommand(sql, c))
                    {
                        cmd.ExecuteNonQuery();
                    }
                    c.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
            }
        }

        private void updateFilePath(string md5, string path)
        {

            string sql = String.Format("update File set path = '{0}' where md5 == '{1}'", path, md5);

            try
            {
                using (SQLiteConnection c = new SQLiteConnection(ConnectionString))
                {
                    c.Open();
                    using (SQLiteCommand cmd = new SQLiteCommand(sql, c))
                    {
                        cmd.ExecuteNonQuery();
                    }
                    c.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
            }
        }

        private void connectPeerAndFile(string guid, string md5)
        {
            string sql = String.Format("insert into state (File_md5, Peer_guid, Timestamp) values ('{0}', '{1}', CURRENT_TIMESTAMP )", md5, guid);

            try
            {
                using (SQLiteConnection c = new SQLiteConnection(ConnectionString))
                {
                    c.Open();
                    using (SQLiteCommand cmd = new SQLiteCommand(sql, c))
                    {
                        cmd.ExecuteNonQuery();
                    }
                    c.Close();
                }
            }
            catch (SQLiteException)
            {
                // I expect to get an error here. We attempt to insert a row with an GUID
                // that might already exist, then sqlite trigger the constraint and cause a exception.  
                // But the error can be safely ignored. 
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
            }

        }

        private void setStatusOfFile(string md5, int downloading, int uploading, int complete, int allowDownload)
        {
            // only two values allowed for the int: 0 for false and 1 for true
            string sql = String.Format("update file set downloading = {0}, uploading = {1}, complete = {2}, allowDownload = {3}, Timestamp = CURRENT_TIMESTAMP where md5 == '{4}';", downloading, uploading, complete, allowDownload, md5);

            try
            {
                using (SQLiteConnection c = new SQLiteConnection(ConnectionString))
                {
                    c.Open();
                    using (SQLiteCommand cmd = new SQLiteCommand(sql, c))
                    {
                        cmd.ExecuteNonQuery();
                    }
                    c.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
            }
        }

        private void setStatusOfFile(string md5, int downloading, int uploading, int complete, int allowDownload, int publicFile)
        {
            // only two values allowed for the int: 0 for false and 1 for true
            string sql = String.Format("update file set downloading = {0}, uploading = {1}, complete = {2}, allowDownload = {3}, public = {4}, Timestamp = CURRENT_TIMESTAMP where md5 == '{5}';", downloading, uploading, complete, allowDownload, publicFile, md5);

            try
            {
                using (SQLiteConnection c = new SQLiteConnection(ConnectionString))
                {
                    c.Open();
                    using (SQLiteCommand cmd = new SQLiteCommand(sql, c))
                    {
                        cmd.ExecuteNonQuery();
                    }
                    c.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
            }
        }

        public void authorizeFileDownloadWithPeer(System.IO.FileInfo file, string md5, PeerInfo peer)
        {

            byte[] md5Byte = ChecksumCalc.md5StringToByteArray(md5);
            FileInfoP2P f = new FileInfoP2P(file.Name, file.Length, md5Byte, FileInfoP2P.Status.complete);

            addFileP2P(f);
            updateFilePath(md5, file.FullName);


            string guidString = ByteUtils.convertByteToString(peer.GUID);

            connectPeerAndFile(guidString, md5);

            // set file status complete and allowDownload to true
            setStatusOfFile(md5, 0, 0, 1, 1);
        }

        public void setStatusOfFile(string md5, FileInfoP2P.Status status)
        {
            switch (status)
            {
                case FileInfoP2P.Status.newFile:
                    setStatusOfFile(md5, 0, 0, 0, 0);
                    break;
                case FileInfoP2P.Status.downloading:
                    setStatusOfFile(md5, 1, 0, 0, 0);
                    break;
                case FileInfoP2P.Status.complete:
                    setStatusOfFile(md5, 0, 0, 1, 0);
                    break;
                case FileInfoP2P.Status.uploading:
                    setStatusOfFile(md5, 0, 1, 1, 1);
                    break;
                case FileInfoP2P.Status.unknown:
                    break;
                default:
                    break;
            }
        }

        public void addKnownFile(string guid, TCP_file_transfer.FileInfoP2P file)
        {
            // some checks to avoid errors
            if (guid == "" || file == null)
            {
                return;
            }

            addFileP2P(file);
            addPeer(guid, "");

            string md5 = ChecksumCalc.getMD5String(file.md5);
            connectPeerAndFile(guid, md5);

        }

        public void addUser(string guid, string username)
        {
            // simple pre-condition
            if (guid == "" || username.Length > 255)
            {
                return;
            }

            addPeer(guid, username);

            // TODO: 
            // if the guid was already added then the above method
            // will not work and a update query is needed
        }

        public string getUsername(string guid)
        {
            string sql = String.Format("select username from peer where guid == {0}", guid);
            object result = null;

            try
            {
                using (SQLiteConnection c = new SQLiteConnection(ConnectionString))
                {
                    c.Open();
                    using (SQLiteCommand cmd = new SQLiteCommand(sql, c))
                    {
                        SQLiteDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            result = reader["username"];
                        }
                        reader.Close();
                        reader.Dispose();
                    }
                    c.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
            }

            if (result == null)
            {
                return "";
            }

            return (string)result;
        }

        private void insertGuid(string guid)
        {
            // add guid to MySettings table if it does not already exist
            string sql = String.Format("insert into MySettings (Peer_guid) values ('{0}')", guid);

            try
            {
                using (SQLiteConnection c = new SQLiteConnection(ConnectionString))
                {
                    c.Open();
                    using (SQLiteCommand cmd = new SQLiteCommand(sql, c))
                    {
                        cmd.ExecuteNonQuery();
                    }
                    c.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
            }
        }

        public void setGuid(string guid)
        {
            // most be positive
            if (guid == "")
            {
                return;
            }

            // if a guid already exist in MySettings and is different from this one
            // you try to set? remove current guid and replace it with this one? error?

            // add guid to Peer table if it does not already exist
            addPeer(guid, "");

            string currentGuid = getGuid();
            if (currentGuid == "")
            {
                // guid does not exist, insert into db
                insertGuid(guid);
            }

            // add guid to MySettings table if it does not already exist
            string sql = String.Format("update MySettings set Peer_guid = '{0}' where Peer_guid = '{1}'", guid, currentGuid);

            try
            {
                using (SQLiteConnection c = new SQLiteConnection(ConnectionString))
                {
                    c.Open();
                    using (SQLiteCommand cmd = new SQLiteCommand(sql, c))
                    {
                        cmd.ExecuteNonQuery();
                    }
                    c.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
            }


        }

        public string getGuid()
        {
            string sql = String.Format("select Peer_guid from MySettings");
            object result = null;

            try
            {
                using (SQLiteConnection c = new SQLiteConnection(ConnectionString))
                {
                    c.Open();
                    using (SQLiteCommand cmd = new SQLiteCommand(sql, c))
                    {
                        SQLiteDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            result = reader["Peer_guid"];
                        }
                        reader.Close();
                        reader.Dispose();
                    }
                    c.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
            }

            if (result == null)
            {
                return "";
            }

            return (string)result;
        }

        private void insertPort(int port, string guid)
        {
            // add guid to MySettings table if it does not already exist
            string sql = String.Format("insert into MySettings (port) values ({0}) where Peer_guid == '{1}'", port, guid);

            try
            {
                using (SQLiteConnection c = new SQLiteConnection(ConnectionString))
                {
                    c.Open();
                    using (SQLiteCommand cmd = new SQLiteCommand(sql, c))
                    {
                        cmd.ExecuteNonQuery();
                    }
                    c.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
            }
        }


        public void setPort(int port)
        {
            // most be positive
            if (port < 0 || port > 65535)
            {
                return;
            }

            int currentPort = getPort();
            if (currentPort == -1)
            {
                string guid = getGuid();
                if (guid == "")
                {
                    // ERROR
                    throw new Exception("errror getting guid from sqlite");
                }

                // port does not exist, insert into db
                insertPort(port, guid);
            }

            // add port to MySettings table if it does not already exist
            string sql = String.Format("update MySettings set port = {0} where port == {1}", port, currentPort);

            try
            {
                using (SQLiteConnection c = new SQLiteConnection(ConnectionString))
                {
                    c.Open();
                    using (SQLiteCommand cmd = new SQLiteCommand(sql, c))
                    {
                        cmd.ExecuteNonQuery();
                    }
                    c.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
            }
        }

        public int getPort()
        {
            string sql = String.Format("select port from MySettings");
            object result = null;

            try
            {
                using (SQLiteConnection c = new SQLiteConnection(ConnectionString))
                {
                    c.Open();
                    using (SQLiteCommand cmd = new SQLiteCommand(sql, c))
                    {
                        SQLiteDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            result = reader["port"];
                        }
                        reader.Close();
                        reader.Dispose();
                    }
                    c.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
            }

            if (result == null)
            {
                return -1;
            }

            return (int)result;
        }

        public void setBootPeerIP(string ip)
        {
            string guid = getGuid();
            if (guid == "")
            {
                return;
            }

            // check if ip is valid

            IPAddress address;
            if (IPAddress.TryParse(ip, out address))
            {
                switch (address.AddressFamily)
                {
                    case System.Net.Sockets.AddressFamily.InterNetwork:
                        // we have IPv4

                        string sql = String.Format("update MySettings set boot_peer_ip = '{0}' where Peer_guid == '{1}'", ip, guid);

                        try
                        {
                            using (SQLiteConnection c = new SQLiteConnection(ConnectionString))
                            {
                                c.Open();
                                using (SQLiteCommand cmd = new SQLiteCommand(sql, c))
                                {
                                    cmd.ExecuteNonQuery();
                                }
                                c.Close();
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                            logFile.WriteErrorLog(ex.ToString());
                        }


                        break;
                    case System.Net.Sockets.AddressFamily.InterNetworkV6:
                        // we have IPv6
                        break;
                    default:
                        // ERROR
                        return;
                }
            }
        }

        public string getBootPeerIP()
        {
            string sql = String.Format("select boot_peer_ip from MySettings");
            object result = null;

            try
            {
                using (SQLiteConnection c = new SQLiteConnection(ConnectionString))
                {
                    c.Open();
                    using (SQLiteCommand cmd = new SQLiteCommand(sql, c))
                    {
                        SQLiteDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            result = reader["boot_peer_ip"];
                        }
                        reader.Close();
                        reader.Dispose();
                    }
                    c.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
            }

            if (result == null)
            {
                return "";
            }

            return (string)result;
        }

        public void setBootPeerPort(int port)
        {
            // most be positive
            if (port < 0 || port > 65535)
            {
                return;
            }

            string guid = getGuid();
            if (guid == "")
            {
                return;
            }

            // add port to MySettings table if it does not already exist
            string sql = String.Format("update MySettings set boot_peer_port = {0} where Peer_guid == '{1}'", port, guid);

            try
            {
                using (SQLiteConnection c = new SQLiteConnection(ConnectionString))
                {
                    c.Open();
                    using (SQLiteCommand cmd = new SQLiteCommand(sql, c))
                    {
                        cmd.ExecuteNonQuery();
                    }
                    c.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
            }
        }

        public int getBootPeerPort()
        {
            string sql = String.Format("select boot_peer_port from MySettings");
            object result = null;

            try
            {
                using (SQLiteConnection c = new SQLiteConnection(ConnectionString))
                {
                    c.Open();
                    using (SQLiteCommand cmd = new SQLiteCommand(sql, c))
                    {
                        SQLiteDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            result = reader["boot_peer_port"];
                        }
                        reader.Close();
                        reader.Dispose();
                    }
                    c.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Message, ex.StackTrace);
                logFile.WriteErrorLog(ex.ToString());
            }

            if (result == null)
            {
                return -1;
            }

            return (int)result;
        }
    }
}
