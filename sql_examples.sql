drop table MySettings;
drop table state;
drop table peer;
drop table file;

CREATE TABLE `MySettings` (
	`Peer_guid`			TEXT NOT NULL UNIQUE,
	`downloadPath`		TEXT DEFAULT "",
	`port`				INT DEFAULT 62000,
	`boot_peer_ip`		TEXT DEFAULT "",
	`boot_peer_port`	INT DEFAULT 62000,	
	FOREIGN KEY(`Peer_guid`) 	REFERENCES `Peer` ( `guid` )
);

CREATE TABLE `File` (
	`md5`				TEXT NOT NULL PRIMARY KEY UNIQUE,
	`name`				TEXT DEFAULT "",
	`path`				TEXT DEFAULT "",
	`size`				LONG DEFAULT 0,
	`downloading`		TINYINT(1) DEFAULT 0,
	`uploading`			TINYINT(1) DEFAULT 0,
	`complete`			TINYINT(1) DEFAULT 0,
	`allowDownload`		TINYINT(1) DEFAULT 0,	
	`public`			TINYINT(1) DEFAULT 0,
	`Timestamp` 		DATETIME DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE `State` (
	`File_md5`			INT NOT NULL,
	`Peer_guid`			TEXT NOT NULL,
	`Timestamp` 		DATETIME DEFAULT CURRENT_TIMESTAMP,
	FOREIGN KEY(`File_md5`) 	REFERENCES `File` ( `md5` ),
	FOREIGN KEY(`Peer_guid`) 	REFERENCES `Peer` ( `guid` )
);

CREATE TABLE `Peer` (
	`guid`		TEXT NOT NULL PRIMARY KEY UNIQUE,
	`username`	TEXT UNIQUE
);

CREATE UNIQUE INDEX state_idx 	ON State(File_md5, Peer_guid);
CREATE INDEX size_idx on FILE(size);
CREATE INDEX md5_idx on FILE(md5);
CREATE INDEX name_idx on FILE(name);

select * from File;

/* insert md5, if the md5 already, it will be unsuccessful because md5 is set to UNIQUE */
insert into file (md5) values ('DC-60-73-FB-4F-D8-10-77-B1-28-C4-06-C6-54-81-00');
insert into file (md5, path, size, public) values (
		'31-41-A8-51-FF-3A-36-E4-0E-C8-48-5A-77-18-0F-61', 
		'C:\Users\Jan\Dropbox\music\Cypress Hill\Greatest Hits From the Bong\01 How I Could Just Kill a Man.mp3', 
		6764544, 0
	);
insert into file (md5, path, size, public) values (
		'04-26-9A-E7-EA-FB-E8-4D-B5-71-0F-C2-8D-49-81-0F', 
		'C:\Users\Jan\Dropbox\music\Cypress Hill\Greatest Hits From the Bong\03 Latin Lingo.mp3', 
		5730304, 0
	);
	

insert into peer (guid, username) values (16, "Neo");

/* get fileId using know md5 hash*/
select file.fileId from file where file.md5 = 'DC-60-73-FB-4F-D8-10-77-B1-28-C4-06-C6-54-81-00';

/* either insert or update relationship */
insert into state (File_fileId, Peer_guid) values (1, 16);
insert into state (File_fileId, Peer_guid) values (2, 16);
insert into state (File_fileId, Peer_guid) values (3, 16);
  
  
/* get file and state */
select * from 
File as F, State as S
where F.md5 == S.File_md5;

/* update/set path of file */
update File set path = "C://here/" where File.md5 == '69-41-A8-51-FF-3A-36-E4-0E-C8-48-5A-77-18-0F-61'

/* update state of a file */
update State set downloading = 1, uploading = 1, complete = 1, allowDownload = 1, Timestamp = CURRENT_TIMESTAMP 
where File_md5 == '69-41-A8-51-FF-3A-36-E4-0E-C8-48-5A-77-18-0F-61' and Peer_guid == 123;

/* get shared files */
select * from 
File as F, State as S
where 
F.md5 == S.File_md5 and 
allowDownload == 1 and 
complete = 1;

/* find peers with file, given an md5 hash */
select Peer_guid from State where file_md5 = '72-41-A8-51-FF-3A-36-E4-0E-C8-48-5A-77-18-0F-61';


/* new files */
select md5, name, size from File where complete == 0 and allowDownload == 0 and downloading == 0

/* downloading files */
select md5, name, size from File where complete == 0 and allowDownload == 0 and downloading == 1

/* complete files */
select md5, name, size from File where complete == 1 and allowDownload == 0 and downloading == 0

/* uploading files */
select md5, name, size from File where complete == 1 and allowDownload == 1 and uploading == 1

/* all available files  */
select md5, name, size from File where allowDownload == 0

/* my shared files */
select md5, name, size from File where complete == 1 and allowDownload == 1

/* set status of file to "downloading" */
update File set public = 1 where md5 == 











