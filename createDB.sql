CREATE TABLE `MySettings` (
	`Peer_guid`			TEXT NOT NULL UNIQUE,
	`downloadPath`		TEXT DEFAULT "",
	`port`				INT DEFAULT 62000,
	`boot_peer_ip`		TEXT DEFAULT "",
	`boot_peer_port`	INT DEFAULT 62000,	
	FOREIGN KEY(`Peer_guid`) 	REFERENCES `Peer` ( `guid` )
);

CREATE TABLE `File` (
	`md5`				TEXT NOT NULL PRIMARY KEY UNIQUE,
	`name`				TEXT DEFAULT "",
	`path`				TEXT DEFAULT "",
	`size`				LONG DEFAULT 0,
	`downloading`		TINYINT(1) DEFAULT 0,
	`uploading`			TINYINT(1) DEFAULT 0,
	`complete`			TINYINT(1) DEFAULT 0,
	`allowDownload`		TINYINT(1) DEFAULT 0,	
	`public`			TINYINT(1) DEFAULT 0,
	`Timestamp` 		DATETIME DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE `State` (
	`File_md5`			TEXT NOT NULL,
	`Peer_guid`			TEXT NOT NULL,
	`Timestamp` 		DATETIME DEFAULT CURRENT_TIMESTAMP,
	FOREIGN KEY(`File_md5`) 	REFERENCES `File` ( `md5` ),
	FOREIGN KEY(`Peer_guid`) 	REFERENCES `Peer` ( `guid` )
);

CREATE TABLE `Peer` (
	`guid`		TEXT NOT NULL PRIMARY KEY UNIQUE,
	`username`	TEXT UNIQUE
);

CREATE UNIQUE INDEX state_idx 	ON State(File_md5, Peer_guid);
CREATE INDEX size_idx on FILE(size);
CREATE INDEX md5_idx on FILE(md5);
CREATE INDEX name_idx on FILE(name);
